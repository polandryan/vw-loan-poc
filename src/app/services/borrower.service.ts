import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import * as moment from 'moment'


@Injectable()
export class BorrowerService {
    private currentBorrowers = new BehaviorSubject(null);
    borrowers(): Observable<any>{
        return this.currentBorrowers.asObservable();  
    }   
    async updateBorrowers(accountIds){
        let getNextBorrowers = await this.findBorrowers(accountIds)
        if(getNextBorrowers['success']){
            this.currentBorrowers.next(getNextBorrowers['data'])
        } 
    }
    addBorrowerToBorrowers(newBorrowerObject): void{
        this.currentBorrowers.next(this.currentBorrowers.getValue().concat([newBorrowerObject]))
    }

    constructor(private http: HttpClient, private session: SessionStorageService){}

    findBorrowers(accountId){
        return new Promise((res, rej)=>{            
            this.http.get('/api/borrower/account/' + accountId).toPromise()
            .then(resolved=>{res(resolved)})
            .catch(rejected=>{rej(rejected)})
        })

    }
    createBorrower(firstName){
        return new Promise((res, rej)=>{
            let criteria = {
                firstName: firstName,
                accountId: this.session.get('person')['currentAccountId'],
                create: {dateTime: moment().unix(), personId:this.session.get('person')['_id']}
            }
            this.http.post('/api/borrower/create', criteria).toPromise()
            .then(resolved=>{
                this.addBorrowerToBorrowers(resolved['data'])
                res(resolved)
            })
            .catch(rejected=>{rej(rejected)})
        })
    }
    deleteBorrower(borrowerId){
        return new Promise((res, rej)=>{
            let params = new HttpParams().set(
                '_id', borrowerId,).set(
                'accountId', this.session.get('person')['currentAccountId']).set(
                'createByPersonId', this.session.get('person')['_id'])

            this.http.delete('/api/borrower/delete/'+ borrowerId + '/account/' + this.session.get('person')['currentAccountId'], {params: params}).toPromise()
            .then(resolved=>{
                this.addBorrowerToBorrowers(resolved['data'])
                res(resolved)
            })
            .catch(rejected=>{rej(rejected)})
        })
    }
}