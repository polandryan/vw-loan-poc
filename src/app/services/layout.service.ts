import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable()
export class LayoutService {


    // orientation: landscape || portrate; size = small, medium, large   
    //layoutDefinition: {orientation: String, size: String}
    private currentUiLayout = new BehaviorSubject({orientation: 'landscape', size: 'large'});
    uiLayout(): Observable<any>{
        return this.currentUiLayout.asObservable();  
    }   
    updateUiLayout(width, height){
        let newUiLayout = {orientation: 'landscape', size: 'large'}
        if(width / height > 1){
            newUiLayout.orientation = 'landscape'
          }
        else{
            newUiLayout.orientation ='portrait';
        }
        if(width >= 1008){
            newUiLayout.size = 'large';
        }
        else if(width <= 640){
            newUiLayout.size = 'small';
        }
        else{
            newUiLayout.size = 'medium';
        }
        this.currentUiLayout.next(newUiLayout);
    }
}