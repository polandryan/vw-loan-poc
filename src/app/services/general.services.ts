import { Injectable } from '@angular/core';


@Injectable()
export class GeneralService {
    converObjectArrayToArray(objArray){
        let returnArray =[]
        for(let key in objArray){
            returnArray.push(objArray[key])
        }
        return returnArray
    }

    sortArray(inArray, criteria){
        //criteria is an array with objects[{key: 'key in the array', orderBy: 'ASC/DESC'}, {key: 'next key'}]
        //right now this is just a single dimensional sort 
        //onway to do a multi-dimensional would be to add the strings that need to be compaired 

        let sortedArray = inArray
        let keysExist = true;
        let orderBy = 1
        if(criteria[0]['orderBy'] === 'DESC'){orderBy = -1}

        for(let row of inArray){
            if(row.hasOwnProperty(criteria[0]['key']) && keysExist){
                keysExist = true
            }
            else{
                keysExist = false
            }
        }        
        if(keysExist){
            let compare = function(a,b){
                let keyA
                let keyB
                if(typeof a[criteria[0]['key']] === 'number' && typeof b[criteria[0]['key']] === 'number') {
                    keyA = a[criteria[0]['key']]
                    keyB = b[criteria[0]['key']]
                }
                else{
                    keyA = (a[criteria[0]['key']]).toUpperCase()
                    keyB = (b[criteria[0]['key']]).toUpperCase()
                }
                
                let comparison = 0
                if(keyA < keyB){
                    comparison = -1 * orderBy
                }
                else if(keyA > keyB){
                    comparison = 1 * orderBy
                }
                return comparison
            }
            return inArray.sort(compare)
        }
        else{
            return inArray
        }  
    }
}