import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

export interface Account {
    _id: string,
    name: String,
    emailIds: [{
        id: String,
        tier: String,
        }],
     when: {created: Number}
}


@Injectable()
export class AccountService {



    //GET & SET all of the Logged In USERS Accounts
    private currentAccounts = new BehaviorSubject(null);
    accounts(): Observable<any>{
        return this.currentAccounts.asObservable();  
    }   
    async updateAccounts(accountIds){
        let getNextAccounts = await this.getAccounts(accountIds)
        this.currentAccounts.next(getNextAccounts)
    }
    //Current Possible Account Tiers
    //TRIAL_ACCOUNT | PAID_ACCOUNT | FREE_ACCOUNT
    
    constructor(private http: HttpClient){}

    //eventually make this subject
    getAccounts(accountIds){
        let params = new HttpParams().set("accountIds", accountIds)
        
        return new Promise((res, rej)=>{
            this.http.get('/api/account', {params: params}).subscribe(subscribedResult=> {
                //console.log('get' + JSON.stringify(subscribedResult['data']))
                res(subscribedResult['data'])
            })})
        
        /*
        let result = await this.http.get('/api/account', {params: params}).subscribe(subscribedResult=> result = subscribedResult)
        console.log(JSON.stringify(result))
        return result.data 
        */ 
    }
}