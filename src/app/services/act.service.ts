import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable()
export class ActsService {
    private currentActs = new BehaviorSubject<any[]>([]); 
    acts(): Observable<any[]>{
        return this.currentActs.asObservable();  
    } 
    readActsFromDB(personId, obligationId, actId, types){
       this.readDBActs(personId, obligationId, types)
       .then(resolved=>{
           //console.log('inReadActsFromDB'+ JSON.stringify(resolved))
           if(resolved['success']){
               
            this.currentActs.next(resolved['data'])
           }
           else{
            this.currentActs.next([])  
           }
           
       })
       .catch(rejected=>{
        this.currentActs.next([]) 
       }) 
    }



    financialCreateEventOptions=[
        {value: 'customer_payment', displayName: 'Customer Payment', displayOrder: 1},
        {value: 'credit_fee', displayName: 'Fee', displayOrder: 2},
        {value: 'debit_principal', displayName: 'Reduce Principal', displayOrder: 3},
        {value:'debit_fee', displayName: 'Reduce Fee', displayOrder: 4},
        {value: 'credit_principal', displayName: 'Increase Principal', displayOrder: 5 }
    ]

    constructor(private router: Router, private session: SessionStorageService, private http: HttpClient){}

    createDBAct(createWithCriteria){
        return new Promise((res, rej)=>{
            let returnObj={success: false, data: null, message: 'create_no_dbAct'}
            this.http.put('/api/act/create', createWithCriteria).toPromise()
            .then(resolved=>{res(resolved)})
            .catch(rejected=>{rej(rejected)})
        })
    }

    readDBActs(personId, obligationId, types){
        return new Promise((res, rej)=>{
            let returnObj={success: false, data: null, message: 'create_no_dbAct'}
            let params = new HttpParams().set("personId", personId).set("obligationId", obligationId).set("types", types)
            this.http.get('/api/act/read', {params: params}).toPromise()
            .then(resolved=>{res(resolved)})
            .catch(rejected=>{rej(rejected)})
        })
    }

    updateDBAct(updateWithCriteria){
        return new Promise((res, rej)=>{
            let returnObj={success: false, data: null, message: 'create_no_dbAct'}
            this.http.put('/api/act/create', updateWithCriteria).toPromise()
            .then(resolved=>{res(resolved)})
            .catch(rejected=>{rej(rejected)})
        })
    }
    deleteDBAct(){
        return new Promise((res, rej)=>{
            let returnObj={success: false, data: null, message: 'create_no_dbAct'}
            res(returnObj)
        })
    }
    
    

}