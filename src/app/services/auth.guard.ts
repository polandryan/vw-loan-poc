import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {


    constructor(private router: Router, private session: SessionStorageService, private http: HttpClient){}
    
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
       // let person =  this.session.get('person')  
       // console.log(person._id)
       if(typeof this.session.get('person')._id == 'string'){
        return true
        }
        else{
            this.router.navigate(['/'])
            return false;
        }
    }
    processEmailPassword(email, password){
        let params = new HttpParams().set("email", email).set("password", password);
        return new Promise((res, rej)=>{
            this.http.get('/api/email/verify/password', {params: params}).toPromise()
            .then(resolved=>{
                if(resolved['success']){
                    //update session 
                    this.session.set('person', resolved['data'])
                }
                res(resolved)
            })
            .catch(rejected=>{rej(rejected)})
        })
    }
    accountCreate(email, password){
        let criteria = {email: email, password: password}
        return(new Promise((res, rej)=>{
            this.http.post('/api/account/create', criteria).toPromise()
            .then(regResolved=>{
                res(regResolved)
            })
            .catch(regRejected=>{
                rej(regRejected)
            })
        }))
    }
    verificationUrlParamCode(urlParam, code, password){
        let criteria = {urlParam: urlParam, code: code, password: password}
        return (new Promise((res, rej)=>{
            this.http.post('/api/email/verify/code', criteria).toPromise()
            .then(verifyResolved =>{
                res(verifyResolved)
            })
            .catch(verifyRejected =>{
                rej(verifyRejected)
            })
        }))
    }
    logout(){
        this.session.clear();
        this.router.navigate(['/']);
    }
}