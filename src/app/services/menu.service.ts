import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Router} from '@angular/router'

import { SessionStorageService } from 'angular-web-storage';
import { AccountService, GeneralService, BorrowerService, LoanService} from 'src/app/services';



@Injectable()
export class MenuService {
    
    private currentNavigationMenu = new BehaviorSubject({})
    navigationMenu(): Observable<any>{
        //console.log('currentNavMenu')
        return this.currentNavigationMenu.asObservable()
    }

    previousStringUrl = ''
    private currentSplitUrl = new BehaviorSubject([])
    splitUrl(): Observable<any>{
        return this.currentSplitUrl.asObservable()
    }

 

    constructor( 
        private router: Router,
        private session: SessionStorageService,
        private accountS: AccountService,
        private borrowerS: BorrowerService,
        private loanS: LoanService,
        private general: GeneralService ){
   
        this.router.events.subscribe(routerValues=>{
            if(!(String(this.previousStringUrl) === String(this.router.url)) && this.router.url.length > 1 ){
                this.previousStringUrl = this.router.url
                let routerList = this.router.url.split('/')
                routerList.splice(0,1)
            
                //UPDATE Split URL behaviorSubject
                this.currentSplitUrl.next(routerList)
            
                //update navMenu
                let person = this.session.get('person')
                this.menu(routerList, person)    
            }
        })
    }

    async menu(splitUrl, personObj){
        let path = '/'
        let menus = []
        let buttons = []
        //console.log('Split' +splitUrl.length + JSON.stringify(splitUrl))

        for(let idx = 0; idx <= splitUrl.length; idx++ ){

            //SET 0
            if(idx === 0){ //Account, Profile / Logout
                //SET Buttons
                buttons[idx] = {display: ''}
                if(String(splitUrl[idx]) === String('account')){
                    buttons[idx].display = 'Account'
                }
                if(String(splitUrl[idx]) === String('profile')){
                    buttons[idx].display = 'Profile'
                }
                //Generate Menu Options
                menus[idx]=[
                        {value: 'account', display: 'Accounts', routerLink: 'account', fullPath: path + '/account'},
                        {value: 'profile', display: 'Your Profile', routerLink: 'profile',fullPath: path + '/profile'},
                    ]
            }
            //SET 1
            if(idx === 1){
                path = path + '/' + splitUrl[idx - 1]

                //Generate Menu Options & button
                if(String(splitUrl[idx - 1]) === String('account')){
                    //account Menu options
                    let accounts = await this.accountS.getAccounts(personObj.accountIds)
                    let accountMenu = await this.general.converObjectArrayToArray(await this.constructAccountMenu(accounts, path, splitUrl, idx))
                    menus[idx] = accountMenu
                    //console.log('accountMen: ' + JSON.stringify(accountMenu))
                    
                    //Ascribe Button
                    if(splitUrl.length > idx){
                        for(let menuRow of accountMenu){
                            if(String(menuRow['value']) === String(splitUrl[idx])){
                                buttons[idx]= {display: menuRow['display'] }         
                            }
                        }
                    }
                }
            }
            //SET 2
            if(idx === 2){
                path = path + '/' + splitUrl[idx - 1]

                menus[idx] =[
                        {value: 'list', display: 'List(s)', routerLink: 'list', fullPath: path + '/list'},
                        {value: 'borrower', display: 'Borrower(s)', routerLink: 'borrower', fullPath: path + '/borrower'},
                        {value: 'manage', display: 'Manage', routerLink: 'manage', fullPath: path + '/manage'},
                    ]
                //Asscribe Button if url length is greater then current idx, else the button doesn't yet apply
                if(splitUrl.length > idx){
                    for(let menuRow of menus[idx]){
                        if(String(menuRow['value']) === String(splitUrl[idx])){
                            //console.log(menuRow['display'])
                            buttons[idx] = {display: menuRow['display'] } 
                        }
                    }
                }
                
            }
            //SET 3
            if(idx === 3){

                path = path + '/' + splitUrl[idx - 1]
                if(String(splitUrl[idx - 1]) === String('manage')){
                    menus[idx] = [
                        {value: 'people', display: 'People', routerLink: 'people', fullPath: path + '/people' },
                        {value: 'billing', display: 'Billing', routerLink: 'billing', fullPath: path + '/billing'}
                    ]
                    //Asscribe Button if url length is greater then current idx, else the button doesn't yet apply
                    if(splitUrl.length > idx){
                        for(let menuRow of menus[idx]){
                            //console.log(menuRow['value'] + ' ? ' + splitUrl[idx])
                            if(String(menuRow['value']) === String(splitUrl[idx])){
                                buttons[idx] = {display: menuRow['display'] } 
                            }
                        }
                    } 
                }
                if(String(splitUrl[idx - 1]) === String('borrower')){
                     let loanMenu = await this.constructBorrowerMenu(path)
                     menus[idx] = loanMenu
                    
                    //Asscribe Button if url length is greater then current idx, else the button doesn't yet apply
                    if(splitUrl.length > idx){
                        //console.log('inBorrower' + JSON.stringify(loanMenu))
                        for(let menuRow of menus[idx]){
                            //console.log(menuRow['value'] + ' ? ' + splitUrl[idx])
                            if(String(menuRow['value']) === String(splitUrl[idx])){
                                buttons[idx] = {display: menuRow['display'] } 
                            }
                        }
                    }
                }
            }
            if(idx===4){

                path = path + '/' + splitUrl[idx - 1]
                if(String(splitUrl[idx -2]) === String('borrower')){ //selected a specific person
                    menus[idx] =[
                        {value: 'obligation', display: 'Obligation(s)', routerLink: 'obligation', fullPath: path + '/obligation'},
                        {value: 'communication', display: 'Communications(s)', routerLink: 'communication', fullPath: path + '/communication'},
                        {value: 'event', display: 'Event(s)', routerLink: 'event', fullPath: path + '/event'},
                        {value: 'edit', display: 'Edit', routerLink: 'edit', fullPath: path + '/edit'},
                    ]
                }

                //Asscribe Button if url length is greater then current idx, else the button doesn't yet apply
                if(splitUrl.length > idx){
                    //console.log('idx4Buttons' + JSON.stringify(menus[idx]))
                    for(let menuRow of menus[idx]){
                        //console.log(menuRow['value'] + ' ? ' + splitUrl[idx])
                        if(String(menuRow['value']) === String(splitUrl[idx])){
                            buttons[idx] = {display: menuRow['display'] } 
                        }
                    }
                } 
            }

            if(idx===5){            
                path = path + '/' + splitUrl[idx - 1]
                if(String(splitUrl[idx - 1]) === String('obligation')){ 
                    menus[idx] = await this.constructLoanMenu(path,splitUrl )
                    //Asscribe Button if url length is greater then current idx, else the button doesn't yet apply
                    if(splitUrl.length > idx){
                        for(let menuRow of menus[idx]){
                            //console.log(menuRow['value'] + ' ? ' + splitUrl[idx])
                            if(String(menuRow['value']) === String(splitUrl[idx])){
                                buttons[idx] = {display: menuRow['display'] } 
                            }
                        }
                    }
                }
                //Asscribe Button if url length is greater then current idx, else the button doesn't yet apply
                if(splitUrl.length > idx){
                    for(let menuRow of menus[idx]){
                        //console.log(menuRow['value'] + ' ? ' + splitUrl[idx])
                        if(String(menuRow['value']) === String(splitUrl[idx])){
                            buttons[idx] = {display: menuRow['display'] } 
                        }
                    }
                }
            }
            if(idx===6){
              
                if(String(splitUrl[idx - 2]) === String('obligation') && !(String(splitUrl[idx - 1]) === String('create') ) ){ 
                    let loan = await this.loanS.findLoan(splitUrl[idx - 1])
                    this.loanS.createCurrentLoan(loan)
                    path = path + '/' + splitUrl[idx - 1]
                    //alert('in specific obligation')
                    menus[idx] =[
                        {value: 'edit', display: 'Edit', routerLink: 'edit', fullPath: path + '/edit'},
                        {value: 'event', display: 'Event(s)', routerLink: 'event', fullPath: path + '/event'},
                    ]
                    //Asscribe Button if url length is greater then current idx, else the button doesn't yet apply
                    if(splitUrl.length > idx){
                        for(let menuRow of menus[idx]){
                            //console.log(menuRow['value'] + ' ? ' + splitUrl[idx])
                            if(String(menuRow['value']) === String(splitUrl[idx])){
                                //console.log('display a persons name')
                                buttons[idx] = {display: menuRow['display'] } 
                            }
                        }
                    }
                }
            }
        }
        //console.log(JSON.stringify(buttons) )//+ '|' + JSON.stringify(menus))
        this.currentNavigationMenu.next({buttons: buttons, menus: menus})
    }


    async constructAccountMenu(accounts, previousPath, splitUrl, idxLess1){
        return new Promise((res, rej)=>{
            let newAccountMenu = [];
            //let buttonMenu 
            for(let account of accounts){
                //if(splitUrl[idxLess1 + 1] === account._id){
                    //buttonMenu = {display: account.name}
                //}
                newAccountMenu.push({value: account._id, display: account.name, routerLink: account._id, fullPath: previousPath + '/' + account._id })
            }
            newAccountMenu.push({value: 'new', display: 'Create New', routerLink: 'new', fullPath: previousPath + '/new' })
            if(accounts.length < 1){
                newAccountMenu.push({value: 'multiple', display: 'Multiple Account Dashboards', routerLink: 'multiple', fullPath: previousPath + '/multiple'})
            }
            res(newAccountMenu)
        })
    }
    async constructAccountIdMenu(previousPath){
        return new Promise((res, rej)=>{
            let newAccountIdMenu = [
                {value: 'todo', display: 'Things To Do', routerLink: 'todo', fullPath: previousPath + '/todo' },
                {value: 'borrower', display: 'Borrower(s)', routerLink: 'borrower', fullPath: previousPath + '/borrower' },
                {value: 'manage', display: 'Manage Account', routerLink: 'manage', fullPath: previousPath + '/manage'},
            ]
            res(newAccountIdMenu)
        })
    }
    async constructBorrowerMenu(previousPath){

        return new Promise( async (res, rej)=>{
            let newBorrowerMenu = [
                {value: '', display: 'Find', routerLink: '', fullPath: previousPath + '' },
                {value: 'create', display: 'Create Borrower', routerLink: 'create', fullPath: previousPath + '/create' }
            ]
            await this.borrowerS.borrowers().subscribe(borrowerObjects=>{
                
                if(!(JSON.stringify(borrowerObjects) === null)){
                    //console.log('are borrowers ' + JSON.stringify(borrowerObjects))
                    let borrowerArray = this.general.converObjectArrayToArray(borrowerObjects)    
                    for(let borrower of borrowerArray){
                        newBorrowerMenu.push({value: borrower._id, display: borrower.firstName, routerLink: borrower._id , fullPath: previousPath + '/' + borrower._id  })
                    }        
                }
                
            })
            if(newBorrowerMenu.length === 2){
                this.borrowerS.updateBorrowers(this.session.get('person')['currentAccountId'])
                let dbBorrowersObj = await this.borrowerS.findBorrowers(this.session.get('person')['currentAccountId'])

                if(dbBorrowersObj['success']){
                    let dbBorrowers = this.general.converObjectArrayToArray(dbBorrowersObj['data'])
                    for(let borrower of dbBorrowers){
                        newBorrowerMenu.push({value: borrower._id, display: borrower.firstName, routerLink: borrower._id , fullPath: previousPath + '/' + borrower._id  })
    
                    }
                }
                
            }

            res(newBorrowerMenu)
        })
    }
    async constructBorrowerIdMenu(previousPath){
        return new Promise((res, rej)=>{
            let newBorrowerIdMenu = [
                {value: 'obligation', display: 'Obligation(s)', routerLink: 'obligation', fullPath: previousPath + '/obligation' },
                {value: 'event', display: 'Event(s)', routerLink: 'event', fullPath: previousPath + '/event' },
                {value: 'communication', display: 'Communication(s)', routerLink: 'communication', fullPath: previousPath + '/communication' },
                
            ]
            res(newBorrowerIdMenu)
        })
    }
    async constructLoanMenu(previousPath, splitUrl){
        //console.log('constructLoanMenu')
        return new Promise( async (res, rej)=>{
            let newLoanMenu = [
                {value: '', display: 'Find Obligation(s)', routerLink: '', fullPath: previousPath + ''},
                {value: 'create', display: 'Create Obligation', routerLink: 'create', fullPath: previousPath + '/create'},
            ]
            this.loanS.loans().subscribe(loanObjects=>{
                let loanArray = this.general.converObjectArrayToArray(loanObjects)
                if(loanArray.length > 0){
                    for(let loan of loanArray){
                        newLoanMenu.push({
                            value: loan._id, display: loan.vin, routerLink: loan._id, fullPath: previousPath + '/' + loan._id 
                        })
                    }
                    
                }
            })
            if(newLoanMenu.length === 2){
                let loanArray = await this.loanS.findLoans(splitUrl[1],splitUrl[3])
                if(loanArray['success']){
                    this.loanS.createCurrentLoans(loanArray['data'])
                    for(let loan of loanArray['data']){
                       newLoanMenu.push({
                           value: loan._id, display: loan.vin, routerLink: loan._id, fullPath: previousPath + '/' + loan._id 
                       })
                   }
                }
            }
            //console.log('MenuService construct loanmenu' + JSON.stringify(newLoanMenu))
            res(newLoanMenu)
        })
    }
}