import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';


@Injectable()
export class LoanService {

    frequencyOptions =[
        {value:'monthly', displayName: 'Monthly', displayOrder: 1},
        {value:'weekly', displayName: 'Weekly', displayOrder: 2},
        {value:'biweekly', displayName: 'Once every 2 weeks', displayOrder: 3},
    ]
    compoundFrequencyOptions = [
        {value: 'simple', displayName: 'Exact / Simple', displayOrder: 1},
    ]


    private currentLoans = new BehaviorSubject(null);
    loans(): Observable<any>{
        return this.currentLoans.asObservable();  
    }   
    async updateLoans(personId, accountId){
        let getNextLoans = await this.findLoans(accountId, personId)
        //console.log(JSON.stringify(getNextLoans))
        if(getNextLoans['success']){
            this.currentLoans.next(getNextLoans['data'])
        }   
    }
    async createCurrentLoans(newCurrentLoans){
        this.currentLoans.next(newCurrentLoans)
    }

    private currentLoan = new BehaviorSubject(null);
    loan(): Observable<any>{
        return this.currentLoan.asObservable();
    }
    createCurrentLoan(newLoan){
        this.currentLoan.next(newLoan)
    }


    specificLoan(id){
        console.log('specificLoan'+ id)
        if(id){
            for(let loan of this.currentLoans.getValue()){
                if(id === loan._id){
                    //console.log('foundLoan' + JSON.stringify(loan))
                    this.currentLoan.next(loan)
                    //return loan
                }
            }
        }
        else{
            this.currentLoan.next({})
        }
    }


    constructor(private router: Router, private session: SessionStorageService, private http: HttpClient){}
    
    createLoan(criteria){
        return new Promise((res, rej)=>{            
            this.http.post('/api/obligation/create', criteria).toPromise()
            .then(resolved=>{
                console.log('createdLoan' + JSON.stringify(resolved))
                if(resolved['success']){
                    res(resolved['data'])
                }
            })
            .catch(rejected=>{rej(rejected)})
        })
    }
    checkCreateLoan(criteria){
        return new Promise((res,rej)=>{
            this.http.post('/api/obligation/checkCreate', criteria).toPromise()
            .then(resolved=>{
                //console.log('checkCreateLoanRESOLVED:' + JSON.stringify(resolved))
                res(resolved)
            })
            .catch(rejected=>{console.log('checkCreateLoanRejected:'+JSON.stringify(rejected))
                res({success: false, data: null, message: 'rejected checkCreateLoan'})})
        })
    }

    findLoans(accountId, personId){
        return new Promise((res, rej)=>{
            let params = new HttpParams().set("accountId", accountId).set("personId", personId)
            this.http.get('/api/obligation/account/person', {params: params}).toPromise()
            .then(resolved=>{
               //console.log('resolved http findLoans' +resolved)
                res(resolved)})
            .catch(rejected=>{rej(rejected)})
        })
    }

    findLoan(obligationId){
        return new Promise((res, rej)=>{
            let params = new HttpParams().set("obligationId", obligationId)
            this.http.get('./api/obligation/read', {params: params}).toPromise()
            .then(resolved=>{
                //console.log('FindLoan' + JSON.stringify(resolved))
                res(resolved['data'])
            })
            .catch(rejected=>{
                console.log('rejected findLoan')
                rej(rejected)
            })
        })
    }

    loanCustomerPayment(financialEvent){
        return new Promise((res, rej)=>{
            this.http.put('/api/obligation/payment/create',financialEvent ).toPromise()
            .then(resolved=>{
                if(resolved['success']){
                    this.currentLoan.next(resolved['data']['loan'])
                    res(true)
                }
                console.log('Loan payment' + JSON.stringify(resolved))})
            .catch(rejected=>{
                console.log('reject loan payment' + JSON.stringify(rejected))
            res(false) })
        })
    }
    loanFee(financialEvent){
        return new Promise((res, rej)=>{
            this.http.put('/api/obligation/fee/create',financialEvent).toPromise()
            .then(resolved=>{
                console.log('LoanService: loanFee' + JSON.stringify(resolved))
                this.currentLoan.next(resolved['data'])

                //also need to update acts
                
                res(true)})
            .catch(rejected=>{
                console.log('LoanService: loanFee rejected' + JSON.stringify(rejected))
                rej(false)})
        })
    }
    loanPrincipal(financialEvent){
        return new Promise((res, rej)=>{
            this.http.put('/api/obligation/principal/update',financialEvent).toPromise()
            .then(resolved=>{
                //console.log('LoanService: loanPrincipal' + JSON.stringify(resolved))
                this.currentLoan.next(resolved['data']['loan'])

                //also need to update acts
                
                res(true)})
            .catch(rejected=>{
                console.log('LoanService: loanPrincipal rejected' + JSON.stringify(rejected))
                rej(false)})
        })
    }

/*
    async intervalInterestRate(annualInterestRate, annualPeriodicPayments, compoundingPeriods){
        return new Promise((res, rej)=>{
            res( Number(Math.pow( (1 + Number((annualInterestRate / compoundingPeriods).toFixed(13))  ), Number((compoundingPeriods/ annualPeriodicPayments).toFixed(13) ) ).toFixed(13)) -1)
        })
    }

    truncNumber(inNum, sigFigs){
        let truncated
         if(sigFigs >= 0 && sigFigs<= 20){
             //console.log()
             let multiple = Math.pow(10,sigFigs)
            truncated = (Math.trunc(inNum * multiple) / multiple).toFixed(sigFigs)
        }
        else{
            truncated = null
        }
        return truncated
    }
    async convertCompoundLabelToInterval(intervalLabel, paymentFreqLabel){
        //Monthly = 12
        //daily = 365
        //continuiously = 1000000
        return new Promise((res, rej)=>{
            if( intervalLabel === 'match_payment_freq'){
                if(paymentFreqLabel === 'monthly'){
                    res(12)
                }
                else if(paymentFreqLabel === 'weekly'){
                    res(52)
                }
                else if(paymentFreqLabel === 'biweekly'){
                    res(26)
                }
                else if(paymentFreqLabel === 'daily'){
                    res(365)
                }
                else if(paymentFreqLabel === 'continuously'){
                    res(1000000)
                }
                else{
                    rej(null)
                }
            }
            else{
                if(intervalLabel === 'monthly'){
                    res(12)
                }
                else if(paymentFreqLabel === 'biweekly'){
                    res(26)
                }
                else if(intervalLabel === 'weekly'){
                    res(52)
                }
                else if(intervalLabel === 'daily'){
                    res(365)
                }
                else if(intervalLabel === 'continuously'){
                    res(1000000)
                }
                else{
                    rej(null)
                }
            }

        }) 
    }
    async convertPaymentLabelToIntervals(paymentFreqLabel){
        return new Promise((res, rej)=>{
            if(paymentFreqLabel === 'monthly'){
                res(12)
            }
            else if(paymentFreqLabel === 'biweekly'){
                res(26)
            }
            else if(paymentFreqLabel === 'weekly'){
                res(52)
            }
            else {
                rej(null)
            }
        })
    }
*/
   
}