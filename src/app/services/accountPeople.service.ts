import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { SessionStorageService } from 'angular-web-storage';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';


@Injectable()
export class AccountPeopleService {

    constructor(private router: Router, private session: SessionStorageService, private http: HttpClient){}


    addPersonsEmail(email, tier, accountId){
        //first see if you have already addedd it
        let criteria = {email: email, tier: tier, accountId: accountId }
        return(new Promise((res, rej)=>{
            this.http.post('/api/email/create', criteria).toPromise()
            .then(resolved=>{
                if(resolved['success']){
                    //console.log('wireup success');
                    console.log(JSON.stringify(resolved['data']))
                    if(String(resolved['message']) === String('email_created')){
                      alert('/verify/'+ resolved['data']['verification']['urlParam'] + '/setPassword')
                    }
                    res(resolved)
                  }
                  else{
                    console.log(resolved['message'])
                    res({success: false})
                  }
            })
            .catch(rejected=>{
                //console.log(JSON.stringify(rejected))
                rej(rejected)
            })
        }))
    }

    peopleAndEmail(accountId){
        let url = '/api/account/' + accountId + '/peoplesEmail'
        return new Promise((res, rej)=>{
            this.http.get(url).toPromise()
            .then(resolved=>{res(resolved)})
            .catch(rejected=>{rej(rejected)})
        })    
    }

    removePersonsEmail(accountId, emailId, accountEmailId){
        return new Promise((res, rej)=>{
            //let criteria = {accountId: accountId, emailId: emailId, accountEmailId: accountEmailId }
            let params = new HttpParams().set("accountId", accountId).set("emailId", emailId).set("accountEmailId", accountEmailId)
            let urlPath = '/api/account/email/'
            this.http.delete(urlPath, {params: params}).toPromise()
            .then(resolved=>{res(resolved)})
            .catch(rejected=>{rej(rejected)})
        })
    }
}