import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Router} from '@angular/router'

import { SessionStorageService } from 'angular-web-storage';
import {} from 'src/app/services';


@Injectable()
export class SettingService {

    //{eventDisplayTypes: [], eventDisplayOrder}
    private currentSettings = new BehaviorSubject({})
    settings(): Observable<any>{
        //console.log('currentNavMenu')
        return this.currentSettings.asObservable()
    }
    updateLocalSettings(newSettings){
        
        this.currentSettings.next(newSettings)
    }

    createSetting(key, value){
        //add to DB
        //update Local
        this.sessionS.set(key, value)
    }
    //updateSetting(){}

    constructor(private sessionS: SessionStorageService ){}
}
