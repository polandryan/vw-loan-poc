import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './services/auth.guard'

import { PublicComponent } from '../app/public/public.component'
    import { InitiateAuthenticationComponent} from '../app/public/initiate-authentication/initiate-authentication.component'
    import { VerifyEmailComponent} from '../app/public/verify-email/verify-email.component'
    import{ SetPasswordComponent } from '../app/public/verify-email/set-password/set-password.component'

import { PrivateComponent} from '../app/private/private.component'
    import {HomeComponent} from '../app/private/home/home.component'
    import {ProfileComponent} from '../app/private/profile/profile.component'
    import {AccountComponent} from '../app/private/account/account.component'
        import {AccountListComponent} from '../app/private/account/account-list/account-list.component'
        import {AccountNewComponent} from '../app/private/account/account-new/account-new.component'
        import {AccountHomeComponent} from '../app/private/account/account-home/account-home.component'
            import {AccountHomebaseComponent} from '../app/private/account/account-homebase/account-homebase.component'
            import { AccountManageComponent} from '../app/private/account/account-home/account-manage/account-manage.component'
                import {AccountManageHomeComponent} from '../app/private/account/account-home/account-manage/account-manage-home/account-manage-home.component'
                import {AccountPeopleComponent} from '../app/private/account/account-home/account-manage/account-people/account-people.component'
                import {BorrowerComponent} from '../app/private/borrower/borrower.component'
                    import{ManageBorrowersComponent} from '../app/private/borrower/manage-borrowers/manage-borrowers.component'
                    import {BorrowerManageComponent} from '../app/private/borrower/borrower-manage/borrower-manage.component'
                    import {BorrowerOverviewComponent} from '../app/private/borrower/borrower-overview/borrower-overview.component'
                    import {BorrowerObligationsRouterComponent} from '../app/private/obligation/borrower-obligations-router/borrower-obligations-router.component'
                    import {BorrowerObligationsComponent} from '../app/private/obligation/borrower-obligations/borrower-obligations.component'
                    import { ListBorrowerLoansComponent } from './private/obligation/list-borrower-loans/list-borrower-loans.component';
                    import { CreateBorrowerLoanComponent} from '../app/private/obligation/create-borrower-loan/create-borrower-loan.component'    
                        import {BorrowerObligationComponent} from '../app/private/obligation/borrower-obligation/borrower-obligation.component'
                        import { BorrowerObligationRouterComponent } from './private/obligation/borrower-obligation-router/borrower-obligation-router.component';
                import {ObligationEventRouterComponent} from '../app/private/event/obligation-events-router/obligation-event-router/obligation-event-router.component'    
                import {ObligationEventsComponent} from '../app/private/event/obligation-events-router/obligation-events/obligation-events.component'  


const routes: Routes =[
    {path: '', component: PublicComponent, children:[
        {path: '', component: InitiateAuthenticationComponent },
        {path: 'verify/:id', component: VerifyEmailComponent },
        {path: 'verify/:id/setPassword', component: SetPasswordComponent}
    ] },
    {path: '', component: PrivateComponent, canActivate: [AuthGuard], children:[
        {path: 'home', component: HomeComponent},
        {path: 'profile', component: ProfileComponent},
        {path: 'account', component: AccountComponent, children:[
            {path: '', component: AccountListComponent},
            {path: 'new', component: AccountNewComponent},
            {path: ':id', component: AccountHomeComponent, children:[
                {path: '', component: AccountHomebaseComponent},
                {path: 'manage', component: AccountManageComponent, children:[
                    {path: '', component: AccountManageHomeComponent},
                    {path: 'people', component: AccountPeopleComponent}
                ]},
                {path: 'borrower', component: BorrowerComponent, children:[
                    {path: '', component: ManageBorrowersComponent },
                    {path: ':id', component: BorrowerManageComponent, children:[
                        {path: '', component: BorrowerOverviewComponent},
                        {path: 'obligation', component: BorrowerObligationsRouterComponent, children:[
                            {path: '', component: BorrowerObligationsComponent},
                            {path: 'create', component: CreateBorrowerLoanComponent },
                            {path: ':id', component: BorrowerObligationRouterComponent, children:[
                                {path: '', component: BorrowerObligationComponent},
                                //{path: 'edit', component: BorrowerObligationComponent},
                                {path: 'event', component: ObligationEventRouterComponent, children: [
                                    {path: '', component: ObligationEventsComponent }
                                ]}

                            ]}

                        ] }
                    ]}
                ]}
                
            ]}
        ]}
     ]},
    
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRouters {}