import { Component, OnInit } from '@angular/core';
import {AuthGuard} from '../services/auth.guard'

@Component({
  selector: 'app-private',
  templateUrl: './private.component.html',
  styleUrls: ['./private.component.css']
})
export class PrivateComponent implements OnInit {

  constructor(private authGuard: AuthGuard){}
  ngOnInit() {   }

  logout(){
    this.authGuard.logout();
  }

}
