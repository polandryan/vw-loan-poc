import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { SessionStorageService } from 'angular-web-storage';
import { BorrowerService , GeneralService} from 'src/app/services';


@Component({
  selector: 'app-list-borrowers',
  templateUrl: './list-borrowers.component.html',
  styleUrls: ['./list-borrowers.component.css']
})
export class ListBorrowersComponent implements OnInit {
  formGroupRows: FormGroup = this.fb.group({
    rows: this.fb.array([])
  });
  buildRow(firstName, borrowerId){
    return new FormGroup({
      firstName: new FormControl(firstName),
      borrowerId: new FormControl(borrowerId),
    })
  }
  listBorrowers=[]

  addTableRow(firstName, accountId){
    //console.log(createCriteria.linkWorkTitle)
    this.tableRows.push(this.buildRow(firstName, accountId));
  }
  get tableRows(){
    return this.formGroupRows.get('rows') as FormArray
  }

  constructor(private fb: FormBuilder, private session: SessionStorageService, private borrowerS: BorrowerService, private generalS: GeneralService) { 
 
 /*   this.borrowerS.findBorrowers(this.session.get('person').currentAccountId)
    .then(resolved=>{
      //console.log(JSON.stringify(resolved))
      this.listBorrowers=[]
      for(let borrower of resolved['data']){
        this.addTableRow(borrower['firstName'],borrower['_id'] )
        this.listBorrowers.push({firstName: borrower['firstName'], _id: borrower['_id'] , path: './'+ borrower['_id'] + '/obligation' })
      }
      console.log(JSON.stringify(this.listBorrowers))
    })
    .catch()*/
    this.borrowerS.updateBorrowers(this.session.get('person').currentAccountId)
    this.borrowerS.borrowers().subscribe(borrowerObjects=>{
      this.listBorrowers = this.generalS.converObjectArrayToArray(borrowerObjects);
      
    })
    
  }


  ngOnInit() {
  }

  remove(borrowerId){
    this.borrowerS.deleteBorrower(borrowerId)
    .then(resolved=>{console.log(JSON.stringify(resolved))})
    .catch(rejected=>{console.log(JSON.stringify(rejected))})
  }
}
