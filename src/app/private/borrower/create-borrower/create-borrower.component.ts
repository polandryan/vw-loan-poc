import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { SessionStorageService } from 'angular-web-storage';
import { BorrowerService } from 'src/app/services';

@Component({
  selector: 'app-create-borrower',
  templateUrl: './create-borrower.component.html',
  styleUrls: ['./create-borrower.component.css']
})
export class CreateBorrowerComponent implements OnInit {
  addBorrower = new FormGroup({
    firstName: new FormControl()
  })

  constructor(private borrowerS: BorrowerService){}

  ngOnInit() {
  }
  processAddBorrower(){
    this.borrowerS.createBorrower(this.addBorrower.get('firstName').value)
    .then(resolved=>{
      this.addBorrower.get('firstName').setValue(null)
      //console.log('resolved:' + JSON.stringify(resolved))
    })
    .catch(rejected=>{
      //console.log('rejected:' + JSON.stringify(rejected))
    })

  }
  deleteBorrower(borrowerId){
    
  }

}
