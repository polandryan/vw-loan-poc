import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrowerManageComponent } from './borrower-manage.component';

describe('BorrowerManageComponent', () => {
  let component: BorrowerManageComponent;
  let fixture: ComponentFixture<BorrowerManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorrowerManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowerManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
