import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageBorrowersComponent } from './manage-borrowers.component';

describe('ManageBorrowersComponent', () => {
  let component: ManageBorrowersComponent;
  let fixture: ComponentFixture<ManageBorrowersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageBorrowersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageBorrowersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
