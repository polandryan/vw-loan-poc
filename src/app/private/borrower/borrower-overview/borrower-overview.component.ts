import { Component, OnInit } from '@angular/core';
import { MenuService, GeneralService} from 'src/app/services/index'

@Component({
  selector: 'app-borrower-overview',
  templateUrl: './borrower-overview.component.html',
  styleUrls: ['./borrower-overview.component.css']
})
export class BorrowerOverviewComponent implements OnInit {
  navMenu=[]

  constructor(private menuS: MenuService, private generalS: GeneralService) {
    this.menuS.navigationMenu().subscribe(resolved=>{
      let menuArray = this.generalS.converObjectArrayToArray(resolved['menus'])
      //console.log('borrowerOverview' + JSON.stringify(menuArray[4]))
      this.navMenu = menuArray[4]
    })
   }

  ngOnInit() {
  }

}
