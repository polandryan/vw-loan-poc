import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrowerOverviewComponent } from './borrower-overview.component';

describe('BorrowerOverviewComponent', () => {
  let component: BorrowerOverviewComponent;
  let fixture: ComponentFixture<BorrowerOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorrowerOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowerOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
