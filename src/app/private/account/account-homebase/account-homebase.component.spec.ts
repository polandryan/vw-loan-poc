import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountHomebaseComponent } from './account-homebase.component';

describe('AccountHomebaseComponent', () => {
  let component: AccountHomebaseComponent;
  let fixture: ComponentFixture<AccountHomebaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountHomebaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountHomebaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
