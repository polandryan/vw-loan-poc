import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountHomeLeftNavComponent } from './account-home-left-nav.component';

describe('AccountHomeLeftNavComponent', () => {
  let component: AccountHomeLeftNavComponent;
  let fixture: ComponentFixture<AccountHomeLeftNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountHomeLeftNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountHomeLeftNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
