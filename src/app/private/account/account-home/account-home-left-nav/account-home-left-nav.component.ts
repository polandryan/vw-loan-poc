import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-account-home-left-nav',
  templateUrl: './account-home-left-nav.component.html',
  styleUrls: ['./account-home-left-nav.component.css']
})
export class AccountHomeLeftNavComponent implements OnInit {
  menuItems = [
    {urlLink: '/', name: 'contact lists'},
    {urlLink: '/', name: 'enter payment(s)'},
    {urlLink: './borrower', name: 'Borrower(s)'},
    {urlLink: '/', name: 'create loan'},
    {urlLink: '/', name: 'manage loan'},
    {urlLink: './manage', name: 'manage account'},
    {urlLink: './manage/people', name: 'people'},
    {urlLink: '/', name: 'billing'},
    
    
  ]

  constructor() { }

  ngOnInit() {
  }

}
