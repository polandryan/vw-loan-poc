import { Component, OnInit } from '@angular/core';
import {MenuService} from '../../../services/menu.service'
import {GeneralService} from '../../../services/general.services'

@Component({
  selector: 'app-account-home',
  templateUrl: './account-home.component.html',
  styleUrls: ['./account-home.component.css']
})
export class AccountHomeComponent implements OnInit {
  displayMenu = false
  routerMenu=[]

  constructor(private menuS: MenuService, private generalS: GeneralService) { 
    this.menuS.navigationMenu().subscribe(menuResults=>{
      if(this.generalS.converObjectArrayToArray(menuResults['buttons']).length === 2){
        this.displayMenu = true
      }
      else{
        this.displayMenu = false
      }
      this.routerMenu = this.generalS.converObjectArrayToArray(menuResults['menus'])[2]
    })
  }

  ngOnInit() {
  }

}
