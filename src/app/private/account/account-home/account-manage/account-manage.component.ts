import { Component, OnInit } from '@angular/core';
import {AccountService} from '../../../../services/account.service'

@Component({
  selector: 'app-account-manage',
  templateUrl: './account-manage.component.html',
  styleUrls: ['./account-manage.component.css']
})
export class AccountManageComponent implements OnInit {
  
  constructor(private accountService: AccountService) {}

  ngOnInit() {
  }

}
