import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountPeopleComponent } from './account-people.component';

describe('AccountPeopleComponent', () => {
  let component: AccountPeopleComponent;
  let fixture: ComponentFixture<AccountPeopleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountPeopleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountPeopleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
