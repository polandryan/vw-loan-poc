import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { AccountPeopleService } from '../../../../../services/accountPeople.service'
import { SessionStorageService } from 'angular-web-storage';



@Component({
  selector: 'app-account-people',
  templateUrl: './account-people.component.html',
  styleUrls: ['./account-people.component.css']
})
export class AccountPeopleComponent implements OnInit {
  addPerson = new FormGroup({
    email: new FormControl(),
    tier: new FormControl(),
  })

  accountPeoplesEmail = []
  formGroupRows: FormGroup = this.fb.group({
    rows: this.fb.array([])
  });
  buildRow(email, tier, verified, emailId, accountEmailId){
    return new FormGroup({
      email: new FormControl(email),
      tier: new FormControl(tier),
      verified: new FormControl(verified),
      emailId: new FormControl(emailId),
      accountEmailId: new FormControl(accountEmailId)
    })
  }
  addTableRow(email, tier, verified, emailId, accountEmailId){
    //console.log(createCriteria.linkWorkTitle)
    this.tableRows.push(this.buildRow(email, tier, verified, emailId, accountEmailId));
  }
  get tableRows(){
    return this.formGroupRows.get('rows') as FormArray
  }


  tiers=[{value: 'ACCOUNT_USER', displayName: 'User'},{value: 'ACCOUNT_ADMIN', displayName: 'Administrator'}]
  
  
  constructor(private accountPeople: AccountPeopleService, private session: SessionStorageService, private fb: FormBuilder ) { 
    this.accountPeople.peopleAndEmail(this.session.get('person')['currentAccountId'])
    .then(resolved=>{
      //console.log(JSON.stringify(resolved))
      this.accountPeoplesEmail = resolved['data']
      for(let row of resolved['data']){
        this.addTableRow(row['email'], row['tier'], false, row['emailId'], row['accountEmailId'])
      }
    })
    .catch(rejected=>{console.log(JSON.stringify(rejected))})
  }



  processAddPerson(){
    //alert(this.addPerson.get('tier').value)
    this.accountPeople.addPersonsEmail(this.addPerson.get('email').value, this.addPerson.get('tier').value, this.session.get('person').currentAccountId)
    .then(resolved=>{
      if(resolved['success']){
        console.log('wireup success');
        this.addTableRow(resolved['data']['email'],resolved['data']['tier'],true,resolved['data']['emailId'],resolved['data']['accountEmailId'])
        this.addPerson.get('email').setValue(null)
        this.addPerson.get('tier').setValue(null)
        
      }
      else{
        //console.log('wire up appropriate error' + JSON.stringify(resolved))
      }
    })
    .catch(rejected=>{
      //console.log('wire up errors' + JSON.stringify(rejected))
    })
  }

  ngOnInit(){
  }

  resendInvite(emailId){
    alert('resendInvite for' + emailId)
  }
  
  removeIdxEmailfromAccount(idx){
    //console.log(this.formGroupRows.get('rows').value[idx].accountEmailId)
    this.accountPeople.removePersonsEmail(this.session.get('person').currentAccountId, this.formGroupRows.get('rows').value[idx].emailId, this.formGroupRows.get('rows').value[idx].accountEmailId)
    .then(resolved=>{
      let rows = this.formGroupRows.get('rows') as FormArray
      rows.removeAt(idx)
      //console.log(resolved)
    })
    .catch(rejected=>{console.log(rejected)})
  }
}
