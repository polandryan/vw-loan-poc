import { Component, OnInit } from '@angular/core';
import { MenuService,GeneralService } from 'src/app/services';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-account-manage-home',
  templateUrl: './account-manage-home.component.html',
  styleUrls: ['./account-manage-home.component.css']
})
export class AccountManageHomeComponent implements OnInit {
  displayMenu = false
  routerMenu=[]

  updateAccount = new FormGroup({
    name: new FormControl('dan@dan.com')
  })

  constructor(private menuS: MenuService, private generalS: GeneralService) { 
    this.menuS.navigationMenu().subscribe(menuResults=>{
      if(this.generalS.converObjectArrayToArray(menuResults['buttons']).length === 3){
        this.displayMenu = true
      }
      else{
        this.displayMenu = false
      }
      this.routerMenu = this.generalS.converObjectArrayToArray(menuResults['menus'])[3]
    })
  }


  ngOnInit() {
  }

}
