import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountManageHomeComponent } from './account-manage-home.component';

describe('AccountManageHomeComponent', () => {
  let component: AccountManageHomeComponent;
  let fixture: ComponentFixture<AccountManageHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountManageHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountManageHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
