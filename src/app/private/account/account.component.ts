import { Component, OnInit } from '@angular/core';
import { AccountService} from '../../services/account.service'
import {Router} from '@angular/router'
import { SessionStorageService } from 'angular-web-storage';
import { GeneralService} from '../../services/general.services'
import {MenuService} from '../../services/menu.service'

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  //currentAccounts=[]
  displayMenu= false
  routerMenu=[]

  constructor(private accountS: AccountService, private router: Router, private session: SessionStorageService, private generalS: GeneralService, private menuS: MenuService) {
    this.menuS.navigationMenu().subscribe(menuResults=>{
      if(this.generalS.converObjectArrayToArray(menuResults['buttons']).length === 1){
        this.displayMenu = true
      }
      else{
        this.displayMenu = false

      }
      this.routerMenu = this.generalS.converObjectArrayToArray(menuResults['menus'])[1]
      //console.log(JSON.stringify(this.routerMenu))
    })
 
    /*
    this.accountS.accounts().subscribe(accountResults=>{
      if(accountResults === null){
        this.accountS.updateAccounts(this.session.get('person').accountIds );
      }
      else{
        //console.log(JSON.stringify(accountResults))
        this.currentAccounts = accountResults;

        this.menuS.splitUrl().subscribe(splitUrl=>{
          if(splitUrl.length === 1 && String(splitUrl[0]) === String('account')){
            this.displayMenu = true
          }
          else{
            this.displayMenu = false
          }
          if(splitUrl.length > 1 && String(splitUrl[0]) === String('account')){
            //update navmenu to have account stuff
            //get the old menu & updated it / I don't need to get it because this is the root, but I will in other routers.
            let accountMenu = [
              {value: 'new', display: ' Create New Menu', routerLink: '//account/new'}
            ]
            for(let account of this.currentAccounts ){
              accountMenu.push({value: account['_id'], display: account['name'], routerLink:'//account/' + account['_id']})
            }
            if(this.currentAccounts.length > 1){
              accountMenu.push({value: 'multiple', display: 'Multiple Accounts Dashboard', routerLink:'//account/multiple'})
            }
          }
        })
        //update menu I know it is always position 0, but I don't know if it should be displayed or not at this point, because I don't know if it is in the url

      }   
    })
*/


  }

  ngOnInit() {
  }

}
