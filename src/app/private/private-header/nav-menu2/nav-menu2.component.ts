import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../../services/menu.service'
import { GeneralService} from '../../../services/general.services'

@Component({
  selector: 'app-nav-menu2',
  templateUrl: './nav-menu2.component.html',
  styleUrls: ['./nav-menu2.component.css']
})
export class NavMenu2Component implements OnInit {
  menus=[]
  buttons = []
  constructor(private menuS: MenuService, private generalS: GeneralService) {
    this.menuS.navigationMenu().subscribe(menu2Results=>{
      this.buttons = this.generalS.converObjectArrayToArray(menu2Results['buttons'])
      this.menus = this.generalS.converObjectArrayToArray(menu2Results['menus'])
      //console.log('nav-menus2' + JSON.stringify(this.menus[this.menus.length -1]))
    })   
  }
  ngOnInit() {
  }

}
