import { Component, OnInit } from '@angular/core';
import { SessionStorageService } from 'angular-web-storage';
import { FormGroup, FormControl, FormControlName } from '@angular/forms';
import {AccountService, AuthGuard, GeneralService, MenuService} from '../../services'

@Component({
  selector: 'app-private-header',
  templateUrl: './private-header.component.html',
  styleUrls: ['./private-header.component.css']
})
export class PrivateHeaderComponent implements OnInit {

  selectAccount = new FormGroup({
    selected: new FormControl()
  })
  accountSelectOptions= []
  currentAccountId =''
  currentAccounts =[]


  navigationMenu=[]
  selectedNavigationOptions=[]

  constructor(
    private auth: AuthGuard,
   )
  {
    /*
    this.accountService.updateAccounts(this.session.get('person').accountIds );
   
    this.getAccountsAndURL();
    this.accountService.accounts().subscribe(accountResults=>{
      
      this.accountSelectOptions =[] //value:accountId, name, routerLink: url
      let tempResults = this.general.converObjectArrayToArray(accountResults);
      this.currentAccounts = tempResults;
      let sortedResults = this.general.sortArray(tempResults, [{key: 'name', orderBy: 'asc'}])

      for(let row of sortedResults){
        this.accountSelectOptions.push({value: row._id, name: row.name, routerLink: '/account/' + row._id})
      }
      
      this.accountSelectOptions.push({value: 'CREATE_NEW', name: 'Create New', routerLink: '/account/new'})
      if(this.accountSelectOptions.length > 2){
        this.accountSelectOptions.push({value: 'MULTIPLE', name: 'Select Multiple', routerLink:'/account'})
      }
    })
    this.menuS.navigationMenu().subscribe(navResults =>{ 
      this.navigationMenu= navResults
      //console.log(JSON.stringify(navResults))
    })
    this.menuS.navigationSelections().subscribe(results=>{
      this.selectedNavigationOptions = results
      //console.log(JSON.stringify(results))
    })
    
    this.currentAccountId = this.session.get('person')['currentAccountId']
    
    //Get menuOptions for within an account
  
    this.menuS.navigationMenu2();*/

  }

  ngOnInit() {
    //this.selectAccount.get('selected').setValue(this.session.get('person')['currentAccountId'])
  }

  logout(){
    this.auth.logout();
  }

}
