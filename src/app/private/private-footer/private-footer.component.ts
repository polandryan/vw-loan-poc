import { Component, OnInit } from '@angular/core';
import { LayoutService } from '../../services'

@Component({
  selector: 'app-private-footer',
  templateUrl: './private-footer.component.html',
  styleUrls: ['./private-footer.component.css']
})
export class PrivateFooterComponent implements OnInit {

  currentUiLayout;
  constructor( private layoutService: LayoutService ){
    this.layoutService.uiLayout().subscribe(layout =>{this.currentUiLayout = layout});
   }


  ngOnInit() {
  }

}
