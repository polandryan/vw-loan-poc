import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrowerObligationRouterComponent } from './borrower-obligation-router.component';

describe('BorrowerObligationRouterComponent', () => {
  let component: BorrowerObligationRouterComponent;
  let fixture: ComponentFixture<BorrowerObligationRouterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorrowerObligationRouterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowerObligationRouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
