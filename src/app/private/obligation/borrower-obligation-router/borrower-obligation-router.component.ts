import { Component, OnInit } from '@angular/core';
import { MenuService, GeneralService } from 'src/app/services';

@Component({
  selector: 'app-borrower-obligation-router',
  templateUrl: './borrower-obligation-router.component.html',
  styleUrls: ['./borrower-obligation-router.component.css']
})
export class BorrowerObligationRouterComponent implements OnInit {
  subMenu=[]
  constructor(private menuS: MenuService, private generalS: GeneralService) { 
    this.menuS.navigationMenu().subscribe(menuResults=>{
      this.subMenu = this.generalS.converObjectArrayToArray(menuResults['menus'])[6]
    })
  }


  ngOnInit() {
  }

}
