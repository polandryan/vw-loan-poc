import { Component, OnInit } from '@angular/core';
import {LoanService, GeneralService} from 'src/app/services'
import { Router } from '@angular/router';


@Component({
  selector: 'app-borrower-obligations',
  templateUrl: './borrower-obligations.component.html',
  styleUrls: ['./borrower-obligations.component.css']
})
export class BorrowerObligationsComponent implements OnInit {

  borrowerObligations = []
  constructor( private loanS: LoanService, private router: Router, private generalS: GeneralService) {
    this.loanS.updateLoans(this.router.url.split('/')[4],this.router.url.split('/')[2])
    this.loanS.loans().subscribe(resolved=>{
      //console.log(JSON.stringify(resolved))
      this.borrowerObligations = this.generalS.converObjectArrayToArray(resolved)
    })

  }

  ngOnInit() {
  }

}
