import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrowerObligationsComponent } from './borrower-obligations.component';

describe('BorrowerObligationsComponent', () => {
  let component: BorrowerObligationsComponent;
  let fixture: ComponentFixture<BorrowerObligationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorrowerObligationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowerObligationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
