import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrowerObligationsRouterComponent } from './borrower-obligations-router.component';

describe('BorrowerObligationsRouterComponent', () => {
  let component: BorrowerObligationsRouterComponent;
  let fixture: ComponentFixture<BorrowerObligationsRouterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorrowerObligationsRouterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowerObligationsRouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
