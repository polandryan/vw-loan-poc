import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrowerObligationComponent } from './borrower-obligation.component';

describe('BorrowerObligationComponent', () => {
  let component: BorrowerObligationComponent;
  let fixture: ComponentFixture<BorrowerObligationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorrowerObligationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowerObligationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
