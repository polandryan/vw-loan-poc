import { Component, OnInit } from '@angular/core';
import {MenuService, GeneralService, LoanService} from "src/app/services/index"

@Component({
  selector: 'app-borrower-obligation',
  templateUrl: './borrower-obligation.component.html',
  styleUrls: ['./borrower-obligation.component.css']
})
export class BorrowerObligationComponent implements OnInit {
  navMenu = []
  loan ={}
  constructor(private menuS: MenuService, private generalS: GeneralService, private loanS: LoanService) {
    this.menuS.navigationMenu().subscribe(navResults=>{
      this.navMenu = this.generalS.converObjectArrayToArray(navResults['menus'])[6]
      //console.log(JSON.stringify(this.navMenu  ))
    })
    this.loanS.loan().subscribe(loanResults=>{
      this.loan = loanResults
    }
    )
   }

  ngOnInit() {
  }

}
