import { Component, OnInit } from '@angular/core';
import {LoanService} from '../../../services/loan.service'
import { GeneralService } from 'src/app/services';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import * as moment from 'moment'


@Component({
  selector: 'app-create-borrower-loan',
  templateUrl: './create-borrower-loan.component.html',
  styleUrls: ['./create-borrower-loan.component.css']
})
export class CreateBorrowerLoanComponent implements OnInit {
  menuForm = new FormGroup({
    advancedOptions: new FormControl('basic')
  })
  addAsset = new FormGroup({
    make: new FormControl(),
    model: new FormControl(),
    year: new FormControl(),
    vin: new FormControl(),
    additionalInformation: new FormControl(),
    loanPrincipal: new FormControl(),
  });
  
  addTerm = new FormGroup({
    contractCreatedDateTime: new FormControl(moment({milliseconds: 0}).utc().format()),
    interestAccruedDateTime: new FormControl(moment({milliseconds: 0}).utc().format()),
    projectedFirstPaymentDateTime: new FormControl(moment({milliseconds: 0}).add(1, 'M').utc().format()),
    projectedLastPaymentDateTime: new FormControl(), 
    intervals: new FormControl(12),
    annualInterestRate: new FormControl(),
    intervalLabel: new FormControl('monthly'),
    compoundLabel: new FormControl('simple'),
    daysPerYearLabel: new FormControl('real/365'), //intervalsPerYear
    intervalPayment: new FormControl(),
    intervalPaymentCutOrRound: new FormControl('round'),
    lastPaymentLabel: new FormControl('lastPaymentMatchesPrincipal'),
    lastPaymentCutOrRound: new FormControl('cut'),
    debt: new FormControl(),
    loanDebt: new FormControl(),
    principal: new FormControl(),
    interest: new FormControl(),
    feeDebt: new FormControl(),
    initialInterestAccrualGreaterthanIntervalLabel: new FormControl('paymentPercentage'), 

    projectedTotalAllPayments: new FormControl(),
    projectedTotalAllInterest: new FormControl(),
    projectedTotalAllPrincipal: new FormControl(),



    originationDate: new FormControl(moment({milliseconds: 0}).utc().format()), //remove this field
    totalIntervals: new FormControl(12), //remove this field
    paymentFrequency: new FormControl('monthly'), //remove this field
    compoundFrequency: new FormControl('simple'), //remove this field
    
  })
  loanSummary
  /* = {
    recorded:{
      enteredCreatedDateTime: null,
      interestAccruedDateTime: null,
      projectedFirstPaymentDateTime: null,
      projectedLastPaymentDateTime: null,
      lastPaymentCutOrRound: null,
      lastPaymentLabel: null,
      lastPayment: null,
      intervals: null,
      intervalLabel: null,
      intervalPayment: null,
      intervalPaymentCutOrRound: null,
      daysPerYearLabel: null,
      annualInterestRate: null,
      compoundLabel: 'simple/exact',
      initialInterestAccrualGreaterthanIntervalLabel: 'intervalPercentage',
      //Initial Financial
      debt: null,
      loanDebt: null,
      principal: null,
      interest: null,
      feeDebt: null,
      display: {
          debt: null,
          loanDebt: null,
          principal: null,
          interest: null,
          feeDebt: null,
      },    
      projectedPaid:{ //Rounded based on schedule
          debt: null,
          loanDebt: null,
          principal: null,
          interest: null,
          feeDebt: null,
          payments:null,
          Formulas:{ //Standard Formulas
              payments: null,
              interest: null,
          },
      },
      schedule:[],
    },
    interestAccruedDateTime: null,
    paid:{
        payment: '0.00',
        paymentInterest: '0.00',
        paymentPrincipal: '0.00',
        paymentFee: '0.00',
        payments: '0.00',
        debt: '0.00',
        loanDebt: '0.00',
        principal: '0.00',
        interest: '0.00',
        feeDebt: '0.00',
        display: {
            paymentInterest: '0.00',
            paymentPrincipal: '0.00',
            paymentFee: '0.00',
            debt: '0.00',
            loanDebt: '0.00',
            principal: '0.00',
            interest: '0.00',
            feeDebt: '0.00',
        },
    },
    remaining:{
        debt: null,
        loanDebt: null,
        principal: null,
        interest: null,
        feeDebt: null,
        display: {
            debt: null,
            loanDebt: null,
            principal: null,
            interest: null,
            feeDebt: null,
        }
    },
    deviation:{ //scheduled vs current
        debt: '0.00',
        loanDebt: '0.00',
        principal: '0.00',
        interest: '0.00',
        feeDebt: '0.00',
        display: {
            debt: '0.00',
            loanDebt: '0.00',
            principal: '0.00',
            interest: '0.00',
            feeDebt: '0.00',
        }
      }
    }
  */
  proposedSchedule = []



  constructor(private loanS: LoanService, private generalS: GeneralService, private router: Router ) {}

  ngOnInit() {
    this.onTermChanges();
  }

  onTermChanges(): void {
    this.addTerm.valueChanges.subscribe(val => {
      
      //Go get term, the term service should validate if there is enough data
      //But perhaps I should do some simple validation also I should do this maybe not on change but on updates
      let loanObj={
        debt: this.addTerm.get('debt').value,
        loanDebt: this.addTerm.get('loanDebt').value,
        principal: this.addTerm.get('principal').value,
        interest: this.addTerm.get('interest').value,
        feeDebt: this.addTerm.get('feeDebt').value,
        contractCreatedDateTime: moment(this.addTerm.get('contractCreatedDateTime').value).unix(),
        interestAccruedDateTime: moment(this.addTerm.get('interestAccruedDateTime').value).unix(),
        projectedFirstPaymentDateTime: moment(this.addTerm.get('projectedFirstPaymentDateTime').value).unix(),
        projectedLastPaymentDateTime: moment(this.addTerm.get('projectedLastPaymentDateTime').value).unix(),
        intervals: this.addTerm.get('intervals').value,
        intervalLabel: this.addTerm.get('intervalLabel').value,
        annualInterestRate: this.addTerm.get('annualInterestRate').value,
        intervalPayment: this.addTerm.get('intervalPayment').value,
        daysPerYearLabel: this.addTerm.get('daysPerYearLabel').value,
        lastPaymentLabel: this.addTerm.get('lastPaymentLabel').value,
        intervalPaymentCutOrRound: this.addTerm.get('intervalPaymentCutOrRound').value,
        lastPaymentCutOrRound: this.addTerm.get('lastPaymentCutOrRound').value,
        initialInterestAccrualGreaterthanIntervalLabel: this.addTerm.get('initialInterestAccrualGreaterthanIntervalLabel').value, 
        
      }
      if(this.addTerm.get('intervalPayment').value === '' || this.addTerm.get('intervalPayment').value === null){
        loanObj.intervalPayment = null
      }
      if(this.menuForm.get('advancedOptions').value === 'basic'){
        loanObj.interestAccruedDateTime = loanObj.contractCreatedDateTime
      }
      if(!(String(this.addTerm.get('annualInterestRate').value) === 'null') && !(String(this.addTerm.get('principal').value) === 'null') && !(val === 'loanDebt')){
        this.loanS.checkCreateLoan(loanObj)
        .then((resolved)=>{
          if(resolved['success']){
            console.log('createBorrowerLoanTS>checkCreateLoan' + JSON.stringify(resolved['data']))
            this.loanSummary = resolved['data']
            //this.addTerm.get('loanDebt').setValue(this.loanSummary['recorded']['loanDebt'])
          }
        })
        .catch((rejected)=>{})
        console.log('changed term' +  JSON.stringify(loanObj))
      }
      

    })
  }

}
