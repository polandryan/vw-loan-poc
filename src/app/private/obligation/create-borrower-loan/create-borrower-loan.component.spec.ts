import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateBorrowerLoanComponent } from './create-borrower-loan.component';

describe('CreateBorrowerLoanComponent', () => {
  let component: CreateBorrowerLoanComponent;
  let fixture: ComponentFixture<CreateBorrowerLoanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateBorrowerLoanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateBorrowerLoanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
