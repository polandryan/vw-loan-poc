import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListBorrowerLoansComponent } from './list-borrower-loans.component';

describe('ListBorrowerLoansComponent', () => {
  let component: ListBorrowerLoansComponent;
  let fixture: ComponentFixture<ListBorrowerLoansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListBorrowerLoansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListBorrowerLoansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
