import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import {LoanService, ActsService} from 'src/app/services';
import { Router} from '@angular/router'

import * as moment from 'moment'

@Component({
  selector: 'app-create-financial-event',
  templateUrl: './create-financial-event.component.html',
  styleUrls: ['./create-financial-event.component.css']
})
export class CreateFinancialEventComponent implements OnInit {
  defaultDate

  financialForm = new FormGroup({
    date: new FormControl(this.defaultDate),
    amount: new FormControl(),
    type: new FormControl(),
    comment: new FormControl(),
    excessAmount: new FormControl(),
  });

  financialEventOptions=[]
  
  constructor( private loanS: LoanService, private router: Router, private actS: ActsService) { 
    this.financialEventOptions = this.actS.financialCreateEventOptions
    this.loanS.loan().subscribe(resolvedLoan=>{
      if(resolvedLoan){
        this.defaultDate = moment.unix(resolvedLoan['when']['interestAccrued']).format('MM/DD/YYYY')
      //this.financialForm['date'].setValue(, {onlySelf: true, emitEvent})
      }

    })
  }

  ngOnInit() {
    
  }

  addFinancialTransaction(){
    //get from the URL who the transaction should be added for.

    let financialEvent = {
      accountId: this.router.url.split('/')[2],
      personId: this.router.url.split('/')[4],
      obligationId: this.router.url.split('/')[6],
      type: 'financial,'+ this.financialForm.get('type').value, 
      dateTime: moment(this.financialForm.get('date').value).unix(),
      amount: this.financialForm.get('amount').value,
      createdBy: {personId: '234234', when: moment().unix()},
      excessAmount: this.financialForm.get('excessAmount').value
    }
    
    if(this.financialForm.get('type').value === 'customer_payment'){
      this.loanS.loanCustomerPayment(financialEvent)
    }
    if (String(this.financialForm.get('type').value).includes('fee')  ){
      //console.log('create-financial-event->Fee')
      this.loanS.loanFee(financialEvent)
    }
    if(String(this.financialForm.get('type').value).includes('principal')){
      this.loanS.loanPrincipal(financialEvent)
    }

      this.financialForm.reset()  
      //need to get what acts should be displayed... Use Session or BehaviorSubject? Session probably better

      this.actS.readActsFromDB(null,this.router.url.split('/')[6],null, ['financial','communication','onlyNext' ])
        

  }

  
  
}
