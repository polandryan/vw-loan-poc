import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFinancialEventComponent } from './create-financial-event.component';

describe('CreateFinancialEventComponent', () => {
  let component: CreateFinancialEventComponent;
  let fixture: ComponentFixture<CreateFinancialEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateFinancialEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFinancialEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
