import { Component, OnInit } from '@angular/core';
import { ActsService, LoanService, GeneralService, SettingService } from 'src/app/services'
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { Router} from '@angular/router'
import * as moment from 'moment'


@Component({
  selector: 'app-obligation-events',
  templateUrl: './obligation-events.component.html',
  styleUrls: ['./obligation-events.component.css']
})
export class ObligationEventsComponent implements OnInit {
  currentDisplayFormtValues={dateOrderBy: 'reverse_chronological',  displayEventType:['financial','communication','onlyNext']}
  displayForm = new FormGroup({
    dateOrderBy: new FormControl('reverse_chronological'),
    displayEventType: new FormControl(['financial','communication','onlyNext' ])
  });


  events=[]
  loan ={};
  
  constructor( private loanS: LoanService, private generalS: GeneralService, private router: Router, private actS: ActsService, private settingS: SettingService) {
  
    this.actS.readActsFromDB(null,this.router.url.split('/')[6], null, this.displayForm.get('displayEventType').value)
    this.actS.acts().subscribe(resolved=>{
     this.events = this.generalS.sortArray(this.generalS.converObjectArrayToArray(resolved), [{key: 'dateTime', orderBy: 'DESC'}])
    })
    this.loanS.loan().subscribe(loanResults=>{
      
      if(loanResults){
        //console.log('have a loan' + JSON.stringify(loanResults))
        this.loan = loanResults
      }
      else{//console.log('loan is null')
    }
     
    })

    this.settingS.settings().subscribe(resolvedSettings=>{
      if(Object.keys(resolvedSettings).includes('eventDisplayTypes')){
        //console.log('has default settings')
      }
      else{
        //console.log('no default settings')

        this.settingS.updateLocalSettings({'eventDisplayTypes': ['financial','communication','onlyNext' ]})
      }
    })
  }

  ngOnInit() {
    this.onChanges();
    
  }
  onChanges(): void{
    this.displayForm.valueChanges.subscribe(valueChange=>{
      
      if(!(String(valueChange['dateOrderBy']) === String(this.currentDisplayFormtValues.dateOrderBy)) ){
        if(valueChange['dateOrderBy'] ==='reverse_chronological'){
          this.events = this.generalS.sortArray(this.events, [{key: 'dateTime', orderBy: 'DESC'}])
        }
        else{
          this.events = this.generalS.sortArray(this.events, [{key: 'dateTime', orderBy: 'ASC'}])
        }  
        this.currentDisplayFormtValues.dateOrderBy = valueChange['dateOrderBy']
        console.log('orderChanged')
      }
      if(!(String(valueChange['displayEventType']) === String(this.currentDisplayFormtValues.displayEventType)) ){
        console.log('included values Changed')
        this.actS.readActsFromDB(null,this.router.url.split('/')[6],null, valueChange['displayEventType'])
        this.currentDisplayFormtValues.displayEventType = valueChange['displayEventType']
      }

      
      //if(valueChange)
    })
  }
  makePayment(dateTime, amount){
    console.log(dateTime + '|' +amount);
    let financialEvent = {
      accountId: this.router.url.split('/')[2],
      personId: this.router.url.split('/')[4],
      obligationId: this.router.url.split('/')[6],
      type: 'financial, customer_payment', 
      dateTime: dateTime,
      amount: amount,
      createdBy: {personId: '234234', when: moment().unix()}
    }
    if(this.loanS.loanCustomerPayment(financialEvent)){
      this.actS.readActsFromDB(null,this.router.url.split('/')[6], null, this.displayForm.get('displayEventType').value)
    }
    
  }
}
