import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObligationEventsComponent } from './obligation-events.component';

describe('ObligationEventsComponent', () => {
  let component: ObligationEventsComponent;
  let fixture: ComponentFixture<ObligationEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObligationEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObligationEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
