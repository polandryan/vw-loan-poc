import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCommunicationEventComponent } from './create-communication-event.component';

describe('CreateCommunicationEventComponent', () => {
  let component: CreateCommunicationEventComponent;
  let fixture: ComponentFixture<CreateCommunicationEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCommunicationEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCommunicationEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
