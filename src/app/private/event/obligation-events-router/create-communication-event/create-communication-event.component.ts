import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { ActsService } from 'src/app/services';
import { Router} from '@angular/router'
import * as moment from 'moment'

@Component({
  selector: 'app-create-communication-event',
  templateUrl: './create-communication-event.component.html',
  styleUrls: ['./create-communication-event.component.css']
})
export class CreateCommunicationEventComponent implements OnInit {
  communicationForm = new FormGroup({
    date: new FormControl(),
    comment: new FormControl(),
    method: new FormControl(),
    direction: new FormControl('outbound'),
    tags: new FormControl(),
    state: new FormControl('open'),
  });
  directionOptions = [
    {value: 'outbound', display: 'Outbound', order: 0},
    {value: 'inbound', display: 'Inbound', order: 1},
   ]
   stateOptions=[
     {value: 'open', display: 'Open', order: 0},
     {value: 'closed', display: 'Closed', order: 1},
  ]
  methodOptions=[
    {value: 'voice', display: 'Voice', order: 0},
    {value: 'email', display: 'Email', order: 1},
    {value: 'txt', display: 'Text', order: 2},
 ]
 dispositionOptions=[
  {value: 'late', display: 'Late', order: 0},
  {value: 'reminder', display: 'Remider', order: 1},
  {value: 'badtogood', display: 'Attitude Bad to Good', order: 2},
  {value: 'goodtobad', display: 'Attitude Good to Bad', order: 3},
 ]

  constructor( private actS: ActsService, private router: Router) { }

  ngOnInit() {
  }

  resetcommunicationTransaction(){
    this.communicationForm.reset()
  }
  addCommunicationTransaction(){
    //console.log('add' + this.communicationForm.get('date').value)
    this.actS.updateDBAct({
      accountId: this.router.url.split('/')[2],
      personId: this.router.url.split('/')[4],
      obligationId: this.router.url.split('/')[6],
      dateTime: moment(this.communicationForm.get('date').value).unix(),
      method: this.communicationForm.get('method').value,
      tags: this.communicationForm.get('tags').value,
      type: 'communication',
      comment: this.communicationForm.get('comment').value,
      direction: this.communicationForm.get('direction').value,
      state: this.communicationForm.get('state').value,
    })
    .then(resolved=>{
      this.actS.readActsFromDB(null,this.router.url.split('/')[6],null,  ['financial','communication','onlyNext' ])

      //console.log(resolved)
    })
    .catch(rejected=>{console.log(rejected)})
  }
}
