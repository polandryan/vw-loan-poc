import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObligationEventRouterComponent } from './obligation-event-router.component';

describe('ObligationEventRouterComponent', () => {
  let component: ObligationEventRouterComponent;
  let fixture: ComponentFixture<ObligationEventRouterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObligationEventRouterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObligationEventRouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
