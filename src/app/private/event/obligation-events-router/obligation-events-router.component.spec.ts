import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObligationEventsRouterComponent } from './obligation-events-router.component';

describe('ObligationEventsRouterComponent', () => {
  let component: ObligationEventsRouterComponent;
  let fixture: ComponentFixture<ObligationEventsRouterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObligationEventsRouterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObligationEventsRouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
