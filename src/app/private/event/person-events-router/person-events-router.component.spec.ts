import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonEventsRouterComponent } from './person-events-router.component';

describe('PersonEventsRouterComponent', () => {
  let component: PersonEventsRouterComponent;
  let fixture: ComponentFixture<PersonEventsRouterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonEventsRouterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonEventsRouterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
