import { Component, HostListener } from '@angular/core';
import { LayoutService } from 'src/app/services'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor( private layoutService: LayoutService ){
   }
  @HostListener('window:resize') onresize(){
    this.layoutService.updateUiLayout(document.documentElement.clientWidth, document.documentElement.clientHeight);
  }

}
