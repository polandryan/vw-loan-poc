import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitiateAuthenticationComponent } from './initiate-authentication.component';

describe('InitiateAuthenticationComponent', () => {
  let component: InitiateAuthenticationComponent;
  let fixture: ComponentFixture<InitiateAuthenticationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitiateAuthenticationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitiateAuthenticationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
