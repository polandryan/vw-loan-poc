import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthGuard } from 'src/app/services'
import { Router } from '@angular/router';


@Component({
  selector: 'app-initiate-authentication',
  templateUrl: './initiate-authentication.component.html',
  styleUrls: ['./initiate-authentication.component.css']
})
export class InitiateAuthenticationComponent implements OnInit {
  registerPrompt=false // display register or go back if no email&password was found. there might be an email that was found...


  emailPassword = new FormGroup({
    email: new FormControl(),
    password: new FormControl()
  });

  constructor( private auth: AuthGuard, private router: Router) { }

  ngOnInit() {
  }

  processEmailPassword(){
    //1) Check email & password if match then log in
    //2) if no match go to account create 
    this.auth.processEmailPassword(this.emailPassword.get('email').value, this.emailPassword.get('password').value)
    .then(resolved=>{
      //console.log(resolved['message'])
      if(resolved['success']){ //Email, Password matched and account was verified
        let urlPath ='/account/' + resolved['data']['currentAccountId']
        this.router.navigate([urlPath])
      }
      else if(String(resolved['message']) === String('email_needs_verification')){ // Password Matched, but Account needs verification before continuing
          let urlPath = '/verify/' + resolved['data']['urlParam']
          this.router.navigate([urlPath])

      }
      else if(String(resolved['message']) === String('email_not_found')){ // email & password didn't Match || email didn't exist
          this.registerPrompt = true
      }
      else{
        this.router.navigate(['/'], { queryParams: {emailPassword : false } });
      }
        
      
    })
    .catch(rejected=>{
      //console.log('rejected processEmailPassword')
      this.router.navigate(['/'])
    })


  }
  accountCreate(){
    //The person might just need to confirm their verification code, but not know what it is.
  
    this.auth.accountCreate(this.emailPassword.get('email').value, this.emailPassword.get('password').value)
    .then(registerResults=>{
      //console.log('res:' + JSON.stringify(registerResults))
      let verifyPath = '/verify/' + registerResults['data']['verification']['urlParam']
      this.router.navigate([verifyPath])
  })
    .catch(regsiterRejected=>{console.log('rej:' + JSON.stringify(regsiterRejected))})
  }

}
