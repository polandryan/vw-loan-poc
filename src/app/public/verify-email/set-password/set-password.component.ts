import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthGuard } from 'src/app/services'
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-set-password',
  templateUrl: './set-password.component.html',
  styleUrls: ['./set-password.component.css']
})
export class SetPasswordComponent implements OnInit {
  verification = new FormGroup({
    code: new FormControl(),
    password: new FormControl()
  });
  verificationUrlParam = ''

  constructor(private activatedRoute: ActivatedRoute, private authGuard: AuthGuard,) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params=>{ this.verificationUrlParam = params.get("id")})
  }

  verifyCodeAndPassword(){
    this.authGuard.verificationUrlParamCode(this.verificationUrlParam, this.verification.get('code').value, this.verification.get('password').value)
  }

}
