import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthGuard } from 'src/app/services'
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.css']
})
export class VerifyEmailComponent implements OnInit {
  verification = new FormGroup({
    code: new FormControl(),
  });
  verificationUrlParam = ''
  issue = false

  constructor(private activatedRoute: ActivatedRoute, private authGuard: AuthGuard, private router: Router) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params=>{ this.verificationUrlParam = params.get("id")

    })
  }
  verifyCode(){
    //console.log('verify' + this.verification.get('code').value + ' | ' + this.verificationUrlParam)
    this.authGuard.verificationUrlParamCode(this.verificationUrlParam, this.verification.get('code').value, null)
    .then(resolved=>{
      //console.log(JSON.stringify(resolved));
      if(resolved['success']){
        console.log('go back to login page')
        this.router.navigate(['/'])
      }
      else{console.log('issue')}
    })
    .catch(rejected=>{
      console.log('verification' + rejected['message'])
    })
  }

}
