import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { AppComponent } from './app.component';
import { AppRouters } from './app.routing'
import { RouterModule } from '@angular/router'
import { MaterialModule } from "./app.material";
import { HttpClientModule } from '@angular/common/http';
import { MomentModule } from 'angular2-moment'
import { AngularWebStorageModule } from 'angular-web-storage';
import { LayoutService, AccountService, AccountPeopleService, GeneralService, BorrowerService, LoanService, MenuService, ActsService, SettingService} from '../app/services/index'


import { PublicComponent } from './public/public.component';
import { PrivateComponent } from './private/private.component';
import { InitiateAuthenticationComponent } from './public/initiate-authentication/initiate-authentication.component';
import { VerifyEmailComponent } from './public/verify-email/verify-email.component';
import { HomeComponent } from './private/home/home.component';
import { ProfileComponent } from './private/profile/profile.component';
import { AccountComponent } from './private/account/account.component';
import { AccountListComponent } from './private/account/account-list/account-list.component';
import { AccountHomeComponent } from './private/account/account-home/account-home.component';
import { PrivateHeaderComponent } from './private/private-header/private-header.component';
import { PrivateFooterComponent } from './private/private-footer/private-footer.component';
import { AccountHomeLeftNavComponent } from './private/account/account-home/account-home-left-nav/account-home-left-nav.component';
import { AccountManageComponent } from './private/account/account-home/account-manage/account-manage.component';
import { AccountHomebaseComponent } from './private/account/account-homebase/account-homebase.component';
import { AccountPeopleComponent } from './private/account/account-home/account-manage/account-people/account-people.component';
import { AccountManageHomeComponent } from './private/account/account-home/account-manage/account-manage-home/account-manage-home.component';
import { SetPasswordComponent } from './public/verify-email/set-password/set-password.component';
import { AccountNewComponent } from './private/account/account-new/account-new.component';
import { BorrowerComponent } from './private/borrower/borrower.component';
import { CreateBorrowerComponent } from './private/borrower/create-borrower/create-borrower.component';
import { ListBorrowersComponent } from './private/borrower/list-borrowers/list-borrowers.component';
import { ObligationComponent } from './private/obligation/obligation.component';
import { CreateBorrowerLoanComponent } from './private/obligation/create-borrower-loan/create-borrower-loan.component';
import { BorrowerManageComponent } from './private/borrower/borrower-manage/borrower-manage.component';
import { BorrowerObligationComponent } from './private/obligation/borrower-obligation/borrower-obligation.component';
import { ManageBorrowersComponent } from './private/borrower/manage-borrowers/manage-borrowers.component';
import { BorrowerOverviewComponent } from './private/borrower/borrower-overview/borrower-overview.component';
import { BorrowerObligationRouterComponent } from './private/obligation/borrower-obligation-router/borrower-obligation-router.component';
import { ListBorrowerLoansComponent } from './private/obligation/list-borrower-loans/list-borrower-loans.component';
import { BorrowerObligationsRouterComponent } from './private/obligation/borrower-obligations-router/borrower-obligations-router.component';
import { BorrowerObligationsComponent } from './private/obligation/borrower-obligations/borrower-obligations.component';
import { EventRouterComponent } from './private/event/event-router/event-router.component';
import { ObligationEventsRouterComponent } from './private/event/obligation-events-router/obligation-events-router.component';
import { PersonEventsRouterComponent } from './private/event/person-events-router/person-events-router.component';
import { ObligationEventsComponent } from './private/event/obligation-events-router/obligation-events/obligation-events.component';
import { ObligationEventRouterComponent } from './private/event/obligation-events-router/obligation-event-router/obligation-event-router.component';
import { CreateFinancialEventComponent } from './private/event/obligation-events-router/create-financial-event/create-financial-event.component';
import { CreateCommunicationEventComponent } from './private/event/obligation-events-router/create-communication-event/create-communication-event.component';
import { NavMenu2Component } from './private/private-header/nav-menu2/nav-menu2.component';



@NgModule({
  declarations: [
    AppComponent,
    PublicComponent,
    PrivateComponent,
    InitiateAuthenticationComponent,
    VerifyEmailComponent,
    HomeComponent,
    ProfileComponent,
    AccountComponent,
    AccountListComponent,
    AccountHomeComponent,
    PrivateHeaderComponent,
    PrivateFooterComponent,
    AccountHomeLeftNavComponent,
    AccountManageComponent,
    AccountHomebaseComponent,
    AccountPeopleComponent,
    AccountManageHomeComponent,
    SetPasswordComponent,
    AccountNewComponent,
    BorrowerComponent,
    CreateBorrowerComponent,
    ListBorrowersComponent,
    ObligationComponent,
    CreateBorrowerLoanComponent,
    BorrowerManageComponent,
    BorrowerObligationComponent,
    ManageBorrowersComponent,
    BorrowerOverviewComponent,
    BorrowerObligationRouterComponent,
    ListBorrowerLoansComponent,
    BorrowerObligationsRouterComponent,
    BorrowerObligationsComponent,
    EventRouterComponent,
    ObligationEventsRouterComponent,
    PersonEventsRouterComponent,
    ObligationEventsComponent,
    ObligationEventRouterComponent,
    CreateFinancialEventComponent,
    CreateCommunicationEventComponent,
    NavMenu2Component,
   
  ],
  imports: [
    AppRouters,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MomentModule,
    HttpClientModule,
    MaterialModule,
    RouterModule,
    AngularWebStorageModule,
  
  ],
  providers: [LayoutService, AccountService, AccountPeopleService, GeneralService, BorrowerService, LoanService, MenuService, ActsService,SettingService],
  bootstrap: [AppComponent],
  entryComponents:[]
})
export class AppModule { }
