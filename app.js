var express = require('express');
var path = require('path');

var mongoose = require('mongoose');
//var fs = require('fs');

//var allRoutes= require('./api/routes/index');
var personRoutes = require('./api/routes/person');
var emailRoutes = require('./api/routes/email');
var accountRoutes = require('./api/routes/account');
var obligationRoutes = require('./api/routes/obligation');
var borrowerRoutes = require('./api/routes/borrower');
var actRoutes = require('./api/routes/act')

var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 3000; 



//Database
mongoose.connect('mongodb://localhost:27017/loan', {useNewUrlParser: true})

//LOAD ALL files in MODELS
require('./api/models/person');
require('./api/models/email');
require('./api/models/account');
require('./api/models/obligation');
require('./api/models/act');

app.use('/api/person', personRoutes);
app.use('/api/email', emailRoutes);
app.use('/api/account', accountRoutes);
app.use('/api/obligation', obligationRoutes);
app.use('/api/borrower', borrowerRoutes);
app.use('/api/act', actRoutes);


app.listen(port);
console.log('Magic happens on port ' + port);