var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
    const insertOptions = {upsert: true, new: true, useFindAndModify: false}
    const updateOptions = {upsert: false, new: true, useFindAndModify: false}
var moment = require('moment');

var ObligationCalcController = require('../controllers/obligation_calculations.ts')


exports.orchistrateCreateLoan = function(createLoanObj){
    return orchistrateCreateLoan(createLoanObj)
}
function orchistrateCreateLoan(createLoanObj){return new Promise(async (res,rej)=>{
    let returnObj = {success: false, data: null, message: 'no_orchistration'}
    //get amortization table
    //exports.calcAmortizationTable = function(interestFromDate, firstPaymentDate,amount,annualInterestRate,intervals,intervalFreqLabel,compoundLabel,percentDayLabel,endDate,)
    let dataAmortizationTable = await ObligationCalcController.calcAmortizationTable(createLoanObj.contractStartInterestAccuralDateTime, createLoanObj.projectedFirstPaymentDateTime, createLoanObj.principal, createLoanObj.annualInterestRate, createLoanObj.intervals, createLoanObj.intervalFreqLabel, createLoanObj.compoundLabel, createLoanObj.percentDayLabel, createLoanObj.endDate,)
    .catch((err)=>{console.log(err)})
    //OUT: res({success: true, data: {intervalPayment: payment, totalAllPayments: totalAllPayments, totalAllInterest: totalAllInterest, totalAllPrincipal: totalAllPrincipal, table: table}, message:'calc_amortization'})
    
    if(dataAmortizationTable['success']){
        createLoanObj['intervalPayment'] = dataAmortizationTable['data']['intervalPayment']
        createLoanObj['projectedTotalAllPayments'] = dataAmortizationTable['data']['totalAllPayments']
        createLoanObj['projectedTotalAllInterest'] = dataAmortizationTable['data']['totalAllInterest']
        createLoanObj['projectedTotalAllPrincipal'] = dataAmortizationTable['data']['totalAllPrincipal']

        createLoanObj['amortizationTable'] = dataAmortizationTable['data']['table']
        createLoanObj['projectedEndDateTime'] = dataAmortizationTable['data']['projectedEndDateTime']
    }
   
    
    //console.log('Orchistration '+ JSON.stringify(createLoanObj))

    res(returnObj)
})}


exports.findPersonLoans = function(findCriteria){
    //console.log(JSON.stringify(findCriteria))
    return new Promise((res, rej)=>{
        mongoose.model('obligation').find(findCriteria).exec()
        .then(resolved=>{
            if(resolved.length > 0){
                returnObj = {success: true, data: resolved, message:'loans_found'}
               // console.log(JSON.stringify(returnObj))
                res(returnObj)
            }
            else{
                returnObj ={success: false, data: null, message:'no_loan_found for ' + JSON.stringify(findCriteria) }
                //console.log(JSON.stringify({success: false, data: null, message:'no_email_found'}))
                res(returnObj)
            }
        })
        .catch(rejected=>{
            rej({success: false, data: rejected, message:'db_rejection_error'})
        })
    })   
}
exports.findLoan = function(findCriteria){
    return new Promise((res, rej)=>{
        mongoose.model('obligation').find(findCriteria).exec()
        .then(resolved =>{
            
            if(resolved.length === 1){
                res({success: true, data: resolved[0], message: 'loan_found'})
            }
            else{
                res({success: false, data: null, message: 'loan_not_found'})
            }
        })
        .catch(rejected =>{
            rej({success: false, data: null, message: 'loan_error'})
        })
    })
}

exports.createLoan = function(findCriteria, createCriteria){
    return createLoan(findCriteria, createCriteria)
}
function createLoan(findCriteria, createCriteria){return new Promise((res, rej)=>{
    //console.log(JSON.stringify(findCriteria) + '|' + JSON.stringify(createCriteria) )

    mongoose.model('obligation').findOneAndUpdate(findCriteria, {$set: createCriteria}, insertOptions).exec()
    .then(resolved=>{
        // console.log(JSON.stringify(resolved))
        res({success: true, data: resolved, message: 'obligation_created'})
    })
    .catch(rejected=>{
        // console.log(JSON.stringify(rejected))
        rej({success: false, data: rejected, message: 'obligation_not_created'})
    })
})}

exports.updateLoan = function (findCriteria, updateCriteria){
    return new Promise((res, rej)=>{
        mongoose.model('obligation').findOneAndUpdate(findCriteria, updateCriteria, updateOptions).exec()
        .then(resolved=>{
            res({success: true, data: resolved, message: 'loan_updated'})
        })
        .catch(rejected=>{ 
            rej({success: false, data: rejected, message: 'loan_not_updated'})
        })
    })
}
exports.replaceLoan = function (findCriteria, updateCriteria){
    return new Promise((res, rej)=>{
        mongoose.model('obligation').replaceOne(findCriteria, updateCriteria).exec()
        .then(resolved=>{
            res({success: true, data: resolved, message: 'loan_updated'})
        })
        .catch(rejected=>{ 
            rej({success: false, data: rejected, message: 'loan_not_updated'})
        })
    })
}

exports.convertLabelToInterval = function(compoundLabel, frequencyLabel){
    //Monthly = 12
    //daily = 365
    //continuiously = 1000000
    return new Promise((res, rej)=>{
        if( compoundLabel === 'simple' || compoundLabel === null){
            if(frequencyLabel === 'monthly'){
                res(12)
            }
            else if(frequencyLabel === 'weekly'){
                res(52)
            }
            else if(frequencyLabel === 'biweekly'){
                res(26)
            }
            else if(frequencyLabel === 'daily'){
                res(365)
            }
            else if(frequencyLabel === 'continuously'){
                res(1000000)
            }
            else{
                rej('error')
            }
        }
        else{
            if(compoundLabel === 'monthly'){
                res(12)
            }
            else if(compoundLabel === 'biweekly'){
                res(26)
            }
            else if(compoundLabel === 'weekly'){
                res(52)
            }
            else if(compoundLabel === 'daily'){
                res(365)
            }
            else if(compoundLabel === 'continuously'){
                res(1000000)
            }
            else{
                rej('error')
            }
        }

    }) 
}

function calcDaysInYear(year){
    let days = 0
    for(let month = 1; month <= 12; month++){
        if(month < 10){
            stringMonth = '0' + String(month)
        }
        else{
            stringMonth = String(month)
        }
        let stringDate = String(year) + '-' + stringMonth
        days = days + moment(stringDate, 'YYYY-MM').daysInMonth()
    }    
    return days
}

function calcPercentOf360(fromDateTime, toDateTime){
    return new Promise((res, rej)=>{
        let days360 = 360 * (Number(moment.unix(toDateTime).format('YYYY')) - Number(moment.unix(fromDateTime).format('YYYY')) ) +  30 * ( Number(moment.unix(toDateTime).format('MM')) - Number(moment.unix(fromDateTime).format('MM') ) ) + Number(moment.unix(toDateTime).format('DD') ) - Number( moment.unix(fromDateTime).format('DD'))   
        res(days360 / 360)
    })
}

exports.calcAccruedInterest = async function(fromDateTime, toDateTime, annualInterestRate, amount ){   
    let percent = await calcPercentOf360(fromDateTime, toDateTime)
    return (percent * annualInterestRate * amount)
}


exports.calcPayment = function(inP, inR, inN){
    /*  p = principal loan amount
        r = interest rate per period 
        n = total number of periods 
     */
    //console.log(inP + '|' + inR + '|' + inN)
    let p = Number(inP)
    let r = Number(inR)
    let n = Number(inN)
    return new Promise((res, rej)=>{
        if(p === 0 || r === 0 || n === 0 ){ //Numbers are weird
            res('error')
        }
        else{
           res( this.truncNumber( Number( (p * ( Number((r * Number(Math.pow(1.0 + r,n ).toFixed(13)) ).toFixed(13)) / ( Number(Math.pow(1.0 + r, n).toFixed(13))   - 1) ) ).toFixed(13)  ), 2))
        }
    })
}

exports.intervalInterestRate = function(annualInterestRate, annualPeriodicPayments, compoundingPeriods){
    let r = Number(annualInterestRate)
    let n = Number(annualPeriodicPayments)
    let c = Number(compoundingPeriods)
    return new Promise((res, rej)=>{   
        if(r === 0 || n === 0 || c === 0){
            res('error')
        }
        else{
            res(Number(Math.pow( (1 + Number((r / c).toFixed(13))  ), Number((c / n).toFixed(13) ) ).toFixed(13)) -1)
        }
    })
}

exports.truncNumber = function(inNum, sigFigs){
    return new Promise((res, rej)=>{
        if(sigFigs >= 0 && sigFigs<= 20){
            //console.log()
            let multiple = Math.pow(10,sigFigs)
           res((Math.trunc(inNum * multiple) / multiple).toFixed(sigFigs))
       }
       else{
           rej('error')
       }
    })
    
}
exports.calcProjectedPaymentTotal = function(payment, interval){
    let p = Number(payment)
    let i = Number(interval)
    return new Promise((res, rej)=>{
        if(p === 0 || i === 0){
            res('error')
        }
        else{
            res((i * Math.trunc(100 * p)) / 100)
        }
        
    })
}
exports.calcProjectedTotalsFromSchedule = async function(scheduleArray, payment, debt, origDateTime, annualInterestRate){
    return new Promise(async (res,rej)=>{
        let resultsArray= {
            totalSchedulePayments: null,
            totalScheduleInterest: null,
            totalSchedulePrincipal: null,
            debt: debt,
            actualPayments: [],
            actualTotalInterest: null,
            actualTotalPrincipalPaid: null
        }
        //console.log(JSON.stringify(scheduleArray))
        for(let schedule of scheduleArray){
            resultsArray.totalScheduleInterest  = resultsArray.totalScheduleInterest  + schedule.interest;
            resultsArray.totalSchedulePrincipal = resultsArray.totalSchedulePrincipal + schedule.principal;
            resultsArray.totalSchedulePayments =  resultsArray.totalSchedulePayments  + payment; 
            let fromDateTime = null
            let toDateTime = schedule.dateTime
            //console.log('schedulelength:' + resultsArray.actualPayments.length)
            if(resultsArray.actualPayments.length === 0){

                //console.log('datetime2' + origDateTime)
                fromDateTime = origDateTime
            }
            else{
               // console.log(resultsArray.actualPayments.length - 1)
                fromDateTime = scheduleArray[resultsArray.actualPayments.length - 1].dateTime
            }
            let accruedInterest = await this.calcAccruedInterest(fromDateTime,toDateTime,annualInterestRate,resultsArray.debt)
            resultsArray.debt =resultsArray.debt - (payment - accruedInterest)
            resultsArray.actualTotalInterest =  resultsArray.actualTotalInterest + accruedInterest
            resultsArray.actualTotalPrincipalPaid = resultsArray.actualTotalPrincipalPaid + (payment - accruedInterest)
            resultsArray.actualPayments.push({dateTime: toDateTime, debt:resultsArray.debt, accruedInterest: accruedInterest, principalPaid: payment - accruedInterest})
        
        }
        //console.log(JSON.stringify(resultsArray))
        res(resultsArray)
    })
}
exports.calcProjectedInterestTotal = function(principal, totalProjectedPayment){
    return new Promise((res,rej)=>{
        let p = Number(principal)
        let tp = Number(totalProjectedPayment)
        let projectedInterest = this.truncNumber(( ((tp * 100) - (p *100)) / 100),2)
        res(projectedInterest)
    })
}
exports.calcProjectedSchedule = function(loanDebt, interval, intervalInterestRate, frequency, dateTime, payment){
    let i = Number(interval)
    let p = Number(payment)
    let r = Number(intervalInterestRate)
    let momentSkip = 1
    let momentFreqValue = 'M'

    if(String(frequency) === String('monthly')){
        momentFreqValue = 'M';
        momentSkip = 1
    }
    else if(String(frequency) === String('weekly')){
        momentFreqValue = 'days';
        momentSkip = 7  
    }
    else if(String(frequency) === String('biweekly')){
        momentFreqValue = 'days';
        momentSkip = 14
    }


    return new Promise(async (res, rej)=>{
        let scheduleArray = [
            {
                dateTime: Number(dateTime),
                interest: Number(0.00),
                displayInterest: Number(0.00),
                principal: Number(0.00), 
                displayPrincipal: Number(0.00), 
                loanDebt: Number(loanDebt), 
                displayLoanDebt: Number(loanDebt)}
        ]
        for(let inc = 1; inc <= i; inc++){
            //console.log(Number( r * scheduleArray[inc - 1].loanDebt))
            let newInterest = Number( r * scheduleArray[inc - 1]['loanDebt'])
            let newPrincipal = Number( p - Number( r * scheduleArray[inc - 1]['loanDebt']) )
            let newLoanDebt = Number(scheduleArray[inc - 1]['loanDebt'] - Number( p - Number( r * scheduleArray[inc - 1]['loanDebt']) ) )
            //console.log(newInterest + '|' + newPrincipal + '|' + newLoanDebt)
            let displaySchedule = await this.calcDisplaySchedule(inc, i, p, newLoanDebt, newInterest, newPrincipal )
            let dateTime = moment.unix(scheduleArray[inc - 1]['dateTime']).add(momentSkip, momentFreqValue).unix()
            scheduleArray.push({
                //dateTime: moment.unix(scheduleArray[inc - 1]['dateTime']).utc().add(momentSkip, momentFreqValue ).unix() ,
                dateTime: dateTime,
                interest: newInterest,
                displayInterest: displaySchedule['data']['interest'],
                principal: newPrincipal,
                displayPrincipal: displaySchedule['data']['principal'],
                loanDebt: newLoanDebt,
                displayLoanDebt: displaySchedule['data']['debt']
            })
        }
        res({success: true, data: scheduleArray.slice(1), message: 'created_schedule'})
    })
}
exports.calcDisplaySchedule = function(inInterval, inIntervals, inPayment, inDebt, inInterest, inPrincipal){
    let interval = Number(inInterval)
    let intervals = Number(inIntervals)
    let payment = Number(inPayment)
    let debt = Number(inDebt)
    let interest = Number(inInterest)
    let principal = Number(inPrincipal)
    let display= {interest: 0.00, principal: 0.00, debt: 0.00}
    return new Promise((res,rej)=>{
        if(interval === intervals){ //Last interval debt should equal 0
            display.debt = 0.00
            display.interest = Number((Math.trunc( interest * 100) / 100).toFixed(2))
            display.principal = Number( (Math.trunc( (payment * 100) - Math.trunc( interest * 100) ) / 100 ).toFixed(2) )  
        }
        else{
            display.debt = Number((Math.trunc(debt * 100) / 100).toFixed(2))
            display.interest = Number((Math.trunc(interest * 100) / 100).toFixed(2))
            display.principal = Number( ( ( (payment * 100) - Math.trunc(interest * 100) ) / 100).toFixed(2))
        }
        res({success: true, data: display, message: 'created_display_schedule'})
    })
}
