var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var moment = require('moment');

var ObligationController = require('../controllers/obligation')
var ActController = require('../controllers/act.ts')


exports.createObligationAct = function(amount, dateTime, personId, accountId, obligationId, type, excessAmount){
return new Promise(async (res, rej)=>{
    let returnObj ={success: false, data: null, message: 'no_obligaionAct_created'}
    let obigationDifference = await getObligationDifference(amount, dateTime, obligationId, type, excessAmount)
    console.log(JSON.stringify(obigationDifference))
    res(returnObj)
})}

function getObligationDifference(amount, dateTime, obligationId, type, excessAmount){
return new Promise(async (res, rej)=>{
    let returnObj = {success: false, data: null, message: 'no_obligaionAct_created'}

    let passedValidation = true;
    if(amount < 0){
        passedValidation = false;
        returnObj.message = returnObj.message + '| validationError_amount_<_0'
    }
    
    
    if(passedValidation === true){    
        let foundObligation = await ObligationController.findLoan({_id: obligationId})
        .catch(returnObj.message = returnObj.message + '| no_obligation_found_err')
        if(foundObligation['success']){

            let accruedInterest =0.0
            accruedInterest = await ObligationController.calcAccruedInterest(foundObligation['data']['when']['interestAccrued'], dateTime, foundObligation['data']['annualInterestRate'], foundObligation['data']['principal'])
            .catch(passedValidation = false)
            if(passedValidation = true){
                //amount, InterestAccrued
                let interestOwed =  Number(accruedInterest) + Number(foundObligation['data']['interest'])
                //let ScheduledPrincipalOwed = foundObligation['data']['intervalPayment'] - Number(accruedInterest) - Number(foundObligation['data']['interest'])
                //This attempts to get what would be needed to be current... which I should do somewhere else
                if(type === 'customer_payment' ){

                }
                else{

                }
                let 



                returnObj = {success: true, data: null, message: 'obligaion_found'}
            }
            else{
                returnObj.message = returnObj.message + '| error_with_accruedInterest'
            }
        }
        else{
            returnObj.message = returnObj.message + '| no_obligation_found'
        }
    }
    
    
    res(returnObj)
})}





exports.createFee = async function(financialEvent){return new Promise(async (res, rej)=>{res(await createFee(financialEvent))})  }

 function createFee(financialEvent){
    return new Promise( async (res, rej)=>{
        let returnObj = {success: false, data: null, message: 'fn_createFee_no_create'}
        //chance the obligation
        //create the act
        let fee = await createObligationFee(financialEvent['obligationId'], financialEvent['dateTime'], financialEvent['amount'])
        .catch((err)=>console.log('create Fee Error') + err)
        //console.log('controller createFee' + JSON.stringify(fee))
        res(fee)
    })
}

async  function createObligationFee(obligationId, dateTime, amount){
    return new Promise(async (res, rej)=>{
        //Amounts can be positive or negative. 
        let foundObligation = await ObligationController.findLoan({_id: obligationId})
        //console.log('createObligationFee' + JSON.stringify(initialObligation))
        foundObligation['data']['feeDebt'] = Number(foundObligation['data']['feeDebt']) + Number(amount);
        foundObligation['data']['debt'] = Number(foundObligation['data']['debt']) + Number(amount);

        let initialAct = await ActController.createAct({
            type: ['financial','fee', 'debit'],
            personId: foundObligation['data']['personId'],
            accountId: foundObligation['data']['accountId'],
            obligationId: obligationId,
            dateTime: dateTime,
            amount: amount,
            feeReduction: (-1*amount),
            debt: foundObligation['data']['debt'], 
            loanDebt: foundObligation['data']['loanDebt'],  
            interest: foundObligation['data']['interest'],  
            principal: foundObligation['data']['principal'],
            feeDebt: foundObligation['data']['feeDebt'],
            when: {created: moment().unix()}
        }).catch((err)=> console.log('Err in CreateObligation > createAct'))

        if(initialAct['success'] && foundObligation['success']){
            let updateLoan = ObligationController.replaceLoan({_id: foundObligation['data']['_id']},foundObligation['data'] )
            .catch((err)=>console.log('Err in CreateObligation > createAct'))

        }
        //console.log(JSON.stringify(initialAct))
        res({success: true, data: {loan: foundObligation, act: initialAct }, message: 'fee_created'})

    })
    
}