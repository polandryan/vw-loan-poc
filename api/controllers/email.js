var Email = require('../models/email');
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
    const insertOptions = {upsert: true, new: true, useFindAndModify: false}
    const updateOptions = {upsert: false, new: true, useFindAndModify: false}
var moment = require('moment');
// success: Boolean (T/F), data: [{}] array of objects, message: ''

exports.findEmail = async function(findByCriteria){
    let returnObj = {success: false, data: null, message: 'nothing'}
    return new Promise((res, rej)=>{
        //console.log(JSON.stringify(findByCriteria))
        mongoose.model('email').find(findByCriteria).exec()
        .then(resolved=>{
            if(resolved.length == 1){
                returnObj = {success: true, data: resolved[0], message:'email_found'}
                //console.log(JSON.stringify({success: true, data: resolved[0], message:'email_found'}))
                res(returnObj)
            }
            else{
                returnObj ={success: false, data: null, message:'no_email_found'}
                //console.log(JSON.stringify({success: false, data: null, message:'no_email_found'}))
                res(returnObj)
            }
        })
        .catch(rejected=>{
            rej({success: false, data: rejected, message:'db_rejection_error'})
        })
    })
}
exports.findEmails = async function(findByCriteria){
    let returnObj = {success: false, data: null, message: 'nothing'}
    return new Promise((res, rej)=>{
        //console.log(JSON.stringify(findByCriteria))
        mongoose.model('email').find(findByCriteria).exec()
        .then(resolved=>{
            if(resolved.length > 0){
                returnObj = {success: true, data: resolved, message:'emails_found'}
                //console.log(JSON.stringify({success: true, data: resolved[0], message:'email_found'}))
                res(returnObj)
            }
            else{
                returnObj ={success: false, data: null, message:'no_email_found'}
                //console.log(JSON.stringify({success: false, data: null, message:'no_email_found'}))
                res(returnObj)
            }
        })
        .catch(rejected=>{
            rej({success: false, data: rejected, message:'db_rejection_error'})
        })
    })
}


exports.createEmail =  function(email, password){
    let returnObj = {success: false, data: null, message: 'nothing'}
    return new Promise((res, rej)=>{
        let createCriteria={
            email: email,
            password: password,
            verified: false,
            verification: { 
                generated: moment().unix(),
                expired: moment().add(1, 'd').unix(),
                urlParam: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15),
                code: 'aba'},
            personId: null,
            accountIds: [],
            vwTier: 'customer', 
        }
        mongoose.model('email').findOneAndUpdate({email: email}, {$set: createCriteria}, {upsert: true, new: true, useFindAndModify: false}).exec()
        .then(emailResolved=>{
            returnObj = {success: true, data: emailResolved, message: 'created'}
            //console.log(JSON.stringify(returnObj))
            res(returnObj)
        })
        .catch(emailRejected=>{
            returnObj = {success: false, data: null, message: emailRejected}
            rej(returnObj)
            //console.log(returnObj)
        })
    })
}

exports.updateEmail = function(findCriteria, updateCriteria){
    //console.log('UpdateEmail: findCriteria' + JSON.stringify(findCriteria) + '| update with' + JSON.stringify(updateCriteria))
    return new Promise((res, rej)=>{
       // let returnObj ={success: false, data: null, message: 'nothing'}
        mongoose.model('email').findOneAndUpdate(findCriteria, updateCriteria, updateOptions).exec()
        .then(resolved=>{
            if(resolved == null){
                res({success: false, data: [], message: 'email_not_updated_no_record_found'})
            }
            else{
                res({success: true, data: resolved, message: 'email_updated'})
            }
        })
        .catch(rejected=>{
            rej({success: false, data: rejected, message: 'email_not_updated_db_error'})
        })    
    })
} 



exports.email_check_exists = async function(email){
    let returnObj = {success: false, data: null, message: 'nothing'}
    return new Promise((res, rej)=>{
        mongoose.model('email').find({email: email}).exec()
        .then(emailResolved =>{            
            if(emailResolved.length > 0){
                returnObj={success: true, data: emailResolved, message: 'found email'}
                //console.log('Resolved Email' + JSON.stringify(returnObj))
                res(returnObj)
            }
            else{
                returnObj = {success: false, data: null, message: 'none found'}
                //console.log('Resolved & No Email' + JSON.stringify(returnObj))
                res(returnObj)
            }
        })
        .catch(emailRejected =>{
            returnObj={success: false, data: null, message: emailRejected}
            //console.log(returnObj)
            rej(returnObj)
        })
    }
)}

exports.verifyUrlParam_code = async function(urlParam, code){
    let returnObj = {success: false, data: null, message: 'nothing'}
    return new Promise((res, rej)=>{
        mongoose.model('email').findOne({$and: [{'verification.urlParam': urlParam},{'verification.code': code}]}).exec()
        .then(resolved=>{
            res({success: true, data: resolved, message:'verification matches'})
        })
        .catch(rejected=>{rej({success: false, data: rejected, message:'problem'})})
    }) 
}

exports.deleteEmailAccount = async function(emailId, accountId){
   // let returnObj = {success: false, data: null, message: 'nothing'}
    return new Promise((res, rej)=>{
        //console.log('IN AccountId: '+ accountId + ' Out of Email: ' + emailId)
        //mongoose.model('account').updateOne(findByCriteria, {$pull: {'emailIds':  {_id:  accountEmailId}}}, {multi: false}).exec()
        mongoose.model('email').updateOne({$and: [{'_id': emailId}, {'accountIds': {$in: [accountId]} }]}, {$pull: {'accountIds': {$in: [accountId]}}}, {multi: false}).exec()
        .then(resolved=>{
            console.log('In delete email account'+JSON.stringify(resolved))
            if(resolved['nModified']){
                res({success: true, data: [], message: 'email_account_deleted'})
            }
            else{
                res({success: false, data: [], message: 'email_account_not_Found'})
            }
        })
            
        .catch(rejected=>{rej({success: false, data: [], message: 'email_account_NOT_deleted'})})
    })
}