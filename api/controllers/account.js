var Account = require('../models/account');
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
    const insertOptions = {upsert: true, new: true, useFindAndModify: false}
    const updateOptions = {upsert: false, new: true, useFindAndModify: false}
var moment = require('moment');
// success: Boolean (T/F), data: [{}] array of objects, message: ''

exports.createAccount = function(name){
    return new Promise((res, rej)=>{
        let createdDateTime = moment().unix()
        let findCriteria = {$and: [{name: name}, {'when.created': createdDateTime}]}
        let createCriteria ={
            name: name,
            emailIds: [],
            when: {created: createdDateTime}
        }
        mongoose.model('account').findOneAndUpdate(findCriteria, {$set: createCriteria}, insertOptions).exec()
        .then(resolved=>{
            res({success: true, data: resolved, message: 'account_created'})
        })
        .catch(rejected=>{
            rej({success: false, data: rejected, message: 'account_not_created'})
        })
    })
}
exports.updateAccount = function(findCriteria, updateCriteria){
    //console.log('findCriteria' + JSON.stringify(findCriteria) + '| update with' + JSON.stringify(updateCriteria))
    return new Promise((res, rej)=>{
        let returnObj = {success: false, data: null, message: 'nothing'}
        mongoose.model('account').findOneAndUpdate(findCriteria, updateCriteria, updateOptions).exec()
        .then(resolved=>{
            //console.log('update account res' + JSON.stringify(resolved))
            res({success: true, data: resolved, message: 'account_updated'})
        })
        .catch(rejected=>{
            //console.log('update account rej' + JSON.stringify(rejected))
            rej({success: true, data: rejected, message: 'account_not_updated'})
        })    
    })
}
exports.getAccounts = function(findCriteria){
    //console.log(JSON.stringify(findCriteria))
    return new Promise((res, rej)=>{
        let returnObj = {success: false, data: null, message: 'nothing'}
        mongoose.model('account').find(findCriteria).exec()
        .then(resolved=>{ //found one or more accounts based on critiera
            //console.log(JSON.stringify(resolved))
            if(resolved.length > 0){
                returnObj = {success: true, data: resolved, message: 'accounts_found'}
                res(returnObj)
            }
            else{
                res({success: false, data: null, message: 'accounts_not_found'})
            }
        })
        .catch(rejected=>{
            rej({success: false, data: rejected, message: 'accounts_error'})
        })
    })
}
exports.findAccount = function(findCriteria){
    return new Promise((res, rej)=>{
        let returnObj = {success: false, data: null, message: 'nothing'}
        mongoose.model('account').find(findCriteria).exec()
        .then(resolved=>{ //found one or more accounts based on critiera
            //console.log(JSON.stringify(resolved))
            if(resolved.length == 1){
                returnObj = {success: true, data: resolved[0], message: 'account_found'}
                res(returnObj)
            }
            else{
                res({success: false, data: null, message: 'account_not_found'})
            }
        })
        .catch(rejected=>{
            rej({success: false, data: rejected, message: 'account_error'})
        })
    })
}

exports.deleteAccountEmail = async function(accountId, emailId, accountEmailId){
    return new Promise((res, rej)=>{
        //let returnObj = {success: false, data: null, message: 'nothing'}
        let findByCriteria = {$and: [{'_id': accountId}, {'emailIds._id': accountEmailId }]}
        console.log('In Delete Account' + JSON.stringify(findByCriteria))
        //mongoose.model('account').find(findByCriteria).exec()
        mongoose.model('account').updateOne(findByCriteria, {$pull: {'emailIds':  {_id:  accountEmailId}}}, {multi: false}).exec()
        .then(resolved=>{
            if(resolved['n']){
                res({success: true, data: null, data: 'accounts_email_deleted'})
            }
            else{
                res({success: false, data: null, data: 'accounts_email_NOT_found'})
            }
            
            })
        .catch(rejected=>{
            console.log(rejected)
            rej({success: false, data: null, data: 'accounts_email_NOT_deleted'})})
    })
}