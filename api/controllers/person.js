//var Person = require('../models/person');
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
    const insertOptions = {upsert: true, new: true, useFindAndModify: false}
    const updateOptions = {upsert: false, new: true, useFindAndModify: false}
var moment = require('moment');
// success: Boolean (T/F), data: [{}] array of objects, message: ''

exports.findPerson = function(findCriteria){
    return new Promise((res, rej)=>{
        mongoose.model('person').find(findCriteria).exec()
        .then(resolved =>{
            if(resolved.length == 1){
                res({success: true, data: resolved[0], message: 'person_found'})
            }
            else{ //did't find A person
                res({success: false, data: null, message: 'person_not_found'})
            }
        })
        .catch( rejected=>{
            rej({success: false, data: rejected, message: 'person_error_db'})
        })
    })
}
exports.findPeople = function(findCriteria){
    return new Promise((res, rej)=>{
        mongoose.model('person').find(findCriteria).exec()
        .then(resolved =>{
            if(resolved.length > 0){
                res({success: true, data: resolved, message: 'people_found'})
            }
            else{ //did't find A person
                res({success: false, data: null, message: 'people_not_found'})
            }
        })
        .catch( rejected=>{
            rej({success: false, data: rejected, message: 'people_error_db'})
        })
    })
}

exports.createPerson = async function(){
    return new Promise((res, rej)=>{
        let createdDateTime = moment().unix()
        let findCriteria = {$and: [{firstName: null}, {'when.created': createdDateTime}]}
        let createCriteria ={
            firstName: null,
            currentAccountId: null,
            currentAccountTier: null,
            emailIds: [],
            accountIds: [],
            experimentCode: 'none',
            when: {created: createdDateTime}
        }
        mongoose.model('person').findOneAndUpdate(findCriteria, {$set: createCriteria}, insertOptions).exec()
        .then(resolved=>{
            //console.log(JSON.stringify(resolved))
            res({success: true, data: resolved, message: 'person_created'})
        })
        .catch(rejected=>{
           // console.log('in catch:' + JSON.stringify(rejected))
            rej({success: false, data: rejected, message: 'person_not_created'})
        })
    })
}
exports.updatePerson = function(findCriteria, updateCriteria){
    //console.log('findCriteria' + JSON.stringify(findCriteria) + '| update with' + JSON.stringify(updateCriteria))
    return new Promise((res, rej)=>{
       // let returnObj ={success: false, data: null, message: 'nothing'}
        mongoose.model('person').findOneAndUpdate(findCriteria, updateCriteria, updateOptions).exec()
        .then(resolved=>{
            res({success: true, data: resolved, message: 'person_updated'})
        })
        .catch(rejected=>{
            rej({success: true, data: rejected, message: 'person_not_updated'})
        })    
    })
}