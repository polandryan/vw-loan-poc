

var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var moment = require('moment');

var ObligationController = require('../controllers/obligation')


exports.calcCheckCreate = async function(inValues){
    return new Promise(async (res, rej)=>{
        let obligation = {
            //Settings
            recorded:{
                enteredCreatedDateTime: inValues.contractCreateTime,
                interestAccruedDateTime: inValues.interestAccruedDateTime,
                projectedFirstPaymentDateTime: inValues.projectedFirstPaymentDateTime,
                projectedLastPaymentDateTime: inValues.projectedLastPaymentDateTime ,
                lastPaymentCutOrRound: inValues.lastPaymentCutOrRound,
                lastPaymentLabel: inValues.lastPaymentLabel,
                lastPayment: inValues.lastPayment,
                intervals: inValues.intervals,
                intervalLabel: inValues.intervalLabel,
                intervalPayment: inValues.intervalPayment,
                intervalPaymentCutOrRound: inValues.intervalPaymentCutOrRound,
                daysPerYearLabel: inValues.daysPerYearLabel,
                annualInterestRate: inValues.annualInterestRate,
                compoundLabel: 'simple/exact',
                initialInterestAccrualGreaterthanIntervalLabel: 'intervalPercentage',
                //Initial Financial
                debt: inValues.debt,
                loanDebt: inValues.loanDebt,
                principal: inValues.principal,
                interest: inValues.interest,
                feeDebt: inValues.interest,
                display: {
                    debt: inValues.debt,
                    loanDebt: inValues.loanDebt,
                    principal: inValues.principal,
                    interest: inValues.interest,
                    feeDebt: inValues.interest,
                },    
                projectedPaid:{ //Rounded based on schedule
                    debt: null,
                    loanDebt: null,
                    principal: null,
                    interest: null,
                    feeDebt: null,
                    payments:null,
                    formulas:{ //Standard Formulas
                        payments: null,
                        interest: null,
                    },
                },
                schedule:[{
                    dateTime: inValues.interestAccruedDateTime,
                    type: 'financial,schedule',
                    paid:{
                        payment: '0.00',
                        paymentInterest: '0.00',
                        paymentPrincipal: '0.00',
                        paymentFee: '0.00',
                        payments: '0.00',
                        debt: '0.00',
                        loanDebt: '0.00',
                        principal: '0.00',
                        interest: '0.00',
                        feeDebt: '0.00',
                        display: {
                            paymentInterest: '0.00',
                            paymentPrincipal: '0.00',
                            paymentFee: '0.00',
                            debt: '0.00',
                            loanDebt: '0.00',
                            principal: '0.00',
                            interest: '0.00',
                            feeDebt: '0.00',
                        },
                    },
                    remaining:{
                        debt: inValues.debt,
                        loanDebt: inValues.loanDebt,
                        principal: inValues.principal,
                        interest: inValues.interest,
                        feeDebt: inValues.feeDebt,
                        display: {
                            debt: inValues.debt,
                            loanDebt: inValues.loanDebt,
                            principal: inValues.principal,
                            interest: inValues.interest,
                            feeDebt: inValues.feeDebt,
                        }
                    },
                //deviation
                }],
            },
        
            interestAccruedDateTime: inValues.interestAccruedDateTime,
            paid:{
                payment: '0.00',
                paymentInterest: '0.00',
                paymentPrincipal: '0.00',
                paymentFee: '0.00',
                payments: '0.00',
                debt: '0.00',
                loanDebt: '0.00',
                principal: '0.00',
                interest: '0.00',
                feeDebt: '0.00',
                display: {
                    paymentInterest: '0.00',
                    paymentPrincipal: '0.00',
                    paymentFee: '0.00',
                    debt: '0.00',
                    loanDebt: '0.00',
                    principal: '0.00',
                    interest: '0.00',
                    feeDebt: '0.00',
                },
            },
            remaining:{
                debt: inValues.debt,
                loanDebt: inValues.loanDebt,
                principal: inValues.principal,
                interest: inValues.interest,
                feeDebt: inValues.feeDebt,
                display: {
                    debt: inValues.debt,
                    loanDebt: inValues.loanDebt,
                    principal: inValues.principal,
                    interest: inValues.interest,
                    feeDebt: inValues.feeDebt,
                }
            },
            deviation:{ //scheduled vs current
                debt: '0.00',
                loanDebt: '0.00',
                principal: '0.00',
                interest: '0.00',
                feeDebt: '0.00',
                display: {
                    debt: '0.00',
                    loanDebt: '0.00',
                    principal: '0.00',
                    interest: '0.00',
                    feeDebt: '0.00',
                }
            }
        
        }
        
        //IF THINGS ARE NULL FROM THE USER FILL THEM IN:
        if(obligation.recorded.intervalPayment === null){
            obligation.recorded.intervalPayment = await calcIntervalPayment(obligation.recorded.principal,obligation.recorded.intervals,obligation.recorded.annualInterestRate, obligation.recorded.intervalLabel,obligation.recorded.daysPerYearLabel,obligation.recorded.intervalPaymentCutOrRound,obligation.recorded.interestAccruedDateTime,obligation.recorded.projectedFirstPaymentDateTime,obligation.recorded.projectedLastPaymentDateTime)
        }
        if(obligation.recorded.interest === null){
            obligation.recorded.interest = '0.00'
            obligation.recorded.display.interest = '0.00'
            obligation.remaining.interest = '0.00'

        }
        if(obligation.recorded.loanDebt === null){
            obligation.recorded.loanDebt = Number(obligation.recorded.interest) + Number(obligation.recorded.principal)
            obligation.recorded.display.loanDebt = Number(obligation.recorded.interest) + Number(obligation.recorded.principal)
            obligation.remaining.loanDebt = Number(obligation.recorded.interest) + Number(obligation.recorded.principal)
        }
        if(obligation.recorded.feeDebt === null){
            obligation.recorded.feeDebt = '0.00'
            obligation.recorded.display.feeDebt = '0.00'
            obligation.remaining.feeDebt = '0.00'
        }
        if(obligation.recorded.debt === null){
            obligation.recorded.debt = Number(obligation.recorded.feeDebt) + Number(obligation.recorded.loanDebt)
            obligation.recorded.display.debt = Number(obligation.recorded.feeDebt) + Number(obligation.recorded.loanDebt)
            obligation.remaining.debt = Number(obligation.recorded.feeDebt) + Number(obligation.recorded.loanDebt)
        }

        let daysPerYear = 365
        let momentObj = await convLabelToMoment(inValues.intervalLabel)
        let intervalsPerYear = momentObj['data']['intervalsPerYear']
        let momentIntervalUnits = momentObj['data']['momentIntervalUnits']
        let momentFreqValue = momentObj['data']['momentFreqValue'] //Change based on IntervalLabel

        let tableRow=0; 
        for(let interval = 0; interval < obligation.recorded.intervals; interval++){
            tableRow ++ //This tracks the schedule row being inserted into the obligation
            let newDateTime = moment.unix(obligation.recorded.schedule[interval].dateTime).add(momentIntervalUnits, momentFreqValue).unix()
            if(interval === 0){
                newDateTime = obligation.recorded.projectedFirstPaymentDateTime
            }

            obligation.recorded.schedule.push(
                {
                    dateTime: newDateTime,
                    type: 'financial,schedule',
                    paid:{
                        payment: null, //last payment paid amount
                        paymentInterest: null, //last payment paid amount
                        paymentPrincipal: null, //last payment paid amount
                        paymentFee: null, //last payment paid amount
                        payments: null, //total of payments paid
                        debt: null,
                        loanDebt: null,
                        principal: null,
                        interest: null,
                        feeDebt: null,
                        display: {
                            paymentInterest: null, //last payment paid amount
                            paymentPrincipal: null, //last payment paid amount
                            paymentFee: null, //last payment paid amount
                            debt: null,
                            loanDebt: null,
                            principal: null,
                            interest: null,
                            feeDebt: null,
                        },
                    },
                    remaining:{
                        debt: null,
                        loanDebt: null,
                        principal: null,
                        interest: null,
                        feeDebt: null,
                        display: {
                            debt: null,
                            loanDebt: null,
                            principal: null,
                            interest: null,
                            feeDebt: null,
                        }
                    },
                //deviation
                }
            )
            //Interval Payment has already been calcualted
            if(interval === (Number(obligation.recorded.intervals) - 1)){//the last row behaves differently
                //console.log('lastPayment=' + obligation.recorded.lastPayment)
                if(obligation.recorded.lastPaymentLabel === 'lastPaymentMatchesPrincipal'){
                    if(obligation.recorded.lastPayment === null || obligation.recorded.lastPayment === undefined || obligation.recorded.lastPayment === ''  ){ //Calculate the Last Payment 
                        let lastPayment = await calcLastPayment(obligation.recorded.schedule[tableRow].dateTime, obligation)
                       obligation.recorded.schedule[tableRow].paid.payment = String(lastPayment)
                    }
                    else{ //The user specified or a previous calcuation was conducted
                        obligation.recorded.schedule[tableRow].paid.payment = obligation.recorded.lastPayment    
                    }
                }
                else{ //RETAIN INTERVAL PAYMENT
                    obligation.recorded.schedule[tableRow].paid.payment= obligation.recorded.intervalPayment
                }
                
            }
            else{ //Just use the Interval payment Not Last Row
                obligation.recorded.schedule[tableRow].paid.payment= obligation.recorded.intervalPayment
            }

            let paidPayment = await calcPaidPayment(obligation.recorded.schedule[tableRow],obligation)
            .catch((err)=>{console.log('err calcPaidPayment')})

            
            obligation.recorded.schedule[tableRow].paid = paidPayment['data']['paid']
            obligation.recorded.schedule[tableRow].remaining = paidPayment['data']['remaining']
            obligation.paid =paidPayment['data']['paid']
            obligation.remaining = paidPayment['data']['remaining']
            obligation.interestAccruedDateTime = obligation.recorded.schedule[tableRow].dateTime
            

            //console.log(interval + 'PaidPayment:' +paidPayment['data'].paid.payment+ '|' +paidPayment['data'].paid.display.paymentInterest + '|' + paidPayment['data'].paid.display.paymentInterest + obligation.remaining.interest + '|' + obligation.remaining.principal  )
        }
        obligation.recorded.projectedPaid.principal = obligation.paid.display.principal
        obligation.recorded.projectedPaid.interest = obligation.paid.display.interest
        obligation.recorded.projectedPaid.payments = obligation.paid.payments

        obligation.recorded.projectedPaid.formulas.payments= await calcTruncNumber( (Number(obligation.recorded.intervalPayment) * Number(obligation.recorded.intervals)),2)
        obligation.recorded.projectedPaid.formulas.interest = await calcTruncNumber( (Number(obligation.recorded.projectedPaid.formulas.payments) - Number(obligation.recorded.principal)),2)
        obligation.recorded.lastPayment = obligation.recorded.schedule[tableRow -1].paid.payment
        obligation.recorded.projectedLastPaymentDateTime = obligation.recorded.schedule[tableRow -1].dateTime
        
        //console.log('Obligation: '+JSON.stringify(obligation))
        res({success: true, data: obligation, message:'building obligation'})
    })
}

async function calcIntervalPayment(principal, intervals, annualInterestRate, intervalLabel, daysPerYearLabel, cutOrRound, interestAccruedDateTime, firstPaymentDateTime, lastPaymentDateTime){
    return new Promise(async (res, rej)=>{   
      //console.log(principal +"|" + intervals +'|'+annualInterestRate+'|'+cutOrRound+'|'+interestAccruedDateTime+'|'+firstPaymentDateTime)
      
      let daysPerYear = 365
      
      
      
      let momentObj = await convLabelToMoment(intervalLabel)
      let intervalsPerYear = momentObj['data']['intervalsPerYear']
      let momentIntervalUnits = momentObj['data']['momentIntervalUnits']
      let momentFreqValue = momentObj['data']['momentFreqValue'] //Change based on IntervalLabel

      let extraDays=0
      if(interestAccruedDateTime <  moment.unix(firstPaymentDateTime).subtract(momentIntervalUnits, momentFreqValue).unix() ){
          extraDays = moment(moment.unix(firstPaymentDateTime).subtract(momentIntervalUnits, momentFreqValue)).diff(moment.unix(interestAccruedDateTime) , 'days')
      }
      //totalDays = totalDays +  Number(moment.unix(firstPaymentDateTime).add(intervals, momentFreqValue).diff(moment.unix(firstPaymentDateTime), 'days' ))
      //FV =  P(1+(AR/IPY)^(IPY * (N/IPY + (D/DPY))
      let air = Number(annualInterestRate)/Number(intervalsPerYear) //Annual Interval Rate
      //console.log('annual interval Rate:' + air)
      let years = (Number(intervals) / (intervalsPerYear)) + (Number(extraDays)/ Number(daysPerYear)) //time in years
      let futureValue = principal * Math.pow((1 + Number(air)),(Number(intervalsPerYear) * Number(years)))
      //console.log('futureValue:' + futureValue)
      //IP = FV*IR / ((1 + IR)^n -1)
      let floatPayment = (Number(futureValue) * Number(air)) / (Math.pow((1 + Number(air)),(intervals)) - 1 )
  
      if(cutOrRound === 'cut'){
          res(calcTruncNumber(floatPayment  , 2))
      }
      else{
          res(calcTruncNumber(Math.round(Number(floatPayment) * 100) / 100, 2))
      }
    })
}

function calcTruncNumber(inNum, digits){
    let outNum = '0'
    if(parseFloat(inNum) && isFinite(inNum) ){
    //currently digits always 2
    //console.log(inNum)
    let splitNum = String(inNum).split('.')
    //console.log(typeof(splitNum[1]))
    if( typeof(splitNum[1]) === 'undefined' ){
        splitNum[1] = '00'
    }
    else{splitNum[1] = splitNum[1] + '00'}
        outNum = String(splitNum[0]) + '.' + String(splitNum[1]).substr(0,2)
    }
    return outNum
}

function calcLastPayment(dateTime, obligation){
    return new Promise(async(res, rej)=>{
        let message= 'In calcLastPayment - No Fees'
        //console.log('second to last'+JSON.stringify(obligation))
        let accruedInterest = await calcInterestAccrual(obligation.interestAccruedDateTime, dateTime, obligation.recorded.annualInterestRate, obligation.remaining.principal, obligation.remaining.interest, obligation.recorded.daysPerYearLabel)
        .catch((err)=>{accruedInterest = 0.00})
        //console.log('LastPayment accruedInterest:'+accruedInterest+'|From: '+moment.unix(obligation.interestAccruedDateTime).format('LLLL') + '|To: ' +moment.unix(dateTime).format('LLLL') )
        let lastPayment =  calcTruncNumber( (Math.round( (Number(obligation.remaining.principal) + Number(accruedInterest))  * 100) / 100), 2)
        //console.log(message + lastPayment)
        res(lastPayment)
    }) 
}
async function calcInterestAccrual(fromDateTime, toDateTime, annualInterestRate, principal, accruedInterest, daysPerYearLabel){
    return new Promise(async (res,rej)=>{
        if(Number(toDateTime) > Number(fromDateTime)){
            if(String(daysPerYearLabel) === String('real/30/360')){ //real/30/360  accural30/360
                let daysDiff = Number(360) * (Number(moment.unix(toDateTime).format('YYYY')) - Number(moment.unix(fromDateTime).format('YYYY')) ) +  30 * ( Number(moment.unix(toDateTime).format('MM')) - Number(moment.unix(fromDateTime).format('MM') ) ) + Number(moment.unix(toDateTime).format('DD') ) - Number( moment.unix(fromDateTime).format('DD'))
                let dailyInterestRate = await calcDailyInterestRate(annualInterestRate,daysPerYearLabel,null )
                .catch((err)=> dailyInterestRate = 0)
                res((Number(principal) * Number(daysDiff) * Number(dailyInterestRate)) +  Number(accruedInterest))
            }
    
            else if(String(daysPerYearLabel) === String('real/365')){
                let daysDiff = moment.unix(toDateTime).diff(moment.unix(fromDateTime) , 'days')
                let dailyInterestRate = await calcDailyInterestRate(annualInterestRate,daysPerYearLabel,null )
                .catch((err)=> dailyInterestRate = 0)
                res((Number(principal) * Number(daysDiff) * Number(dailyInterestRate)) +  Number(accruedInterest))
            }
          else if(String(daysPerYearLabel) === String('real/real')){
                let dailyInterestRate = 0
                for(let dayInterval =0; fromDateTime < toDateTime; dayInterval++){
                    dailyInterestRate = dailyInterestRate + Number(await calcDailyInterestRate(annualInterestRate,daysPerYearLabel, fromDateTime) )
                    fromDateTime = moment.unix(fromDateTime).add(1, 'd')
                }
                res((Number(principal) * Number(dailyInterestRate)) +  Number(accruedInterest))
            }
            else{res(0)}
        }
        else{
            res(0)
        }
    })   
}
function calcDailyInterestRate(annualInterestRate, daysPerYearLabel, dateTime){
    return new Promise((res, rej)=>{
        if(String(daysPerYearLabel) === String('real/30/360')){ //real/30/360  accural30/360
            res( Number(annualInterestRate)  / Number(360) )
        }
        else if(String(daysPerYearLabel) === String('real/365')){
            res( Number(annualInterestRate) / Number(365) )
        }
        else if(String(daysPerYearLabel) === String('real/real')){
            let currentYear = moment.unix(dateTime).format('YYYY')
            let nextYear = Number(currentYear) + 1
            let startOfCurrentYear = '01-01-' + currentYear
            let startOfNextYear = '01-01-' + nextYear
            let daysInYear = moment(startOfNextYear).diff(moment(startOfCurrentYear), 'days')
            res( Number(annualInterestRate) / Number(daysInYear) )
        }
        else{
            res(0)
        }
    })
}

async function calcPaidPayment(paymentObj, obligation){
    return new Promise(async (res, rej)=>{
       
        //calcScheduledFuturePrincipal(previousFinancialObj.settings.interestStartAccruedDateTime, paymentObj.dateTime,previousFinancialObj.settings.initialPrincipal,previousFinancialObj.settings.annualInterestRate,previousFinancialObj.settings.intervalPayment,previousFinancialObj.settings.intervalLabel,previousFinancialObj.settings.daysPerYearLabel  )
        let interestAccrued = await calcInterestAccrual(obligation.interestAccruedDateTime, paymentObj.dateTime, obligation.recorded.annualInterestRate, obligation.remaining.principal ,0.00,obligation.recorded.daysPerYearLabel)
            .catch((err)=>{interestAccrued = 0})
        //console.log('InCalcPaidPayment:'+ obligation.remaining.interest)
        obligation.remaining.interest =Number(obligation.remaining.interest) + Number(interestAccrued)
        //console.log('InCalcPaidPayment> interestAccrued:'+ obligation.remaining.interest + '|Days:'+ moment.unix(paymentObj.dateTime).diff(moment.unix(obligation.interestAccruedDateTime) , 'days') +'| From' +moment.unix(obligation.interestAccruedDateTime).format('LLLL') + '|TO:' +moment.unix(paymentObj.dateTime).format('LLLL'))
           

        let paymentLessInterest = await calcPaymentLessInterest(paymentObj, obligation)
            .catch((err)=>{paymentLessInterest = {success: false, data: null, message: 'Error paymentLessInterest'}})
        //console.log('paymentLessInterest2' + JSON.stringify(paymentLessInterest))
            

        let paymentLessPrincipal = await calPaymentLessPrincipal(paymentLessInterest['data'], obligation)
            .catch((err)=>{paymentLessPrincipal = {success: false, data: null, message: 'Error calcPaymentLessPrincipal'}})

        
        let paymentLessFees = await calcPaymentLessFees(paymentLessPrincipal['data'], obligation )
            //.catch((err)=>{paymentLessFees = {success: false, data: null, message: 'Error paymentLessFees'}})
 
        let paymentLessReducePrincipal = await calcPaymentLessReducePrincipal(paymentLessFees['data'], obligation)
            .catch((err)=>{paymentLessReducePrincipal = {success: false, data: null, message: 'Error calcPaymentLessReducePrincipal'}})

        let paymentDebt = await calcPaymentDebt(paymentLessReducePrincipal['data'], obligation)
        .catch((err)=>{paymentDebt = {success: false, data: null, message: 'Error calcPaymentDebt'}})

        let paymentDisplay = await calcPaymentDisplay(paymentDebt['data'])
        .catch((err)=>{paymentDisplay = {success: false, data: null, message: 'Error calcPaymentDisplay'}})

        //console.log('afterPaymentDebt'+JSON.stringify(paymentDisplay['data']['remaining']['principal']))
        if(paymentDisplay['success']){
            //paymentDisplay['data']['dateTime'] = paymentObj.dateTime
            //paymentDisplay['data']['paid']['payment']= paymentObj.paid.payment
            paymentDisplay['data']['paid']['payments'] = calcTruncNumber((Number(obligation.paid.payments) + Number(paymentObj.paid.payment)),2)
            res({success: true, data: paymentDisplay['data'], message:'calculated payment'})
        }
        else{
            res({success: false, data: null, message:'NO payment calculated'}) 
        }
    })
}
function calcPaymentLessInterest(paymentObj, obligation){
    return new Promise((res, rej)=>{
        let message= 'in_calPaymentLessInterest'
        //console.log('InStartCalcPaymentLessInterest: paymentInterestNull=' + paymentObj.paid.paymentInterest + '| payment:' +paymentObj.paid.payment + '|' + Number(calcTruncNumber(obligation.remaining.interest,2)))

        if(paymentObj.paid.paymentInterest === null){ //ATTEMPT TO PAY ALL INTEREST ACCRUED
            if(Number(paymentObj.paid.payment) >= Number(calcTruncNumber(obligation.remaining.interest,2))){ //PAID All Accrued Interest             
                paymentObj.paid.paymentInterest = Number(obligation.remaining.interest)
                paymentObj.paid.interest = Number(obligation.paid.interest) + Number(obligation.remaining.interest)
                paymentObj.remaining.interest = 0.00
                message ='paid all remaining interest'
            }
            else{ //Only a Portion of Accrued Interest 
                paymentObj.paid.paymentInterest = Number(paymentObj.payment)
                paymentObj.paid.interest = Number(obligation.paid.interest) +  Number(paymentObj.payment)
                paymentObj.remaining.interest = Number(obligation.remaining.interest) - Number(paymentObj.payment)
                message= 'paid only a portion of interest'
            }
        }
        else{
            if(Number(paymentObj.paymentInterest) >= Number(calcTruncNumber(paymentObj.remaining.interest,2))){ //PAID All Accrued Interest & can go negative if they pay more            
                paymentObj.paid.paymentInterest = Number(paymentObj.paymentInterest)
                paymentObj.paid.interest = Number(obligation.paid.interest) + Number(paymentObj.paymentInterest)
                paymentObj.remaining.interest = obligation.remaining.interest - paymentObj.paymentInterest
                message = 'paid only specific amount of interest but all accrued'
            }
            else{ //Only a Portion of Accrued Interest 
                paymentObj.paid.paymentInterest = Number(paymentObj.paymentInterest)
                paymentObj.paid.interest = Number(obligation.paid.interest) +  Number(paymentObj.paymentInterest)
                paymentObj.remaining.interest = Number(obligation.remaining.interest) - Number(paymentObj.paymentInterest)
                message = 'paid only specific amount of interest not all the accrued'
            }
        }
        //console.log(message + '| payment: '+ paymentObj.paid.payment +'|' + paymentObj.paid.paymentInterest + '|' + paymentObj.remaining.interest )
        res({success: true, data: paymentObj, message:'paid_accrued_interest' })
    })
}

function calPaymentLessPrincipal(paymentObj,obligation){
    return new Promise((res, rej)=>{
        let message= 'In calcPaymentLessPrincipal '
        //How do I know how much principal should be paid? paymentObject would have it or everything goes to principal less interest paid
        if(paymentObj.paid.paymentPrincipal === null){ //Everything less interest goes to principal
            if(Number(paymentObj.paid.payment) > Number(paymentObj.paid.paymentInterest)){ // there IS an amount that can reduce principal
                //need to check stop negative
                //Need to figure out how much principal is scheduled to be reduced by now!!!
                let paidPrincipal = Number(paymentObj.paid.payment) - Number(paymentObj.paid.paymentInterest)
                paymentObj.paid.paymentPrincipal = paidPrincipal
                paymentObj.paid.principal = Number(obligation.paid.principal) + Number(paidPrincipal)
                paymentObj.remaining.principal = Number(obligation.remaining.principal) - Number(paidPrincipal)
                message = 'Reduce Principal by remaining payment amount '
            }
            else{ //NOTHING left to reduce principal
                paymentObj.paid.paymentPrincipal = 0
                message = 'No Reduction in principal '
            }
        }
        else{ // only a specific amount goes to principal
            paymentObj.paid.paymentPrincipal = paymentObj.paymentPrincipal
            paymentObj.paid.principal = Number(obligation.paid.principal) + Number(paymentObj.paid.paymentPrincipal)
            paymentObj.remaining.principal = Number(obligation.remaining.principal) - Number(paymentObj.paid.paymentPrincipal)
            message = 'Reduce principal only by a specified amount'
        }
        //console.log(message + paymentObj.paid.paymentPrincipal + ' remaining:' + paymentObj.remaining.principal)
        res({success:true, data: paymentObj, message: message})
    })  
}
function calcPaymentLessFees(paymentObj, obligation){
    return new Promise((res, rej)=>{
        let message= 'In calcPaymentLessFees'
        if(Number(obligation.remaining.feesDebt) > 0.01 ){ //FEES to pay
            message= 'calcPaymentLessFees> remaining fees'
        }
        else{ //No fees to pay
            message= 'calcPaymentLessFees> No Fees'
        }
        paymentObj.paid.feeDebt = 0.00
        paymentObj.remaining.feeDebt = obligation.remaining.feeDebt

        res({success: true, data: paymentObj, message: message})
    })
}

function calcPaymentLessReducePrincipal(paymentObj, obligation){
    return new Promise((res, rej)=>{
        let message= 'In calcPaymentLessReducePrincipal'
        res({success: true, data: paymentObj, message: message})
    })
}
function calcPaymentLessReduceFuturePayment(paymentObj, obligation){
    return new Promise((res, rej)=>{
        let message= 'In calcPaymentLessReduceFuturePayment'
        res({success: true, data: paymentObj, message: message})
    })
}
function calcPaymentDebt(paymentObj){
    return new Promise((res, rej)=>{
        let message= 'In calcPaymentDebt'
        paymentObj.paid.loanDebt = Number(paymentObj.paid.principal) + Number(paymentObj.paid.interest)
        paymentObj.paid.debt = Number(paymentObj.paid.loanDebt) + Number(paymentObj.paid.feeDebt)
        paymentObj.remaining.loanDebt = Number(paymentObj.remaining.principal) + Number(paymentObj.remaining.interest)
        paymentObj.remaining.debt = Number(paymentObj.remaining.loanDebt) + Number(paymentObj.remaining.feeDebt)
        //console.log(message + financialObj.paid.loanDebt)
        res({success: true, data: paymentObj, message: message})
    })
}

function calcPaymentDisplay(paymentObj){
    return new Promise((res, rej)=>{
        let message= 'In calcPaymentDisplay'
        
        paymentObj.paid.display.paymentPrincipal = calcTruncNumber( (Math.round(Number(paymentObj.paid.paymentPrincipal) * 100) / 100), 2)
        paymentObj.paid.display.paymentInterest = calcTruncNumber( (Math.round(Number(paymentObj.paid.paymentInterest) * 100) / 100), 2)
        paymentObj.paid.display.paymentFee = calcTruncNumber( (Math.round(Number(paymentObj.paid.paymentFee) * 100) / 100), 2) 
        paymentObj.paid.display.principal = calcTruncNumber( (Math.round(Number(paymentObj.paid.principal) * 100) / 100), 2)
        paymentObj.paid.display.interest =  calcTruncNumber( (Math.round(Number(paymentObj.paid.interest) * 100) / 100), 2)
        paymentObj.paid.display.loanDebt = calcTruncNumber( (Math.round(Number(paymentObj.paid.loanDebt) * 100) / 100), 2)
        paymentObj.paid.display.debt = calcTruncNumber( (Math.round(Number(paymentObj.paid.debt) * 100) / 100), 2)
        
        paymentObj.remaining.display.debt = calcTruncNumber( (Math.round(Number(paymentObj.remaining.debt) * 100) / 100), 2)
        paymentObj.remaining.display.loanDebt= calcTruncNumber( (Math.round(Number(paymentObj.remaining.loanDebt) * 100) / 100), 2)
        paymentObj.remaining.display.principal= calcTruncNumber( (Math.round(Number(paymentObj.remaining.principal) * 100) / 100), 2)
        paymentObj.remaining.display.interest= calcTruncNumber( (Math.round(Number(paymentObj.remaining.interest) * 100) / 100), 2)
        paymentObj.remaining.display.feeDebt = calcTruncNumber( (Math.round(Number(paymentObj.remaining.feeDebt) * 100) / 100), 2)
        //console.log('calcPaymentdisplays:'+ financialObj.remaining.display.debt)
        res({success: true, data: paymentObj, message: message})
    })
}

function calcScheduledFuturePrincipal(startDateTime, endDateTime, initialPrincipal, annualInterestRate, intervalPayment, intervalLabel, daysPerYearLabel, ){
    return new Promise(async (res, rej)=>{
        let daysPerYear = 365
        let momentObj = await convLabelToMoment(intervalLabel)
        let intervalsPerYear = momentObj['data']['intervalsPerYear']

        let IAPR = Number(annualInterestRate) / Number(intervalsPerYear)  //Interval Annual Percentage Rate
        let totalDays =  Number(moment.unix(endDateTime).diff(moment.unix(startDateTime) ,'days'))
        let T = (Number(intervalsPerYear) * (Number(totalDays)) / Number(daysPerYear) )
        let numerator = Math.pow((1 + Number(IAPR)),(intervalsPerYear)) - Math.pow((1 + Number(IAPR) ),(T))
        let denominator = Math.pow((1 + Number(IAPR)),intervalsPerYear) -1
        let futurePrincipal =  (Number(initialPrincipal) * (numerator)) / (denominator)
        //console.log(moment.unix(endDateTime).format('LLLL') + '|' +totalDays +'|'+ T +'|'+ futurePrincipal)
        res(futurePrincipal)

    })
}

function convLabelToMoment(label){
    return new Promise((res,rej)=>{
        let returnObj ={success: true, data: {intervalsPerYear: 12, momentFreqValue: 'M', momentIntervalUnits: 12}, message:'could not convert'}


        if(label === 'monthly'){
            returnObj.data.intervalsPerYear = 12
            returnObj.data.momentFreqValue = 'M'
            returnObj.data.momentIntervalUnits = 1
        }
        if(label === 'weekly'){
            returnObj.data.intervalsPerYear = 52
            returnObj.data.momentFreqValue = 'w'
            returnObj.data.momentIntervalUnits = 1
        }
        if(label === 'biweekly'){
            returnObj.data['intervalsPerYear'] = 26
            returnObj.data['momentFreqValue'] = 'w'
            returnObj.data['momentIntervalUnits'] = 2
        }
        
        res(returnObj)
    })
}