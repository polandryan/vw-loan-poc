
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const insertOptions = {upsert: true, new: true, useFindAndModify: false}
const updateOptions = {upsert: false, new: true, useFindAndModify: false}
var moment = require('moment');

var Act = require('../models/act')

exports.findAct = function(findByCriteria){
    let returnObj = {success: false, data: null, message:'No_Act_Found'}
    return new Promise((res, rej)=>{
        res(returnObj)
    })
}

exports.findActs = function(findByCriteria){
    let returnObj = {success: false, data: null, message:'No_Acts_Found'}
    return new Promise((res, rej)=>{
        mongoose.model('act').find(findByCriteria).exec()
        .then(resolved=>{
            //console.log('inFindActs' + JSON.stringify(resolved))
            res({success:true, data: resolved, message: "found_acts"})
        })
        .catch(rejected=>{res(returnObj)})
        
    })
}

exports.createAct = function(createWithCriteria){
    let returnObj = {success: false, data: null, message:'no_act_created'}
    return new Promise((res, rej)=>{
        //console.log('create Act')
        mongoose.model('act').findOneAndUpdate({dateTime: createWithCriteria['dateTime'], obligationId: createWithCriteria['obligationId'], when: {created: moment().unix()} }, {$set: createWithCriteria}, {upsert: true, new: true, useFindAndModify: false}).exec()
        .then(resolved=>{
            returnObj = {success: true, data: resolved, message: 'act_created'}
            res(returnObj)
        })
        .catch(eventRejected=>{
            returnObj = {success: false, data: null, message: 'act_not_created'}
            console.log(JSON.stringify(eventRejected))
            rej(returnObj)
        })
    })
}


exports.updateAct = function(findByCriteria, updateWithCritera){
    let returnObj = {success: false, data: null, message:'No_Act_Updated'}
    return new Promise((res, rej)=>{
        res(returnObj)
    })
}

exports.replaceAct = function(findByCriteria, replaceWithCritera){
    let returnObj = {success: false, data: null, message:'no_act_replaced'}
    return new Promise((res, rej)=>{
        mongoose.model('act').replaceOne(findByCriteria, replaceWithCritera).exec()
        .then(resolved=>{
            returnObj = {success: true, data: resolved, message: 'act_replaced'}
            res(returnObj)
        })
        .catch(eventRejected=>{
            returnObj = {success: false, data: null, message: 'act_not_replaced'}
            rej(returnObj)
        })
    })
}

exports.deleteAct = function(findByCriteria){
    let returnObj = {success: false, data: null, message:'no_act_deleted'}
    return new Promise((res, rej)=>{
        res(returnObj)
    })
}
