
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var moment = require('moment');

var ObligationController = require('../controllers/obligation')

exports.calcObligationInterestAccrual = function(toDateTime, obligationId, obligation){
    return calcObligationInterestAccrual(toDateTime, obligationId, obligation)
}
function calcObligationInterestAccrual(toDateTime, obligationId, obligation){
    let returnObj = {success: false, data: null, message: 'err_accrued_interest'}
    let foundObligation
    return new Promise(async (res,rej)=>{
        foundObligation = await decipherObligation(obligationId, obligation)
        .catch((err)=>foundObligation = {success: false, data: null, message: 'no_decipher_obligation'})

        if(foundObligation['success']){
            let accruedInterest =  await calcInterestAccrual(foundObligation['data']['when']['interestAccrued'], toDateTime,foundObligation['data']['annualInterestRate'],foundObligation['data']['principal'],foundObligation['data']['interest'], 'real/365' )
            returnObj={success: true, data: {accruedInterest: accruedInterest}, message: 'interest_accrued'}
        }
        else{
            returnObj={success: false, data: null, message: foundObligation['message']}
        }
        //console.log(JSON.stringify(returnObj))
        res(returnObj)
    })
}
async function calcInterestAccrual(fromDateTime, toDateTime, annualInterestRate, principal, accruedInterest, daysPerYearLabel){
    return new Promise(async (res,rej)=>{
        if(Number(toDateTime) > Number(fromDateTime)){
            if(String(daysPerYearLabel) === String('real/30/360')){ //real/30/360  accural30/360
                let daysDiff = Number(360) * (Number(moment.unix(toDateTime).format('YYYY')) - Number(moment.unix(fromDateTime).format('YYYY')) ) +  30 * ( Number(moment.unix(toDateTime).format('MM')) - Number(moment.unix(fromDateTime).format('MM') ) ) + Number(moment.unix(toDateTime).format('DD') ) - Number( moment.unix(fromDateTime).format('DD'))
                let dailyInterestRate = await calcDailyInterestRate(annualInterestRate,daysPerYearLabel,null )
                .catch((err)=> dailyInterestRate = 0)
                res((Number(principal) * Number(daysDiff) * Number(dailyInterestRate)) +  Number(accruedInterest))
            }
    
            else if(String(daysPerYearLabel) === String('real/365')){
                let daysDiff = moment.unix(toDateTime).diff(moment.unix(fromDateTime) , 'days')
                let dailyInterestRate = await calcDailyInterestRate(annualInterestRate,daysPerYearLabel,null )
                .catch((err)=> dailyInterestRate = 0)
                res((Number(principal) * Number(daysDiff) * Number(dailyInterestRate)) +  Number(accruedInterest))
            }
          else if(String(daysPerYearLabel) === String('real/real')){
                let dailyInterestRate = 0
                for(let dayInterval =0; fromDateTime < toDateTime; dayInterval++){
                    dailyInterestRate = dailyInterestRate + Number(await calcDailyInterestRate(annualInterestRate,daysPerYearLabel, fromDateTime) )
                    fromDateTime = moment.unix(fromDateTime).add(1, 'd')
                }
                res((Number(principal) * Number(dailyInterestRate)) +  Number(accruedInterest))
            }
            else{res(0)}
        }
        else{
            res(0)
        }
    })   
}

exports.calcObligationPrincipalAccrual= function(toDateTime, obligationId, obligation){
    return calcObligationPrincipalAccrual(toDateTime, obligationId, obligation)
}
function calcObligationPrincipalAccrual(toDateTime, obligationId, obligation){
    let returnObj = {success: false, data: null, message: 'err_accrued_Principal'}
    let foundObligation
    return new Promise(async (res,rej)=>{
        foundObligation = await decipherObligation(obligationId, obligation)
        .catch((err)=>foundObligation = {success: false, data: null, message: 'no_decipher_obligation'})

        if(foundObligation['success']){
            let sumOfPrincipal = {intervals: 0, total: 0}
            for(let payment of foundObligation['data']['paymentSchedule']){
                if(payment.dateTime <= toDateTime){
                    sumOfPrincipal.intervals ++
                    sumOfPrincipal.total = sumOfPrincipal.total + payment.principal
                }
            }
            let percentScheduledToDateTime = await calcPercentOfDays(foundObligation['data']['paymentSchedule'][sumOfPrincipal.intervals + 1]['dateTime'], toDateTime, 'real/365')
            .catch((err)=>console.log('err cal obligation'))
            sumOfPrincipal.total = sumOfPrincipal.total + (Number(foundObligation['data']['paymentSchedule'][sumOfPrincipal.intervals + 1]['principal']) * Number(percentScheduledToDateTime))
            //console.log(JSON.stringify(sumOfPrincipal))
            //Change obligation to have the paid principal out right. but it is
            let principalAccrued = foundObligation['data']['originalPrincipal'] - foundObligation['data']['principal'] + sumOfPrincipal.total
            returnObj={success: true, data: {principalAccrued: principalAccrued}, message: 'principal_accrued'}
        }
        else{
            returnObj={success: false, data: null, message: foundObligation['message']}
        }
        //console.log(JSON.stringify(returnObj))
        res(returnObj)
    })
}

function decipherObligation(obligationId, obligation){
    return new Promise(async (res,rej)=>{
        let foundObligation = {success: false, data: null, message: 'start_no_obligation'}
        if(JSON.stringify(obligation) === 'null') {
            if(String(obligationId) === 'null'){
                foundObligation = {success: false, data: null, message: 'no_obligation_viaId_or_object'}
            }
            else{
                let tempObligation = await ObligationController.findLoan({_id: obligationId})
                .catch(foundObligation = {success: false, data: null, message: 'no_obligation_error in findLoan'})
                if(tempObligation['success']){
                    foundObligation = {success: true, data: tempObligation['data'], message: 'found_obligation_idlookup'}
                }
            }
        }
        else{
            foundObligation = {success: true, data: obligation, message: 'found_obligation_passthrough_object'}
        }
        res(foundObligation)
    })
    
}

exports.calcCheckCreate = async function(inValues){
    return new Promise(async (res, rej)=>{
        let obligation = {
            //Settings
            recorded:{
                enteredCreatedDateTime: inValues.contractCreateTime,
                interestAccruedDateTime: inValues.interestAccruedDateTime,
                projectedFirstPaymentDateTime: inValues.projectedFirstPaymentDateTime,
                projectedLastPaymentDateTime: inValues.projectedLastPaymentDateTime ,
                lastPaymentCutOrRound: inValues.lastPaymentCutOrRound,
                lastPaymentLabel: inValues.lastPaymentLabel,
                lastPayment: inValues.lastPayment,
                intervals: inValues.intervals,
                intervalLabel: inValues.intervalLabel,
                intervalPayment: inValues.intervalPayment,
                intervalPaymentCutOrRound: inValues.intervalPaymentCutOrRound,
                daysPerYearLabel: inValues.daysPerYearLabel,
                annualInterestRate: inValues.annualInterestRate,
                compoundLabel: 'simple/exact',
                initialInterestAccrualGreaterthanIntervalLabel: 'intervalPercentage',
                //Initial Financial
                debt: inValues.debt,
                loanDebt: inValues.loanDebt,
                principal: inValues.principal,
                interest: inValues.interest,
                feeDebt: inValues.interest,
                display: {
                    debt: inValues.debt,
                    loanDebt: inValues.loanDebt,
                    principal: inValues.principal,
                    interest: inValues.interest,
                    feeDebt: inValues.interest,
                },    
                projectedPaid:{ //Rounded based on schedule
                    debt: null,
                    loanDebt: null,
                    principal: null,
                    interest: null,
                    feeDebt: null,
                    Formulas:{ //Standard Formulas
                        loanDebt: null,
                        principal: null,
                        interest: null,
                    },
                },
                schedule:[{
                    dateTime: inValues.interestAccruedDateTime,
                    type: null,
                    paid:{
                        payment: '0.00',
                        paymentInterest: '0.00',
                        paymentPrincipal: '0.00',
                        debt: '0.00',
                        loanDebt: '0.00',
                        principal: '0.00',
                        interest: '0.00',
                        feeDebt: '0.00',
                        display: {
                            debt: '0.00',
                            loanDebt: '0.00',
                            principal: '0.00',
                            interest: '0.00',
                            feeDebt: '0.00',
                        },
                    },
                    remaining:{
                        debt: inValues.debt,
                        loanDebt: inValues.loanDebt,
                        principal: inValues.principal,
                        interest: inValues.interest,
                        feeDebt: inValues.feeDebt,
                        display: {
                            debt: inValues.debt,
                            loanDebt: inValues.loanDebt,
                            principal: inValues.principal,
                            interest: inValues.interest,
                            feeDebt: inValues.feeDebt,
                        }
                    },
                //deviation
                }],
            },
            current:{
                interestAccruedDateTime: inValues.interestAccruedDateTime,
                paid:{
                    payment: '0.00',
                    paymentInterest: '0.00',
                    paymentPrincipal: '0.00',
                    debt: '0.00',
                    loanDebt: '0.00',
                    principal: '0.00',
                    interest: '0.00',
                    feeDebt: '0.00',
                    display: {
                        debt: '0.00',
                        loanDebt: '0.00',
                        principal: '0.00',
                        interest: '0.00',
                        feeDebt: '0.00',
                    },
                },
                remaining:{
                    debt: inValues.debt,
                    loanDebt: inValues.loanDebt,
                    principal: inValues.principal,
                    interest: inValues.interest,
                    feeDebt: inValues.feeDebt,
                    display: {
                        debt: inValues.debt,
                        loanDebt: inValues.loanDebt,
                        principal: inValues.principal,
                        interest: inValues.interest,
                        feeDebt: inValues.feeDebt,
                    }
                },
                deviation:{ //scheduled vs current
                    debt: '0.00',
                    loanDebt: '0.00',
                    principal: '0.00',
                    interest: '0.00',
                    feeDebt: '0.00',
                    display: {
                        debt: '0.00',
                        loanDebt: '0.00',
                        principal: '0.00',
                        interest: '0.00',
                        feeDebt: '0.00',
                    }
                }
            }
            //},
        }
        
        //IF THINGS ARE NULL FROM THE USER FILL THEM IN:
        if(obligation.recorded.intervalPayment === null){
            obligation.recorded.intervalPayment = await calcIntervalPayment(obligation.recorded.principal,obligation.recorded.intervals,obligation.recorded.annualInterestRate, obligation.recorded.intervalLabel,obligation.recorded.daysPerYearLabel,obligation.recorded.intervalPaymentCutOrRound,obligation.recorded.interestAccruedDateTime,obligation.recorded.projectedFirstPaymentDateTime,obligation.recorded.projectedLastPaymentDateTime)
        }
        if(obligation.recorded.interest === null){
            obligation.recorded.interest = '0.00'
            obligation.recorded.display.interest = '0.00'
            obligation.current.remaining.interest = '0.00'

        }
        if(obligation.recorded.loanDebt === null){
            obligation.recorded.loanDebt = Number(obligation.recorded.interest) + Number(obligation.recorded.principal)
            obligation.recorded.display.loanDebt = Number(obligation.recorded.interest) + Number(obligation.recorded.principal)
            obligation.current.remaining.loanDebt = Number(obligation.recorded.interest) + Number(obligation.recorded.principal)
        }
        if(obligation.recorded.feeDebt === null){
            obligation.recorded.feeDebt = '0.00'
            obligation.recorded.display.feeDebt = '0.00'
            obligation.current.remaining.feeDebt = '0.00'
        }
        if(obligation.recorded.debt === null){
            obligation.recorded.debt = Number(obligation.recorded.feeDebt) + Number(obligation.recorded.loanDebt)
            obligation.recorded.display.debt = Number(obligation.recorded.feeDebt) + Number(obligation.recorded.loanDebt)
            obligation.current.remaining.debt = Number(obligation.recorded.feeDebt) + Number(obligation.recorded.loanDebt)
        }



        //Create Amortization Schedule/table
        let paymentAct={
            dateTime: null,
            payment: null,
            paymentInterest: null,
            paymentPrincipal: null,
            paymentFees: null,
        }
        //let financialPaymentAct = calcFinancialPaymentAct(paymentAct,obligation)

        console.log('Obligation: '+JSON.stringify(obligation))
        res({success: true, data: obligation, message:'building obligation'})
    })
}

exports.calcAmortizationTable2 = async function(inValues){
    return await calcAmortizationTable2(inValues)
}
async function  calcAmortizationTable2(inValues){
    return new Promise(async (res, rej)=>{
        let previousFinancialObj={
            dateTime: inValues.interestAccruedDateTime,
            created:{
                interestAccruedDateTime:null, 
                intervalPayment: null,
                debt: 0,
                loanDebt: 0,
                principal: 0,
                interest: 0,
                feeDebt: 0,
                display:{
                    debt: 0.00,
                    loanDebt: 0.00,
                    principal: 0.00,
                    interest: 0.00,
                    feeDebt: 0.00,
                }

            },
            paid: {
                payment: 0,
                paymentInterest: 0,
                paymentPrincipal: 0,
                paymentFee:0,
                payments: 0,
                debt: 0,
                loanDebt: 0,
                principal: 0,
                interest: 0,
                feeDebt: 0,
                display:{
                    paymentInterest: 0.00,
                    paymentPrincipal: 0.00,
                    paymentFee: 0.00,
                    debt: 0.00,
                    loanDebt: 0.00,
                    principal: 0.00,
                    interest: 0.00,
                    feeDebt: 0.00,
                }
            },
            remaining: {
                debt: inValues.debt,
                loanDebt: inValues.loanDebt,
                principal: inValues.principal,
                interest: 0.00, //should come from inValues
                feeDebt: 0.00, //should come from inValues
                display:{
                    debt: inValues.debt,
                    loanDebt: inValues.loanDebt,
                    principal: inValues.principal,
                    interest: 0.00, //should come from inValues
                    feeDebt: 0.00, //should come from inValues
                }
            },
            settings:{
                daysPerYearLabel: inValues.daysPerYearLabel,
                intervalLabel: inValues.intervalLabel,
                annualInterestRate: inValues.annualInterestRate,
                initialPrincipal: inValues.principal,
                intervals: inValues.intervals,
                interestStartAccruedDateTime: inValues.interestAccruedDateTime,
                firstPaymentDateTime: inValues.projectedFirstPaymentDateTime,
                projectedEndPaymentDateTime: null,
                intervalPayment: null,
            }
        }
        let payment = await calcIntervalPayment(inValues.principal,inValues.intervals, inValues.annualInterestRate, inValues.intervalLabel, inValues.daysPerYearLabel, inValues.intervalPaymentCutOrRound,inValues.interestAccruedDateTime,inValues.projectedFirstPaymentDateTime, null)
        previousFinancialObj.paid.payment = Number(payment)
        let paymentObj={
            dateTime: inValues.projectedFirstPaymentDateTime,
            payment: payment,//should come from inValues
            paymentInterest: null,//should come from inValues
            paymentPrincipal: null,//should come from inValues
            paymentFee: 0,//should come from inValues
            paymentDistributionLabel: 'interestAccrued_principalScheduled_feesDue_principalRemaining' //future payment...
        }
        previousFinancialObj.settings.intervalPayment = payment

        let outValues = {
            intervalPayment: payment,
            lastIntervalPayment: '0.00',
            formulas:{
                payments: '0.00',
                futureValue: '0.00',
                interest: '0.00',
            },
            paid:{
                payments: '0.00',
                debt: '0.00',
                loanDebt: '0.00',
                principal: '0.00',
                interest: '0.00',
                feeDebt: '0.00',
                display:{
                    debt: '',
                    loanDebt: '',
                    principal: '',
                    interest: '',
                    feeDebt: '',
                }
            },
            table:[
                {dateTime: inValues.interestAccruedDateTime,
                paid: {
                    payment: '0',
                    paymentInterest: '0',
                    paymentPrincipal: '0',
                    paymentFee: '0',
                    payments: '0',
                    debt: '0',
                    loanDebt: '0',
                    principal: '0',
                    interest: '0',
                    feeDebt: '0',
                    display:{
                        paymentInterest: '0.00',
                        paymentPrincipal: '0.00',
                        paymentFee: '0.00',
                        debt: '0.00',
                        loanDebt: '0.00',
                        principal: '0.00',
                        interest: '0.00',
                        feeDebt: '0.00',
                    }
                },
                remaining: {
                    debt: inValues.debt,
                    loanDebt: inValues.loanDebt,
                    principal: inValues.principal,
                    interest: '0.00', //should come from inValues
                    feeDebt: '0.00', //should come from inValues
                    display:{
                        debt: inValues.debt,
                        loanDebt: inValues.loanDebt,
                        principal: inValues.principal,
                        interest: '0.00', //should come from inValues
                        feeDebt: '0.00', //should come from inValues
                    }
                },
                settings:{
                    daysPerYearLabel: inValues.daysPerYearLabel,
                    intervalLabel: inValues.intervalLabel,
                    annualInterestRate: inValues.annualInterestRate,                    
                }
            }]
        }

        for(let interval = 0; interval < inValues.intervals; interval++){
            paymentObj.dateTime = moment.unix(inValues.projectedFirstPaymentDateTime).add(interval, 'M').unix()


            if(Number(interval) === Number((Number(inValues.intervals) - 1)) && String(inValues.lastPaymentLabel) === 'lastPaymentMatchesPrincipal' ){
                paymentObj.payment = await calcLastPayment(paymentObj.dateTime, previousFinancialObj)
            }

           // console.log('PaymentObj:' + JSON.stringify(paymentObj))

            let paidPayment = await calcPaidPayment2(paymentObj, previousFinancialObj)
            .catch((err)=>{'Prob in generating payment Schedule'})
            if(paidPayment['success']){
                //console.log('interval: ' + interval +'|dateTime: '+paidPayment['data']['dateTime'] +'|remaining: ' + JSON.stringify(paidPayment['data']['remaining']) )
                previousFinancialObj = paidPayment['data']
            }
            else{
                console.log('paidPayment schedule problem')
                interval = inValues.intervals
            }
         
            outValues.table[interval] = {
                dateTime: paidPayment['data'].dateTime,
                paid: {
                    payment: paidPayment['data'].paid.payment,
                    paymentInterest: paidPayment['data'].paid.paymentInterest,
                    paymentPrincipal: paidPayment['data'].paid.paymentPrincipal,
                    paymentFee: paidPayment['data'].paid.paymentFee,
                    payments: paidPayment['data'].paid.payments,
                    debt: paidPayment['data'].paid.debt,
                    loanDebt: paidPayment['data'].paid.loanDebt,
                    principal: paidPayment['data'].paid.principal,
                    interest: paidPayment['data'].paid.interest,
                    feeDebt: paidPayment['data'].paid.feeDebt,
                    display:{
                        paymentInterest: paidPayment['data'].paid.display.paymentInterest,
                        paymentPrincipal: paidPayment['data'].paid.display.paymentPrincipal,
                        paymentFee: paidPayment['data'].paid.display.paymentFee,
                        debt: paidPayment['data'].paid.display.debt,
                        loanDebt: paidPayment['data'].paid.display.loanDebt,
                        principal: paidPayment['data'].paid.display.principal,
                        interest: paidPayment['data'].paid.display.interest,
                        feeDebt: paidPayment['data'].paid.display.feeDebt,
                    }
                },
                remaining: {
                    debt: paidPayment['data'].remaining.debt,
                    loanDebt: paidPayment['data'].remaining.loanDebt,
                    principal: paidPayment['data'].remaining.principal,
                    interest: paidPayment['data'].remaining.interest,
                    feeDebt: paidPayment['data'].remaining.feeDebt,
                    display:{
                        debt: paidPayment['data'].remaining.display.debt,
                        loanDebt: paidPayment['data'].remaining.display.loanDebt,
                        principal: paidPayment['data'].remaining.display.principal,
                        interest: paidPayment['data'].remaining.display.interest,
                        feeDebt: paidPayment['data'].remaining.display.feeDebt,
                    }
                },
                settings:{
                    daysPerYearLabel: paidPayment['data'].settings.daysPerYearLabel,
                    intervalLabel: paidPayment['data'].settings.intervalLabel,
                    annualInterestRate: paidPayment['data'].settings.annualInterestRate,
                }   
            }
            //console.log('Interval' + interval+': ' + JSON.stringify(outValues.table[interval]))
        }
        
        outValues.formulas.payments = calcTruncNumber(Number(outValues.intervalPayment) * Number(outValues.table.length),2)
        outValues.formulas.interest = calcTruncNumber(Number(outValues.formulas.payments) - Number(inValues.principal),2)
        outValues.formulas.futureValue = calcTruncNumber( await calcFutureValue(inValues.principal, inValues.annualInterestRate, inValues.intervals, inValues.intervalLabel, inValues.daysPerYearLabel, inValues.interestAccruedDateTime, inValues.firstPaymentDateTime), 2 )
        outValues.formulas['principal'] = inValues.principal

        //console.log('INTERVAL-0--->'+ JSON.stringify(outValues.table[0]))
        //console.log('INTERVAL-11--->'+ JSON.stringify(outValues.table[11]))
        outValues.lastIntervalPayment = outValues.table[Number(inValues.intervals) - 1].paid.payment
        outValues.paid.payments  = outValues.table[Number(inValues.intervals) - 1].paid.payments
        outValues.paid.debt = outValues.table[Number(inValues.intervals) - 1].paid.debt
        outValues.paid.loanDebt = outValues.table[Number(inValues.intervals) - 1].paid.loanDebt
        outValues.paid.feeDebt = outValues.table[Number(inValues.intervals) - 1].paid.feeDebt
        outValues.paid.display.debt = outValues.table[Number(inValues.intervals) - 1].paid.display.debt
        outValues.paid.display.loanDebt = outValues.table[Number(inValues.intervals) - 1].paid.display.loanDebt
        outValues.paid.display.principal = outValues.table[Number(inValues.intervals) - 1].paid.display.principal
        outValues.paid.display.interest = outValues.table[Number(inValues.intervals) - 1].paid.display.interest
        outValues.paid.display.feeDebt = outValues.table[Number(inValues.intervals) - 1].paid.display.feeDebt
        
        res({success: true, data: outValues, message:'' })
    })

}
async function calcAmortizationFinancialEvent(inFinancialObj){
    /*
    inFinancialObj={
        dateTime: null,
        payment: null,
        annualInterestRate: null,
        daysPerYearLabel: null,
        previousOutFinancialObj={}
    }
     */
    let outFinancialObj={
        dateTime: null,
        paid: {
            payment: null,
            payments: null,
            
            debt: null,
            loanDebt: null,
            principal: null,
            interest: null,
            feeDebt: null,
            display:{
                paymentInterest: null,
                paymentPrincipal: null,
                paymentFee: null,
                debt: null,
                loanDebt: null,
                principal: null,
                interest: null,
                feeDebt: null,
            }
        },
        remaining: {
            debt: null,
            loanDebt: null,
            principal: null,
            interest: null,
            feeDebt: null,
            display:{
                debt: null,
                loanDebt: null,
                principal: null,
                interest: null,
                feeDebt: null,
            }
        },
    }
    let dailyInterestRate= await calcDailyInterestRate(inFinancialObj.annualInterestRate, inFinancialObj.daysPerYearLabel, null)
    .catch((err)=> dailyInterestRate = 0.00)
    let dailyAccruedInterest = await calcInterestAccrual(
        inFinancialObj.previousOutFinancialObj.dateTime,
        inFinancialObj.dateTime,
        inFinancialObj.annualInterestRate,
        inFinancialObj.previousOutFinancialObj.remaining.principal,
        inFinancialObj.previousOutFinancialObj.remaining.interest,
        inFinancialObj.daysPerYearLabel )

}

function calcDailyInterestRate(annualInterestRate, daysPerYearLabel, dateTime){
    return new Promise((res, rej)=>{
        if(String(daysPerYearLabel) === String('real/30/360')){ //real/30/360  accural30/360
            res( Number(annualInterestRate)  / Number(360) )
        }
        else if(String(daysPerYearLabel) === String('real/365')){
            res( Number(annualInterestRate) / Number(365) )
        }
        else if(String(daysPerYearLabel) === String('real/real')){
            let currentYear = moment.unix(dateTime).format('YYYY')
            let nextYear = Number(currentYear) + 1
            let startOfCurrentYear = '01-01-' + currentYear
            let startOfNextYear = '01-01-' + nextYear
            let daysInYear = moment(startOfNextYear).diff(moment(startOfCurrentYear), 'days')
            res( Number(annualInterestRate) / Number(daysInYear) )
        }
        else{
            res(0)
        }
    })
}


async function calcPaidPayment2(paymentObj, previousFinancialObj){
    return new Promise(async (res, rej)=>{
        //console.log('2paymentObj:' + paymentObj.payment + '|' + JSON.stringify(previousFinancialObj))


        let financialObj = previousFinancialObj

        //calcScheduledFuturePrincipal(previousFinancialObj.settings.interestStartAccruedDateTime, paymentObj.dateTime,previousFinancialObj.settings.initialPrincipal,previousFinancialObj.settings.annualInterestRate,previousFinancialObj.settings.intervalPayment,previousFinancialObj.settings.intervalLabel,previousFinancialObj.settings.daysPerYearLabel  )
        let interestAccrued = await calcInterestAccrual(previousFinancialObj.dateTime, paymentObj.dateTime, previousFinancialObj.settings.annualInterestRate,previousFinancialObj.remaining.principal,0.00,previousFinancialObj.settings.daysPerYearLabel)
            .catch((err)=>{interestAccrued = 0})
            financialObj.remaining.interest = Number(financialObj.remaining.interest) + Number(interestAccrued)
        console.log('InCalcPaidPayment2' + financialObj.remaining.interest + '| Days:' + moment.unix(paymentObj.dateTime).diff(moment.unix(previousFinancialObj.dateTime) , 'days') + '|payment Date:' + moment.unix(paymentObj.dateTime).format('LLLL'))

        let paymentLessInterest = await calcPaymentLessInterest(paymentObj, financialObj)
            .catch((err)=>{paymentLessInterest = {success: false, data: null, message: 'Error paymentLessInterest'}})
            
        let paymentLessPrincipal = await calPaymentLessPrincipal(paymentObj, paymentLessInterest['data'])
            .catch((err)=>{paymentLessPrincipal = {success: false, data: null, message: 'Error calcPaymentLessPrincipal'}})
        
        let paymentLessFees = await calcPaymentLessFees(paymentObj, paymentLessPrincipal['data'] )
            .catch((err)=>{paymentLessFees = {success: false, data: null, message: 'Error paymentLessFees'}})
        let paymentLessReducePrincipal = await calcPaymentLessReducePrincipal(paymentObj, paymentLessFees['data'])
            .catch((err)=>{paymentLessReducePrincipal = {success: false, data: null, message: 'Error calcPaymentLessReducePrincipal'}})
        let paymentDebt = await calcPaymentDebt(paymentLessReducePrincipal['data'])
        .catch((err)=>{paymentDebt = {success: false, data: null, message: 'Error calcPaymentDebt'}})
        let paymentDisplay = await calcPaymentDisplay(paymentDebt['data'])
        .catch((err)=>{paymentDisplay = {success: false, data: null, message: 'Error calcPaymentDisplay'}})
        if(paymentDisplay['success']){
            paymentDisplay['data']['dateTime'] = paymentObj.dateTime
            paymentDisplay['data']['paid']['payment']= paymentObj.payment
            paymentDisplay['data']['paid']['payments'] = calcTruncNumber((Number(financialObj.paid.payments) + Number(paymentObj.payment)),2)
            res({success: true, data: paymentDisplay['data'], message:'calculated payment'})
        }
        else{
            res({success: false, data: null, message:'NO payment calculated'}) 
        }
        
    })
}

exports.calcPaidPayment = async function(paymentObj,previousFinancialObj){
    return new Promise(async (res, rej)=>{
        let paidPayment = await calcPaidPayment(paymentObj, previousFinancialObj)
        res(paidPayment)
    })
    
}

//payment:{amount, dateTime, interestProportion, principalProportion, feesProportion}, obligation: {interestAccruedDateTime, annualInterestRate,daysPerYearLabel,debt, loanDebt, principal, interest, feeDebt}
async function calcPaidPayment(paymentObj, previousFinancialObj){
    return new Promise( async (res,rej)=>{   
        console.log('paymentObj:' + paymentObj.payment + '|' + JSON.stringify(previousFinancialObj))
        let financialObj = previousFinancialObj
        await calcInterestAccrual(previousFinancialObj.dateTime, paymentObj.dateTime, previousFinancialObj.settings.annualInterestRate,previousFinancialObj.remaining.principal,0.00,previousFinancialObj.settings.daysPerYearLabel)
        .then((interestAccrued)=>{
            //console.log('Interest Accrued:' + interestAccrued)
            financialObj.remaining.interest = Number(financialObj.remaining.interest) + Number(interestAccrued)
            return calcPaymentLessInterest(paymentObj, financialObj)
        })
        .then((paymentLessInterest)=>{
            financialObj = paymentLessInterest['data']
            return calPaymentLessPrincipal(paymentObj,financialObj )
        })
        .then((paymentLessPrincipal)=>{
            //NEEDT TO DO FEES AND THEN EITHER FUTURE PAYMENT OR REDUCE PRINCIPAL paymentDistributionLabel
            financialObj = paymentLessPrincipal['data']
            //console.log(paymentLessPrincipal['message'] + paymentLessPrincipal['data']['remaining']['principal'] )
            return calcPaymentLessFees(paymentObj, financialObj)
        })
        .then((paymentLessFees)=>{
            financialObj = paymentLessFees['data']
            //console.log(paymentLessFees['message'])
            //spit here either reduce principal again or some how apply it to the future? but it doesn't reduce the accruing interest?
            return calcPaymentLessReducePrincipal(paymentObj, financialObj)
        })
        .then((paymentLess)=>{
            financialObj = paymentLess['data']
            //console.log(paymentLess['message'])
            return calcPaymentDebt(financialObj)
             
        })
        .then((paymentDebt)=>{
            
            //console.log('paymentDebt')
            return calcPaymentDisplay(paymentDebt['data'])
        })
        .then((paymentDisplay)=>{
            financialObj = paymentDisplay['data']
            //console.log(paymentDisplay['message'] + paymentDisplay['data']['paid']['display']['debt'] )
        })
        .catch((rejected)=>{
            console.log(rejected)
            //res({success: false, data: null, message: 'calculated No PaidPayment'})
        })
        financialObj.dateTime = paymentObj.dateTime
        financialObj.paid.payment = paymentObj.payment
        financialObj.paid.payments = calcTruncNumber((Number(financialObj.paid.payments) + Number(paymentObj.payment)),2)
        res({success: true, data: financialObj, message: 'calculated PaidPayment'})
        
    })
    
}

function calcPaymentLessInterest(paymentObj, financialObj){
    return new Promise((res, rej)=>{
        let message= ''
        if(paymentObj.paymentInterest === null){ //ATTEMPT TO PAY ALL INTEREST ACCRUED
            if(Number(paymentObj.payment) >= Number(calcTruncNumber(financialObj.remaining.interest,2))){ //PAID All Accrued Interest             
                financialObj.paid.paymentInterest = Number(financialObj.remaining.interest)
                financialObj.paid.interest = Number(financialObj.paid.interest) + Number(financialObj.remaining.interest)
                financialObj.remaining.interest = 0.00
                message ='paid all remaining interest'
            }
            else{ //Only a Portion of Accrued Interest 
                financialObj.paid.paymentInterest = Number(paymentObj.payment)
                financialObj.paid.interest = Number(financialObj.paid.interest) +  Number(paymentObj.payment)
                financialObj.remaining.interest = Number(financialObj.remaining.interest) - Number(paymentObj.payment)
                message= 'paid only a portion of interest'
            }
        }
        else{
            if(Number(paymentObj.paymentInterest) >= Number(calcTruncNumber(financialObj.remaining.interest,2))){ //PAID All Accrued Interest & can go negative if they pay more            
                financialObj.paid.paymentInterest = Number(paymentObj.paymentInterest)
                financialObj.paid.interest = Number(financialObj.paid.interest) + Number(paymentObj.paymentInterest)
                financialObj.remaining.interest = financialObj.remaining.interest - paymentObj.paymentInterest
                message = 'paid only specific amount of interest but all accrued'
            }
            else{ //Only a Portion of Accrued Interest 
                financialObj.paid.paymentInterest = Number(paymentObj.paymentInterest)
                financialObj.paid.interest = Number(financialObj.paid.interest) +  Number(paymentObj.paymentInterest)
                financialObj.remaining.interest = Number(financialObj.remaining.interest) - Number(paymentObj.paymentInterest)
                message = 'paid only specific amount of interest not all the accrued'
            }
        }
        //console.log(message + '| payment: '+ financialObj.paid.payment +'|' + financialObj.paid.paymentInterest + '|' + financialObj.remaining.interest )
        res({success: true, data: financialObj, message:'paid_accrued_interest' })
    })
}
function calPaymentLessPrincipal(paymentObj, financialObj){
    return new Promise((res, rej)=>{
        let message= 'In calcPaymentLessPrincipal'
        //How do I know how much principal should be paid? paymentObject would have it or everything goes to principal less interest paid
        if(paymentObj.paymentPrincipal === null){ //Everything less interest goes to principal
            if(Number(paymentObj.payment) > Number(financialObj.paid.paymentInterest)){ // there IS an amount that can reduce principal
                //need to check stop negative
                //Need to figure out how much principal is scheduled to be reduced by now!!!
                let paidPrincipal = Number(paymentObj.payment) - Number(financialObj.paid.paymentInterest)
                financialObj.paid.paymentPrincipal = paidPrincipal
                financialObj.paid.principal = Number(financialObj.paid.principal) + Number(paidPrincipal)
                financialObj.remaining.principal = Number(financialObj.remaining.principal) - Number(paidPrincipal)
                message = 'Reduce Principal by remaining payment amount'
            }
            else{ //NOTHING left to reduce principal
                financialObj.paid.paymentPrincipal = 0
                message = 'No Reduction in principal'
            }
        }
        else{ // only a specific amount goes to principal
            financialObj.paid.paymentPrincipal = paymentObj.paymentPrincipal
            financialObj.paid.principal = Number(financialObj.paid.principal) + Number(paymentObj.paymentPrincipal)
            financialObj.remaining.principal = Number(financialObj.remaining.principal) - Number(paymentObj.paymentPrincipal)
            message = 'Reduce principal only by a specified amount'
        }
       // console.log(message + financialObj.paid.paymentPrincipal + ' remaining:' + financialObj.remaining.principal)
        res({success:true, data: financialObj, message: message})
    })  
}
function calcPaymentLessFees(paymentObj, financialObj){
    return new Promise((res, rej)=>{
        let message= 'In calcPaymentLessFees'
        if(Number(financialObj.remaining.feesDebt) > 0.01 ){ //FEES to pay
            message= 'calcPaymentLessFees> remaining fees'
        }
        else{ //No fees to pay
            message= 'calcPaymentLessFees> No Fees'
        }
        /*
        if(paymentObj.paymentFee === null){ //pay any remaining fees if excess funds
            if( (Number(financialObj.paid.paymentPrincipal) + Number(financialObj.paid.paymentInterest)) > Number(paymentObj.payment) ){

            }
            else{
                message= 'In calcPaymentLessFees>no amount left for fees' 
            }

        }
        else{// Specific amount of fees specified

        }*/
        res({success: true, data: financialObj, message: message})
    })
}
function calcPaymentLessReducePrincipal(paymentObj, financialObj){
    return new Promise((res, rej)=>{
        let message= 'In calcPaymentLessReducePrincipal'
        res({success: true, data: financialObj, message: message})
    })
}
function calcPaymentLessReduceFuturePayment(paymentObj, financialObj){
    return new Promise((res, rej)=>{
        let message= 'In calcPaymentLessReduceFuturePayment'
        res({success: true, data: financialObj, message: message})
    })
}
function calcPaymentDebt(financialObj){
    return new Promise((res, rej)=>{
        let message= 'In calcPaymentDebt'
        financialObj.paid.loanDebt = Number(financialObj.paid.principal) + Number(financialObj.paid.interest)
        financialObj.paid.debt = Number(financialObj.paid.loanDebt) + Number(financialObj.paid.feeDebt)
        financialObj.remaining.loanDebt = Number(financialObj.remaining.principal) + Number(financialObj.remaining.interest)
        financialObj.remaining.debt = Number(financialObj.remaining.loanDebt) + Number(financialObj.remaining.feeDebt)
        //console.log(message + financialObj.paid.loanDebt)
        res({success: true, data: financialObj, message: message})
    })
}
function calcPaymentDisplay(financialObj){
    return new Promise((res, rej)=>{
        let message= 'In calcPaymentDisplay'
        
        financialObj.paid.display.paymentPrincipal = calcTruncNumber( (Math.round(Number(financialObj.paid.paymentPrincipal) * 100) / 100), 2)
        financialObj.paid.display.paymentInterest = calcTruncNumber( (Math.round(Number(financialObj.paid.paymentInterest) * 100) / 100), 2)
        financialObj.paid.display.paymentFee = calcTruncNumber( (Math.round(Number(financialObj.paid.paymentFee) * 100) / 100), 2) 
        financialObj.paid.display.principal = calcTruncNumber( (Math.round(Number(financialObj.paid.principal) * 100) / 100), 2)
        financialObj.paid.display.interest =  calcTruncNumber( (Math.round(Number(financialObj.paid.interest) * 100) / 100), 2)
        financialObj.paid.display.loanDebt = calcTruncNumber( (Math.round(Number(financialObj.paid.loanDebt) * 100) / 100), 2)
        financialObj.paid.display.debt = calcTruncNumber( (Math.round(Number(financialObj.paid.debt) * 100) / 100), 2)
        
        financialObj.remaining.display.debt = calcTruncNumber( (Math.round(Number(financialObj.remaining.debt) * 100) / 100), 2)
        financialObj.remaining.display.loanDebt= calcTruncNumber( (Math.round(Number(financialObj.remaining.loanDebt) * 100) / 100), 2)
        financialObj.remaining.display.principal= calcTruncNumber( (Math.round(Number(financialObj.remaining.principal) * 100) / 100), 2)
        financialObj.remaining.display.interest= calcTruncNumber( (Math.round(Number(financialObj.remaining.interest) * 100) / 100), 2)
        financialObj.remaining.display.feeDebt = calcTruncNumber( (Math.round(Number(financialObj.remaining.feeDebt) * 100) / 100), 2)
        //console.log('calcPaymentdisplays:'+ financialObj.remaining.display.debt)
        res({success: true, data: financialObj, message: message})
    })
}
function calcLastPayment(dateTime, financialObj){
    return new Promise(async(res, rej)=>{
        let message= 'In calcLastPayment - No Fees'
        //console.log('second to last'+JSON.stringify(financialObj))
        let accruedInterest = await calcInterestAccrual(financialObj.dateTime, dateTime, financialObj.settings.annualInterestRate, financialObj.remaining.principal, financialObj.remaining.interest, financialObj.settings.daysPerYearLabel)
        .catch((err)=>{accruedInterest = 0.00})
        //console.log('accruedInterest:'+accruedInterest)
        let lastPayment =  calcTruncNumber( (Math.round( (Number(financialObj.remaining.principal) + Number(accruedInterest))  * 100) / 100), 2)
        //console.log(message + lastPayment)
        res(lastPayment)
    }) 
}
exports.calcIntervalPayment = async function(principal, intervals, annualInterestRate, intervalLabel, daysPerYearLabel, cutOrRound, interestAccruedDateTime, firstPaymentDateTime, lastPaymentDateTime){
    return calcIntervalPayment(principal, intervals, annualInterestRate, intervalLabel, daysPerYearLabel, cutOrRound, interestAccruedDateTime, firstPaymentDateTime, lastPaymentDateTime)
}
async function calcIntervalPayment(principal, intervals, annualInterestRate, intervalLabel, daysPerYearLabel, cutOrRound, interestAccruedDateTime, firstPaymentDateTime, lastPaymentDateTime){
  return new Promise((res, rej)=>{   
    //console.log(principal +"|" + intervals +'|'+annualInterestRate+'|'+cutOrRound+'|'+interestAccruedDateTime+'|'+firstPaymentDateTime)
    
    let daysPerYear = 365
    let intervalsPerYear = 12
    let momentFreqValue = 'M' //Change based on IntervalLabel

    let extraDays=0
    if(interestAccruedDateTime <  moment.unix(firstPaymentDateTime).subtract(1, momentFreqValue).unix() ){
        extraDays = moment(moment.unix(firstPaymentDateTime).subtract(1, momentFreqValue)).diff(moment.unix(interestAccruedDateTime) , 'days')
    }
    //totalDays = totalDays +  Number(moment.unix(firstPaymentDateTime).add(intervals, momentFreqValue).diff(moment.unix(firstPaymentDateTime), 'days' ))
    //FV =  P(1+(AR/IPY)^(IPY * (N/IPY + (D/DPY))
    let air = Number(annualInterestRate)/Number(intervalsPerYear) //Annual Interval Rate
    //console.log('annual interval Rate:' + air)
    let years = (Number(intervals) / (intervalsPerYear)) + (Number(extraDays)/ Number(daysPerYear)) //time in years
    let futureValue = principal * Math.pow((1 + Number(air)),(Number(intervalsPerYear) * Number(years)))
    console.log('futureValue:' + futureValue)
    //IP = FV*IR / ((1 + IR)^n -1)
    let floatPayment = (Number(futureValue) * Number(air)) / (Math.pow((1 + Number(air)),(intervals)) - 1 )

    if(cutOrRound === 'cut'){
        res(calcTruncNumber(floatPayment  , 2))
    }
    else{
        res(calcTruncNumber(Math.round(Number(floatPayment) * 100) / 100, 2))
    }
  })
}

function calcFutureValue(principal, annualInterestRate,  intervals, intervalLabel, daysPerYearLabel, interestAccruedDateTime, firstPaymentDateTime,){
    return new Promise((res, rej)=>{
    
        let daysPerYear = 365
        let intervalsPerYear = 12
        let momentFreqValue = 'M' //Change based on IntervalLabel
    
        let extraDays=0
        if(interestAccruedDateTime <  moment.unix(firstPaymentDateTime).subtract(1, momentFreqValue).unix() ){
            extraDays = moment(moment.unix(firstPaymentDateTime).subtract(1, momentFreqValue)).diff(moment.unix(interestAccruedDateTime) , 'days')
        }
        //totalDays = totalDays +  Number(moment.unix(firstPaymentDateTime).add(intervals, momentFreqValue).diff(moment.unix(firstPaymentDateTime), 'days' ))
        //FV =  P(1+(AR/IPY)^(IPY * (N/IPY + (D/DPY))
        let air = Number(annualInterestRate)/Number(intervalsPerYear) //Annual Interval Rate
        //console.log('annual interval Rate:' + air)
        let years = (Number(intervals) / (intervalsPerYear)) + (Number(extraDays)/ Number(daysPerYear)) //time in years
        let futureValue = principal * Math.pow((1 + Number(air)),(Number(intervalsPerYear) * Number(years)))

    res(futureValue)
     })

}
function calcScheduledFuturePrincipal(startDateTime, endDateTime, initialPrincipal, annualInterestRate, intervalPayment, intervalLabel, daysPerYearLabel, ){
    return new Promise((res, rej)=>{
        let daysPerYear = 365
        let intervalsPerYear = 12
        let momentFreqValue = 'M'

        let IAPR = Number(annualInterestRate) / Number(intervalsPerYear)  //Interval Annual Percentage Rate
        let totalDays =  Number(moment.unix(endDateTime).diff(moment.unix(startDateTime) ,'days'))
        let T = (Number(intervalsPerYear) * (Number(totalDays)) / Number(daysPerYear) )
        let numerator = Math.pow((1 + Number(IAPR)),(intervalsPerYear)) - Math.pow((1 + Number(IAPR) ),(T))
        let denominator = Math.pow((1 + Number(IAPR)),intervalsPerYear) -1
        let futurePrincipal =  (Number(initialPrincipal) * (numerator)) / (denominator)
        console.log(moment.unix(endDateTime).format('LLLL') + '|' +totalDays +'|'+ T +'|'+ futurePrincipal)
        res(futurePrincipal)

    })
}
function calcIntervalPaymetFromFutureValue(){}
function calcIntervalPaymetFromPresentValue(){}



function calcPercentOfDays(fromDateTime, toDateTime, daysPerYearLabel){
    return new Promise((res, rej)=>{
        if(fromDateTime > toDateTime){
            res(0)
        }
        else{
            
            if(String(daysPerYearLabel) === String('real/30/360')){ //real/30/360  accural30/360
                
                let daysDiff = Number(360) * (Number(moment.unix(toDateTime).format('YYYY')) - Number(moment.unix(fromDateTime).format('YYYY')) ) +  30 * ( Number(moment.unix(toDateTime).format('MM')) - Number(moment.unix(fromDateTime).format('MM') ) ) + Number(moment.unix(toDateTime).format('DD') ) - Number( moment.unix(fromDateTime).format('DD'))   
                //console.log('IN 30/360' + (daysDiff / Number(360)) )
                res(daysDiff / Number(360) ) 
            }
            else if(String(daysPerYearLabel) === String('real/365')){
                
                let daysDiff = moment.unix(toDateTime).diff(moment.unix(fromDateTime) , 'days')
                //console.log('IN real/365: ' + ( Number(daysDiff) / Number(365) ) )
                res(Number(daysDiff) / Number(365) ) 
            }
            else{
                //console.log('IN None')
                res(0)
            }     
        }
    })
}

function calcDisplayValues(row){
    //row: payment, paidInterest, paidPrincipal, principal
    let payment = calcTruncNumber(row.payment, 2)
    let paidPrincipal = calcTruncNumber( (Math.round(Number(row.paidPrincipal) * 100) / 100), 2)
    let paidInterest =  calcTruncNumber( ( Math.round((Number(payment) * 100) - (Number(paidPrincipal) * 100) ) / 100), 2)

    let principal = null
    //console.log('row.displayPrincipal' + row.previousDisplayPrincipal)
    if(row.previousDisplayPrincipal){
        //console.log('previousDisplay:'+row.previousDisplayPrincipal + '|' + paidPrincipal)
        principal =  calcTruncNumber(Math.round((Number(row.previousDisplayPrincipal) * 100) - (Number(paidPrincipal) * 100) ) / 100, 2)
    }
    else{
        principal = calcTruncNumber(row.principal, 2)
    }
     
    return {payment: payment, paidInterest: paidInterest, paidPrincipal: paidPrincipal, principal: principal}
}

function calcTruncNumber(inNum, digits){
    let outNum = '0'
    if(parseFloat(inNum) && isFinite(inNum) ){
    //currently digits always 2
    //console.log(inNum)
    let splitNum = String(inNum).split('.')
    //console.log(typeof(splitNum[1]))
    if( typeof(splitNum[1]) === 'undefined' ){
        splitNum[1] = '00'
    }
    else{splitNum[1] = splitNum[1] + '00'}
        outNum = String(splitNum[0]) + '.' + String(splitNum[1]).substr(0,2)
    }
    return outNum
}
