var express = require('express');
var router = express.Router();
var moment = require('moment');


var ObligationController = require('../controllers/obligation')
var ActController = require('../controllers/act.ts')
var ObligationActController = require('../controllers/obligation_acts.ts')
var ObligationCalcController = require('../controllers/obligation_calculations.ts')
var ObligationCalcOrigController = require('../controllers/obligation_calculations_orig.ts')

router.get('/account/person', async function(req, res){
    //console.log('get loans by' + req.query.personId + '|' + req.query.accountId)
    let loans = await ObligationController.findPersonLoans({personId: req.query.personId, accountId:  req.query.accountId})
    //console.log(JSON.stringify(loans))
    res.send(loans)
})

router.post('/create2', async function(req, res){
    //Creating a loan does 3 things: (1) creates Obligation Object, (2) creates an Asset Object, (3) creates an Financial Act.


    let createLoanObj = {
        type: 'loan',
        personId: 'person123', //req.body['personId'],
        accountId: 'account123', // req.body['accountId'],
        assetId: null, // req.body['assetId'],
       
        debt: 2323, //req.body['debt'],
        loanDebt: 2323, // req.body['loanDebt'], 
        interest: 0.00, // req.body['interest'],
        principal: 2323, //req.body['principal'],
        feeDebt: 0.00, // req.body['feeDebt'],
        intervalLabel: 'monthly', // req.body['intervalLabel'],
        intervals: 12, //req.body['intervals'],
        intervalPayment: null, // req.body['intervalPayment'],
        annualInterestRate: 0.34, // req.body['annualInterestRate'],
        compoundLabel: 'simple', //req.body['compoundLable'],
        
        originalDebt: 2323, // req.body['originalDebt'],
        originalLoanDebt: 2323, // req.body['originalLoanDebt'],
        originalPrincipal: 2323, // req.body['originalPrincipal'],
        originalInterest: 0.00, //req.body['originalInterest'],
        originalFeeDebt: 0.00, // req.body['originalFeeDebt'],

        paidDebt: 0.00, // req.body['paidDebt'],
        paidLoanDebt: 0.00, //req.body['paidLoanDebt'], 
        paidInterest: 0.00, //req.body['paidInterest'],
        paidPrincipal: 0.00, //req.body['paidPrincipal'],
        paidFeeDebt: 0.00, // req.body['paidFeeDebt'],
        
        //amortizationTable: [amortizationTableSchema],
        projectedTotalAllPayments: null, //req.body['totalAllPayments'],
        projectedTotalAllInterest: null, //req.body['totalAllInterest'],
        projectedTotalAllPrincipal: null, //req.body['totalAllPrincipal'],
        
        contractCreatedDateTime: moment().unix(), //req.body['contractCreatedDateTime'], 
        contractStartInterestAccuralDateTime: moment().unix(), //req.body['contractStartInterestAccuralDateTime'], 
        interestAccruedDateTime: moment().unix(),//req.body['interestAccruedDateTime'], 
        projectedEndDateTime: null, //req.body['projectedEndDateTime'],
        projectedFirstPaymentDateTime: moment().add(1, 'M').unix(), // req.body['projectedPaymentDateTime'],
       // projectedLastPaymentDateTime:  
        objectCreatedByPersonId: req.body['CreatedByPersonId'],
        objectCreatedDateTime: moment().unix(),
    }
    ObligationController.orchistrateCreateLoan(createLoanObj)
    createAssetObj



    res.sendStatus(200)
})

router.post('/checkCreate', async function(req,res){
    //cal Amoritization table & data
    console.log(JSON.stringify(req.body))   
    //res.send(await ObligationCalcOrigController.calcAmortizationTable2(req.body))

    res.send(await ObligationCalcController.calcCheckCreate(req.body))

})

router.post('/create', async function(req, res){
    //console.log('create body:'+JSON.stringify(req.body))
    //createAnAsset

    //createAnObligation for the asset
    ObligationController.orchistrateCreateLoan(
        req.body['personId'],                                       //personId who has the oblication
        req.body['accountId'],                                      //accountId
        null,                                                       //who is putting in the obligation
        req.body['dateTime'],                                       //interestFromDate
        moment.unix(req.body['dateTime']).add(1, 'M').unix() ,      //firstPaymentDate 1 month from interestFromDate
        req.body['loanDebt'],                                       //InitalPrincipalAmount
        req.body['annualInterestRate'],                             //annualInterestRate 0.65
        req.body['intervals'],                                      //intervals 12
        req.body['frequencyLabel'],                                 //intervalLabel monthly
        'simple',                                                   //compound
        '30/360',                                                   //Year type
        null,                                                       //enddate
        'assetId',                                                  //assetId                               
    )
    

    ObligationCalcController.calcAmortizationTable(
        req.body['dateTime'],                                       //interestFromDate
        moment.unix(req.body['dateTime']).add(1, 'M').unix() ,      //firstPaymentDate 1 month from interestFromDate
        req.body['loanDebt'],                                       //InitalPrincipalAmount
        req.body['annualInterestRate'],                             //annualInterestRate 0.65
        req.body['intervals'],                                      //intervals 12
        req.body['frequencyLabel'],                                 //intervalLabel monthly
        null,                                                       //intervalPayment
        'simple',                                                   //compound
        '30/360',                                                   //Year type
        null,                                                       //enddate
    )

    let data = {
        type: 'loan',
        personId: req.body['personId'],
        accountId: req.body['accountId'],
        
        dateTime: req.body['dateTime'],
        debt: Number(req.body['loanDebt']), //total due
        loanDebt: Number(req.body['loanDebt']), 
        interest: 0.0,
        principal: Number(req.body['loanDebt']), //** I don't show exactly how much principal they have paied but this is what they owe */
        feeDebt: 0.0,
        frequencyLabel: req.body['frequencyLabel'],
        interval: req.body['intervals'],
        annualInterestRate: req.body['annualInterestRate'],
        intervalInterestRate: null,
        compoundLabel: req.body['compoundLabel'],
        
        year: req.body['year'],
        make: req.body['make'], 
        model: req.body['model'], 
        vin: req.body['vin'], 
        additionalInformation: req.body['additionalInformation'],

        paymentSchedule: [],
        intervalPayment: null,
        totalAllPayments: null,
        totalAllInterest: null,
        originalPrincipal: req.body['loanDebt'],

        qaCalcs: {},
        //eventId: null,
        when: {created: moment().unix(), interestAccrued: req.body['dateTime']}
    }
    let frequency = await ObligationController.convertLabelToInterval(null, data.frequencyLabel)
    let compound = await ObligationController.convertLabelToInterval(data.compoundLabel, data.frequencyLabel)
    

    data.intervalInterestRate = Number(await ObligationController.intervalInterestRate(data.annualInterestRate, frequency, compound))
    data.intervalPayment = Number(await ObligationController.calcPayment(data.principal, data.intervalInterestRate, data.interval))
    data.totalAllPayments = Number(await ObligationController.calcProjectedPaymentTotal(data.intervalPayment, data.interval))
    data.totalAllInterest = Number(await ObligationController.calcProjectedInterestTotal(data.principal, data.totalAllPayments))
    
    let schedule = await ObligationController.calcProjectedSchedule(data.loanDebt, data.interval, data.intervalInterestRate, frequency, data.dateTime, data.intervalPayment)
    data.paymentSchedule = schedule['data']
    data.qaCalcs = await ObligationController.calcProjectedTotalsFromSchedule(schedule['data'], data.intervalPayment, req.body['loanDebt'],req.body['dateTime'],req.body['annualInterestRate']  )
    //console.log(JSON.stringify(data))
    let createInitialLoan = await ObligationController.createLoan({$and: [{personId: data.personId}, {accountId: data.accountId}, {vin: data.vin}, {dateTime: data.dateTime} ]}, data)
    //console.log('createInitialLoan' + createInitialLoan['success'])
    let createAct = await ActController.createAct({
        type: ['financial','create_loan'],
        personId: createInitialLoan.data.personId,
        accountId: createInitialLoan.data.accountId,
        obligationId: createInitialLoan.data._id,
    
        dateTime: createInitialLoan.data.dateTime,
        
        amount: req.body['loanDebt'],
        debt: createInitialLoan.data.debt, 
        loanDebt: createInitialLoan.data.loanDebt,
        interest: createInitialLoan.data.interest,
        principal: createInitialLoan.data.principal,
        feeDebt: createInitialLoan.data.feeDebt,
        
        when: {created: moment().unix()}
    })

    if(createInitialLoan.success && createAct.success){
        res.send({success: true, data: createInitialLoan, message: 'created_loan'})
    }
    else{
        res.sendStatus(500)
    }
})

router.get('/read', async function(req,res){
    let foundLoan = await ObligationController.findLoan({_id: req.query.obligationId})
    if(foundLoan.success){
        res.send(foundLoan)
    }
    else{
        res.sendStatus(500)
    }
    
})



router.put('/payment/create', async function(req, res){
    ObligationActController.createObligationAct(req.body.amount, req.body.dateTime, req.body.personId, req.body.accountId, req.body.obligationId, req.body.type, req.body.excessAmount)
    let returnObj={success:false, data:null, message:'payment_not_created'}
    ObligationCalcController.calcObligationInterestAccrual(req.body.dateTime,req.body.obligationId, null)
    ObligationCalcController.calcObligationPrincipalAccrual(req.body.dateTime,req.body.obligationId, null)
    
    //console.log('in create payment' + JSON.stringify(req.body) )
    let distributePayment = Number(req.body.amount)
    //let principal = Number(0.00)
    //let interest = Number(0.00)
    let accruedInterest = 0
    let reduceInterest = 0
    let paymentPrincipal = 0
    let reduceFee = 0
    let reducePrincipal = 0

    let initialAct = await ActController.createAct({
        type: ['financial', 'customer_payment'], // finanncial (create_loan...) / communication
        personId: req.body.personId,
        accountId: req.body.accountId,
        obligationId: req.body.obligationId,
        dateTime: req.body.dateTime,
        amount: req.body.amount,
        interestReduction: null,
        principalReduction: null,
        feeReduction: null,
        accuredInterest: null,
        
        debt: null, 
        loanDebt: null, 
        interest: null,
        principal: null,
        feeDebt: null,

        foriegnIds: [],
        when: {created: moment().unix()}
    })
    .catch(returnObj={success: false, data: null, message:'no_act_created'})

    let loan = await ObligationController.findLoan({_id: req.body.obligationId})
    .catch(returnObj={success: false, data: null, message:'no_obligation_created'})


    if(loan.success && initialAct.success){
        //console.log('loan & inital act success')
        initialAct['data'].debt = loan['data'].debt
        initialAct['data'].loanDebt = loan['data'].loanDebt
        initialAct['data'].interest = loan['data'].interest
        initialAct['data'].principal = loan['data'].principal
        initialAct['data'].feeDebt = loan['data'].feeDebt

        if(String(req.body.type).includes('customer_payment')){
            if(String(loan['data'].intervalPayment) === String(req.body.amount)){
                initialAct['data'].type.push('payment_exact')
            }
            else if(loan.intervalPayment > Number(req.body.amount)) {
                initialAct['data'].type.push('payment_insufficient')
            }
            else{
                initialAct['data'].type.push('payment_excess')
            }   
        }
        
        let dateTimeInterestAccrued =loan['data']['when']['interestAccrued']
        //console.log(req.body.dateTime  + '|' + dateTimeInterestAccrued )
        if(req.body.dateTime > dateTimeInterestAccrued){
           //console.log(dateTimeInterestAccrued +'|'+ req.body.dateTime+'|'+ loan['annualInterestRate'] +'|'+loan['data']['principal'] )
            accruedInterest  = await ObligationController.calcAccruedInterest(dateTimeInterestAccrued,req.body.dateTime, loan['data']['annualInterestRate'],loan['data']['principal']  )
            .catch((err)=> returnObj={success:false, data:null, message:'principal_not_updated: accruedInterest_not_found' + err})
            dateTimeInterestAccrued = req.body.dateTime
        }
        //console.log('accruedInterest' + accruedInterest )

       // let accruedInterest = await ObligationController.calcAccruedInterest(loan['data']['when']['interestAccrued'], req.body.dateTime, loan['data']['annualInterestRate'], loan['data']['principal'])
        
        loan['data']['when']['interestAccrued'] = dateTimeInterestAccrued
        distributePayment = distributePayment - loan['data'].interest - accruedInterest // Reducess any accrrued interest + interest previously accrued in the loan by the payment. 
        //console.log('String is' + String(req.body.excessAmount))
        if(distributePayment >= 0 && ( String(req.body.excessAmount) === 'principal_ignoreFees' ||String(req.body.excessAmount) ==='null') ){
            loan['data'].principal = loan['data'].principal - distributePayment
            loan['data'].loanDebt = loan['data'].loanDebt - distributePayment 
            loan['data'].debt = loan['data'].debt - distributePayment 
            loan['data'].interest = 0.00
            initialAct['data'].principalReduction = distributePayment
            initialAct['data'].accuredInterest = accruedInterest
        }
        else if(distributePayment >= 0 && ( String(req.body.excessAmount) === 'principal_fees' ) ){
            //If there is anything left 
            if( (Number(req.body.amount) - Number(loan['data'].intervalPayment)) > 0.0){ //there isn't any extra for fees it just covers the payment
                loan['data'].principal = loan['data'].principal - distributePayment
                loan['data'].loanDebt = loan['data'].loanDebt - distributePayment 
                loan['data'].debt = loan['data'].debt - distributePayment 
                loan['data'].interest = 0.00
                initialAct['data'].principalReduction = distributePayment
                initialAct['data'].accuredInterest = accruedInterest
            }
            else{ // there is extra above the payment
         
            }

           /*   
            loan['data'].principal = loan['data'].principal - (loan['data'].intervalPayment - distributePayment)
            loan['data'].loanDebt = loan['data'].loanDebt - (loan['data'].intervalPayment - distributePayment)
            loan['data'].debt = loan['data'].debt - distributePayment 
            loan['data'].interest = 0.00
            initialAct['data'].principalReduction = distributePayment
            initialAct['data'].accuredInterest = accruedInterest
*/
            //there could be excess fees paid

        }


        else{ //the interest in distributed payment is negative. Thus minus is acutally adding it.

            loan['data'].loanDebt = loan['data'].loanDebt - distributePayment 
            loan['data'].debt = loan['data'].debt - distributePayment
            loan['data'].interest = loan['data'].interest - distributePayment            
            initialAct['data'].principalReduction = 0.00
            initialAct['data'].accuredInterest = -1 * distributePayment
        }
         
        let idx=0
        let paymentidx = 0
        //let spansNextInterval = false
        for(let scheduledPayment of loan['data'].paymentSchedule){
            if(moment(moment.unix(scheduledPayment.dateTime ).format("YYYY-MM-DD")).isSameOrBefore(moment.unix(req.body.dateTime).format("YYYY-MM-DD")) && !scheduledPayment.eventId){
                paymentidx = idx
                scheduledPayment.eventId = initialAct['data']['_id'];
            }
            idx++                    
        }

        //UPDATE loan & event
        let replaceLoan = await ObligationController.replaceLoan({_id: loan['data']._id}, loan['data']) 
        let replaceAct = await ActController.replaceAct({_id: initialAct['data']._id}, initialAct['data'])  
        if(replaceLoan.success && replaceAct.success){
            returnObj ={success: true, data: {loan: loan['data'], event: initialAct['data']}, message: 'payment_created'}
        }
        else{
            returnObj ={success: false, data: null, message: 'payment_replace_failed'}
        }

    }
    else if(initialEvent.success && !loan.success){
        //Rollback initial event
        console.log('roll back payment/create initialEvent')
    }
    else{
        //Rollback initial event
        console.log('roll back payment/create initialEvent')
    }
     
    res.send(returnObj)
})

router.put('/fee/create', async function(req, res){
    let returnObj={success:false, data:null, message:'fee_not_created'}

    let type = []
    let amount = Number(req.body.amount)

    if(String(req.body.type).includes('credit_fee') ){
        type= ['financial','fee', 'increase']
    }
    if(String(req.body.type).includes('debit_fee') ){
        type= ['financial','fee', 'decrease']
        amount = amount * (-1)
    }
    //console.log('in create fee' + JSON.stringify(req.body) )
    //Allow Fees to be entered before or after payments... this will have the problem of not deailing with previous excess payments correctly
    let foundObligation = await ObligationController.findLoan({_id: req.body.obligationId})
    .catch((err)=>console.log('error fee/create foundObligation'))
    foundObligation['data']['debt'] = (Number(foundObligation['data']['debt']) +  Number(amount))
    foundObligation['data']['feeDebt'] = (Number(foundObligation['data']['feeDebt']) +  Number(amount))

    let initalAct = await ActController.createAct({
        type: type,
        personId: foundObligation['data']['personId'],
        accountId: foundObligation['data']['accountId'],
        obligationId: req.body.obligationId,
        dateTime: req.body.dateTime,
        amount: req.body.amount,

        debt: foundObligation['data']['debt'], 
        loanDebt: foundObligation['data']['loanDebt'],  
        interest: foundObligation['data']['interest'],  
        principal: foundObligation['data']['principal'],
        feeDebt: foundObligation['data']['feeDebt'],
        when: {created: moment().unix()},
    })
    .catch((err)=>console.log('error fee/create initialAct'))

    console.log('New FeeDebt' + foundObligation['data']['feeDebt'] )
    let replaceObligation = await ObligationController.replaceLoan({_id: foundObligation['data']['_id']}, foundObligation['data'])
    .catch((err)=>console.log('error fee/create replaceObligation'))
    if(replaceObligation['success'] && initalAct['success']){
        //console.log('Fee Success' + )
        returnObj = {success: true, data: foundObligation['data'], message: 'fee_created'}
    }
    else{
        console.log('roll back')
    }
    res.send(returnObj)
})

router.put('/principal/update', async function(req, res){
    let returnObj={success:false, data:null, message:'principal_not_updated'}
    let type = []
    let amount = Number(req.body.amount)
    let dateTimeInterestAccrued

    if(String(req.body.type).includes('credit_principal') ){
        type= ['financial','principal', 'increase']
    }
    if(String(req.body.type).includes('debit_principal') ){
        type= ['financial','principal', 'decrease']
        amount = amount * (-1)
    }
    //need to calculate daily interest and then adjust loan
    let foundObligation = await ObligationController.findLoan({_id: req.body.obligationId})
    .catch((err)=>returnObj={success:false, data:null, message:'principal_not_updated: Obligation_not_found'})
    dateTimeInterestAccrued = foundObligation['data']['when']['interestAccrued']
    let accrruedInterest = 0

    if(req.body.dateTime > dateTimeInterestAccrued){
        accrruedInterest  = await ObligationController.calcAccruedInterest(dateTimeInterestAccrued,req.body.dateTime,foundObligation['data']['annualInterestRate'],foundObligation['data']['principal']  )
        .catch((err)=> returnObj={success:false, data:null, message:'principal_not_updated: accruedInterest_not_found' + err})
        dateTimeInterestAccrued = req.body.dateTime

    }
    foundObligation['data']['when']['interestAccrued'] = dateTimeInterestAccrued
    foundObligation['data']['interest'] = foundObligation['data']['interest'] + accrruedInterest
    foundObligation['data']['principal'] = foundObligation['data']['principal'] + amount
    foundObligation['data']['loanDebt'] = foundObligation['data']['interest'] + foundObligation['data']['principal']
    foundObligation['data']['debt'] =  foundObligation['data']['loanDebt'] + foundObligation['data']['feeDebt']
    let updateLoan = await ObligationController.updateLoan({_id: foundObligation['data']['_id']}, foundObligation['data'])
    .catch((err)=>console.log(JSON.stringify(err)))
   // console.log(JSON.stringify(updateLoan))
    let initalAct = await ActController.createAct({
        type: type,
        personId: foundObligation['data']['personId'],
        accountId: foundObligation['data']['accountId'],
        obligationId: req.body.obligationId,
        dateTime: req.body.dateTime,
        amount: req.body.amount,

        debt: foundObligation['data']['debt'], 
        loanDebt: foundObligation['data']['loanDebt'],  
        interest: foundObligation['data']['interest'],  
        principal: foundObligation['data']['principal'],
        feeDebt: foundObligation['data']['feeDebt'],
        when: {created: moment().unix()},
    })
    .catch((err)=>console.log('error fee/create initialAct'))
      
    if(initalAct['success']){
        returnObj={success: true, data:{loan: foundObligation['data']}, message:'principal_updated'}
    }
    else{
        console.log('roll back')
    }
    res.send(returnObj)

})



module.exports = router;