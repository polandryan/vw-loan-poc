var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

var EmailController = require('../controllers/email')
var AccountController = require('../controllers/account')
var PersonController = require('../controllers/person')

/*########################################################################################################################################### */

router.get('', async function(req, res){
   // console.log('in get account for '+ req.query.accountIds)
    let splitAccountIds =  String(req.query.accountIds).split(',')
    let findIdString = []
    for(let accountId of splitAccountIds){
       findIdString.push(mongoose.Types.ObjectId(accountId))
    }
    await AccountController.getAccounts({'_id': { $in: findIdString }})
    .then(resolved=>{res.send(resolved)})
    .catch(rejected=>{res.send(rejected)})
    //res.sendStatus(500)
})

router.post('/create',  async function(req, res){
    let data = req.body;
    let proceed = true
    if(!data.email){proceed = false}
    //console.log(data.email)
    let findEmail = await EmailController.findEmail({email: data.email});

    if(findEmail.message == 'no_email_found' && proceed){ //no email found
        console.log('create account person and email')
        let createInitalAccount = await AccountController.createAccount(data.email)
        let createInitalPerson = await PersonController.createPerson()
        let createInitalEmail = await EmailController.createEmail(data.email, data.password)
        if(createInitalAccount.success && createInitalPerson.success && createInitalEmail.success){
            //console.log('update enteries' + createInitalAccount.data._id )
            let updateAccount = await AccountController.updateAccount({_id: createInitalAccount.data._id},{$push: {emailIds: {id: String(createInitalEmail.data._id), tier: 'ACCOUNT_ADMIN' }}})
            let updatePerson = await PersonController.updatePerson({_id: createInitalPerson.data._id}, {$push: {accountIds: String(createInitalAccount.data._id), emailIds: String(createInitalEmail.data._id)}, $set: {currentAccountId: createInitalAccount.data._id, currentAccountTier: 'ACCOUNT_ADMIN'} } )
            let updateEmail = await EmailController.updateEmail({_id: createInitalEmail.data._id}, {$push: {accountIds: String(createInitalAccount.data._id)}, $set: {personId: createInitalPerson.data._id}})
            if(updateAccount.success && updatePerson.success && updateEmail.success){
                //return for verify code
                res.send({sucees: true, data: {verification: {urlParam: createInitalEmail.data.verification.urlParam}}, message: 'new_account_ready_verification'})
            }
            else{//Errored roll back
                res.sendStatus(500)
            }
        }
        else{//roll back the creations
            res.sendStatus(500)
        }
    }
    else if(findEmail.message == 'email_found'){
        res.sendStatus(500)
        console.log('1) verify password then create new account ')
    }
    else{
        res.sendStatus(500)
        console.log('some kind of error' + JSON.stringify(findEmail))
    }
})

router.delete('/email', async function(req, res){
    let data = req.query //accountId, emailId, accountEmailId (object row _id)
    console.log('delete ' + data.accountEmailId + ' from account ' + data.accountId + 'which removes email ' + data.emailId )
    let returnObj = {success: false, data: null, message: 'nothing'}
    let findAccount = await AccountController.findAccount({'_id': data.accountId})
    if(findAccount.success){
        let deleteAccountEmail = await AccountController.deleteAccountEmail(data.accountId, data.emailId, data.accountEmailId)
        let deleteEmailAccount = await EmailController.deleteEmailAccount(data.emailId, data.accountId)
        if(deleteAccountEmail.success && deleteEmailAccount.success){
            console.log('deleted all enteries')
            res.send({success: true, data:[], message:'email_deleted_from_account'})
        }
        else{
            console.log('didnt delete all entires need to rollback stuff')
            res.send({success: false, data:[], message: 'email_NOT_deleted_from_account_Rollback'})
        }        
    }
    else{
        res.sendStatus(500)
    }
})

router.get('/:id/peoplesEmail', async function(req, res){
    let returnObj = {success: false, data: null, message: 'nothing'}
    //console.log('in peoplesEmail' + req.params.id)
    let findAccount = await AccountController.getAccounts({_id: req.params.id}) //allows for more than 1
    //console.log(JSON.stringify(findAccount))
    if(findAccount.success && findAccount.data.length == 1){
        //build criteria to find email addresses
        let findIdString = []
        for(let accountEmail of findAccount.data[0].emailIds){
            findIdString.push(mongoose.Types.ObjectId(accountEmail.id))
        }
        //console.log('findIdString' + JSON.stringify(findIdString))
        let emailsInAccount = await EmailController.findEmails({_id: { $in: findIdString }})
        //console.log('emailsInAccount:'+JSON.stringify(emailsInAccount.data))
        if(emailsInAccount.success){
            returnObj = {success: true, data: [], message: 'account_emails'}
            //console.log(JSON.stringify(emailsInAccount.data))
            //difference between returning one or multile
            for(let email of emailsInAccount.data){
               
                for(let accountEmail of findAccount.data[0].emailIds){
                    //console.log(JSON.stringify(email._id) + JSON.stringify(accountEmail.id))

                    if(accountEmail.id == email._id){
                        //email, tier, accountId, emailId, account_EmailIds_id
                        returnObj.data.push({email: email.email, tier: accountEmail.tier, accountId: findAccount.data._id, emailId: email._id, accountEmailId: accountEmail._id })
                    }
                }
            }
            //console.log(JSON.stringify(returnObj))
        }
        else{
            returnObj = {success: false, data: null, message: 'account_NO_emails'}
        }  
    }
    else{
        returnObj = {success: false, data: null, message: 'account_not_fount'}
    }
    res.send(returnObj)
})



module.exports = router;