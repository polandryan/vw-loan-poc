var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var moment = require('moment');

var PersonController = require('../controllers/person')
var AccountController = require('../controllers/account')

//####################################################################

router.get('/account/:id', async function(req, res){
    let accountObject = await AccountController.findAccount({_id: req.params.id})
    let borrowerPersons = await PersonController.findPeople({_id: { $in: accountObject.data.borrowerPersonIds }})
    if(borrowerPersons.success){
        res.send(borrowerPersons)
    }
    else{
        res.send({success: false, data:null, message: 'borrower_not_found'})
    }
})

router.post('/create', async function(req, res){
    let data = req.body;
    //create a person
    let createInitialPerson = await PersonController.createPerson()
    //update person with name, and account
    let updateInitialPerson = await PersonController.updatePerson({_id: String(createInitialPerson.data._id)}, {$set: {firstName: data['firstName'], accountIds: [data['accountId']]} })
    console.log(JSON.stringify(updateInitialPerson))
    //then update account with borrowerpersonId
    let updatePreExistingAccount = await AccountController.updateAccount({_id: data['accountId']}, {$push: {borrowerPersonIds: String(createInitialPerson.data._id)}} )
    //console.log('in borrower create' + JSON.stringify(data))
    if(updatePreExistingAccount.success && updateInitialPerson.success){
        res.send(updateInitialPerson)
    }
    else{
        res.sendStatus(500)
    }
})

router.delete('/delete/:borrowerPersonId/account/:accountId', async function(req,res){
    //console.log('in delete borrower' + JSON.stringify(req.params))
    let findByCriteria = {_id: req.params.accountId }
    mongoose.model('account').updateOne(findByCriteria, {$pull: {'borrowerPersonIds':  req.params.borrowerPersonId}}, {multi: false}).exec()
    .then(resolved=>{res.sendStatus(200)})
    .catch(rejected=>{res.sendStatus(500)})
    
})

//##############################################################################

module.exports = router;