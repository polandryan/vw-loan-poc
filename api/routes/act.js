var express = require('express');
var router = express.Router();
var moment = require('moment');

var ActController = require('../controllers/act.ts')
var ObligationController = require('../controllers/obligation')

router.get('/read', async function(req, res){
    //console.log('Get obligation Acts for ' + JSON.stringify(req.query))
    let acts =[]
    if(String(req.query.types).includes('financial')){
        let foundFinActs = await ActController.findActs({obligationId: req.query.obligationId, type: {$in: ["financial"]} })
        if(foundFinActs['success']){
            acts = acts.concat(foundFinActs['data'])
        }
    }
    if(String(req.query.types).includes('onlyNext') && !String(req.query.types).includes('schedule') ){
        
        let foundLoan = await ObligationController.findPersonLoans({_id: req.query.obligationId})
        //console.log('in onlyNext' + JSON.stringify(foundLoan))
        if(foundLoan['success']){
            let continueIncrement = true
            for(let row of foundLoan['data'][0]['paymentSchedule']){
                if(continueIncrement && !row['eventId']){
                    continueIncrement = false
                    acts.push(row)
                }
            }
        }
    }
    if(String(req.query.types).includes('schedule') ){
        let foundLoan = await ObligationController.findPersonLoans({_id: req.query.obligationId})
        if(foundLoan['success']){
            for(let row of foundLoan['data'][0]['paymentSchedule']){
                if(!row['eventId']){
                    acts.push(row)
                }
            }
        }
    }
    if(String(req.query.types).includes('communication')){
        let foundComActs = await ActController.findActs({obligationId: req.query.obligationId, type: {$in: ["communication"]} })
        if(foundComActs['success']){
            acts = acts.concat(foundComActs['data'])
        }
    }
    res.send({success: true, data: acts, message: 'found_acts'})
})

router.put('/create', async function(req,res){
    //console.log('create Com Act'+ JSON.stringify(req.body))
    let responseObj = {success:false, data:null, message:'no_create_guts'}
    if(String(req.body.type).includes('communication')){
        responseObj = await ActController.createAct({
            type: [req.body.type],
            personId: req.body.personId,
            accountId: req.body.accountId,
            obligationId: req.body.obligationId,
            dateTime: req.body.dateTime,
        //Communication Acts
            comment: req.body.comment,
            method: req.body.method,
            direction: req.body.direction,
            tags: req.body.tags,
            state: req.body.state,
            
            when: {created: moment().unix()}
        })
       // console.log('create Com Act2'+ JSON.stringify(req.body))
    }
    res.send(responseObj)
    
})

module.exports = router;