var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

var EmailController = require('../controllers/email')
var PersonController = require('../controllers/person')
var AccountController = require('../controllers/account')


//###################################################################################

router.get('/verify/password', async function(req, res){
    //console.log(req.query.email)
    let verifiedEmailPassword = await EmailController.findEmail({$and: [{email: req.query.email}, {password: req.query.password}, {verified: true}]})
    if(verifiedEmailPassword.success){
        let person = await PersonController.findPerson({_id: verifiedEmailPassword.data.personId})
        if(person.success){
            res.send(person)
        }
        else{
            res.send({success: false, data: person.data, message: 'email_found_person_not_found'})
        }
    }
    else{
        let findEmail = await EmailController.findEmail({email: req.query.email})
        if(findEmail.success && !findEmail.data.verified && (req.query.password == findEmail.data.password) ){ //account needs verification, but the password is wrong
            res.send({success: false, data: findEmail.data.verification, message: 'email_needs_verification'})
        }
        else if(findEmail.success && !(req.query.password == findEmail.data.password)){ // email found password didn't match
            res.send({success: false, data: null, message: 'email_found_password_not_match'} )

        }
        else{
            res.send({success: false, data: null, message: 'email_not_found'})
        }
    } 
});

router.post('/register', async function(req, res){   
    let data = req.body;
    let findByCriteria = {email: data.email};
    let doesEmailExist = await EmailController.email_check_exists(data.address);
    if(!doesEmailExist.success && doesEmailExist.message == 'none found'){
        let createEmailRegister = await EmailController.email_create_register(data.email, data.password);
        if(createEmailRegister.success){
            res.send(createEmailRegister)
        }
        else{
            res.sendStatus(500)
        }
    }
});

router.post('/verify/code', async function(req, res){
    let data = req.body;
    let doesVerificationParamMatchCode = await EmailController.verifyUrlParam_code(data.urlParam, data.code)
    if(doesVerificationParamMatchCode['success']){
        let updateCriteria = await new Promise((res, rej)=>{
            if(data.password == null){
                res({$set: {verified: true}})
            }
            else{
                res({$set: {verified: true, password: data.password}})
            }
        })
        console.log('email update with' + JSON.stringify(updateCriteria))
        let updateEmail = await EmailController.updateEmail({_id: doesVerificationParamMatchCode['data']['_id']}, updateCriteria )
        //console.log(JSON.stringify(updateEmail))
        res.send(updateEmail)
    }
    else{
        //couldn't find code
        res.sendStatus(500)
    }
})

router.post('/create', async function(req, res){
    let returnObj = {success: false, data: null, message: 'nothing'}
    let data = req.body //email, tier, accountId
   // console.log(JSON.stringify(data))
    //see if email exists
    //*Need to see if accountId exists, and then we should pass in who is doing it and see if they can/should do it. 

    let findEmail = await EmailController.findEmail({email: data.email})
    let findAccountEmail = await EmailController.findEmail({$and: [{email: data.email}, {'accountIds': data.accountId}]})

    if(findEmail.success){//does email already exist for this account?
        
        if(findAccountEmail.success){//previously exists and seems to be associated
            res.send({success: false, data: [], message:'email_already_associated'})
        }
        else{//previously exists but isn't associted, so associate it to this account
            let updateExistingEmail = await EmailController.updateEmail({_id: findEmail.data._id}, {$push: {accountIds: data.accountId}})
            let updateExistingAccount = await AccountController.updateAccount({_id: data.accountId}, {$push: {emailIds: {id: findEmail.data._id, tier: data.tier }}})
            let updateExistingPerson = await PersonController.updatePerson({_id:findEmail.data.personId}, {$push: {accountIds: data.accountId, emailIds: findEmail.data._id}})
            if(updateExistingEmail.success && updateExistingAccount.success && updateExistingPerson.success){
                //HAPPY PATH
                let returnData = {
                    email: data.email, 
                    tier: data.tier, 
                    emailId: updateExistingEmail.data._id, 
                    accountId: updateExistingAccount.data._id,
                    verified: updateExistingEmail.data.verified,
                    verification: updateExistingEmail.data.verification }
                for(let accountEmail of updateExistingAccount.data.emailIds){
                    if(accountEmail.id == updateExistingEmail.data._id){
                        returnData['accountEmailId'] = accountEmail._id 
                    }
                }
                
                res.send({success: true, data: returnData, message: 'email_associated'})
            }
            else{
                //updates fail roll back stuff
                res.send({success: false, data: null, message: 'email_not_associated'})
            }
        }
        
    }
    else{ // No email exists 
        //insert email and new person *** we will have to figure out how to merge people later.
        let initialEmail = await EmailController.createEmail(data.email, null)
        let initialPerson = await PersonController.createPerson()
        if(initialEmail.success && initialPerson.success){
            //EMAIL & PESON were created now update account & email with corrisponding Id's
            let updateInitialEmail = await EmailController.updateEmail({_id: initialEmail.data._id}, {$push: {accountIds: data.accountId}, $set:{personId: initialPerson.data._id}})
            let updateInitialAccount = await AccountController.updateAccount({_id: data.accountId},{$push: {emailIds: {id: initialEmail.data._id, tier: data.tier }}, $set: {personId: initialPerson.data._id} })
            let updateInitialPerson = await PersonController.updatePerson({_id: initialPerson.data._id}, {$push: {accountIds: data.currentAccount_id, emailIds: initialEmail.data._id}, $set: {currentAccountId: data.accountId, currentAccountTier: data.tier}})
            if(updateInitialEmail.success && updateInitialAccount.success & updateInitialPerson.success){
                //HAPPY PATH
                let returnData = {
                    email: data.email, 
                    tier: data.tier, 
                    emailId: updateInitialEmail.data._id, 
                    accountId: updateInitialAccount.data._id,
                    verified: updateInitialEmail.data.verified,
                    verification: updateInitialEmail.data.verification }
                for(let accountEmail of updateInitialAccount.data.emailIds){
                    if(accountEmail.id == updateInitialEmail.data._id){
                        returnData['accountEmailId'] = accountEmail._id 
                    }
                }
                returnObj = {success: true, data: returnData, message: 'email_created'} 
                //console.log(returnObj)
                res.send(returnObj)
            }
            else{
                // Updates Errorred
                res.send({success: false, data: null, message: 'email_created_updates_failed'})
            }
        }
        else{
            //Roll back email creation
            res.send({success: false, data: null, message: 'email_not_created_updates_failed'})
        }
    }
})

module.exports = router;