var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var emailSchema = new Schema({
    email: String,
    password: String,
    verified: Boolean,
    verification: {generated: Number, expired: Number, urlParam: String, code: String},
    personId: String,
    accountIds: [],
    vwTier: String,
}, {collection: 'email' });

module.exports = mongoose.model('email', emailSchema);
