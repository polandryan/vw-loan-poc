var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ActSchema = new Schema({
    type: [], // financial (create_loan...) / communication
    personId: String,
    accountId: String,
    obligationId: String,
    dateTime: Number,
//Communication Acts
    comment: String,
    method: String,
    direction: String,
    tags: [],
    state: String,
//Financial Acts
    amount: Number,
    interestReduction: Number,
    principalReduction: Number,
    feeReduction: Number,
    accuredInterest:Number,

    debt: Number, 
    loanDebt: Number, 
    interest: Number,
    principal: Number,
    feeDebt: Number,
    foriegnIds: [],
    
    when: {created: Number}
}, {collection: 'act'});


module.exports = mongoose.model('act', ActSchema);