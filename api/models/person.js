var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var personSchema = new Schema({
    firstName: String,
    currentAccountId: String,
    currentAccountTier: String,
    emailIds: [],
    accountIds:[],
    experimentCode: String,
    when: {created: Number}
}, {collection: 'person'});

module.exports = mongoose.model('person', personSchema);
