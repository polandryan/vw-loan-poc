var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var amortizationTableSchema = new Schema({
    dateTime: Number,
    interest: Number,
    displayInterest: Number,
    principal: Number, 
    displayPrincipal: Number, 
    loanDebt: Number, 
    displayLoanDebt: Number,
    acttId: String,
})



var obligationSchema = new Schema({
    type: String, //loan
    personId: String,
    accountId: String,
    assetId: String,
    
    debt: Number, //total due
    loanDebt: Number, 
    interest: Number,
    principal: Number,
    feeDebt: Number,
    intervalLabel: String,
    intervals: Number,
    intervalPayment: Number,
    annualInterestRate: Number,
    compoundLabel: String,
    originalPrincipal: Number,

    paidDebt: Number,
    paidLoanDebt: Number, 
    paidInterest: Number,
    paidPrincipal: Number,
    paidFeeDebt: Number,
    paidPayments: Number, 
    
    amortizationTable: [amortizationTableSchema],
    projectedTotalAllPayments: Number,
    projectedTotalAllInterest: Number,
    projectedTotalAllPrincipal: Number,
    
    contractCreatedDateTime: Number, 
    interestAccruedDateTime: Number, 
    projectedFirstPaymentDateTime: Number,
    projectedLastPaymentDateTime: Number, 
    endDateTime: Number,

    make: String, 
    model: String, 
    vin: String, 
    year: Number, 
    additionalInformation: String,

    objectCreatedByPersonId: String,
    objectCreatedDateTime: Number,
}, {collection: 'obligation'});

module.exports = mongoose.model('obligation', obligationSchema);

/*Version 2
    Actually a loan because there is interest. 
    type: String, //loan
    personId: String,
    accountId: String,
    assetId: String,
    percision : 13
    displayPercision: 2 ? 

    debt
    loanDebt
    principal
    intereste
    feeDebt

    paidDebt
    paidLoanDebt
    paidPrincipal
    paidFeeDebt
    totalAllPayments

    annualInterestRate
    apr?  (fees & expences to aquire the loan?)
    intervals
    intervalLabel
    intervalPaymentAmount
    compoundLabel: simple/exact
    accrualDayType: 30/360 | real
    

    -when
        create
        end
        accruedInterest
        customTags (delinquent, missed... )

    
    startingDebt, startingLoanDebt....
    startingTotalAllPayments, startingTotalAllInterest


    estimates (?real or 30/360 || other)        
        endDate
        reformDebt, loanDebt, principal, interest (where you should be vs where you are)
        projectedDebt, projectedLoanDebt

        amortizationTable
            date , Payment, Principal, Interest (actual vs., display)


*/